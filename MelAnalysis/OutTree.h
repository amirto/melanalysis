#ifndef OutTree_h
#define OutTree_h

#include <vector>
#include <map>
#include <string>
#include "TTree.h"
#include "AsgTools/AsgMetadataTool.h"
#include "PATInterfaces/SystematicSet.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "SUSYTools/ISUSYObjDef_xAODTool.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "MuonSelectorTools/MuonSelectionTool.h"
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "IsolationSelection/IsolationSelectionTool.h"
#include "EventPrimitives/EventPrimitivesHelpers.h"
#include "xAODPrimitives/IsolationType.h"
#include "xAODEventInfo/EventInfo.h"
#include <EventLoop/Algorithm.h>

class OutTree : public asg::AsgMetadataTool {

public:
  OutTree( const std::string& name);
  ~OutTree();
  StatusCode initialize();
  bool process(xAOD::TEvent*& event, xAOD::ElectronContainer*& electrons, xAOD::PhotonContainer*& photons, xAOD::MuonContainer*& muons, xAOD::JetContainer*& jets, std::string sysname);
  bool processEmpty();//xAOD::TEvent*& event, xAOD::ElectronContainer*& electrons, xAOD::PhotonContainer*& photons, xAOD::MuonContainer*& muons, xAOD::JetContainer*& jets, std::string sysname);
  StatusCode FillTree();

  TString bookName(TString branch, TString sys_name );

  TTree* newTree;

  std::map<const std::string, float> m_WeightPU_map;

  std::map<const std::string, std::vector<float>*> jet_pt_map;
  std::map<const std::string, std::vector<float>*> jet_eta_map;
  std::map<const std::string, std::vector<float>*> jet_phi_map;
  std::map<const std::string, std::vector<float>*> jet_m_map;
  std::map<const std::string, std::vector<bool>*> jet_isB85_map;
  std::map<const std::string, std::vector<float>*> jet_BtagWeight_map;
  std::map<const std::string, std::vector<float>*> jet_JVT_map;
  std::map<const std::string, std::vector<bool>*> jet_passOR_map;
  std::map<const std::string, std::vector<int>*> labeljets_reco_map; 

  std::map<const std::string, std::vector<float>*> lep_pt_map;
  std::map<const std::string, std::vector<float>*> lep_eta_map;
  std::map<const std::string, std::vector<float>*> lep_phi_map;
  std::map<const std::string, std::vector<float>*> lep_m_map;
  std::map<const std::string, std::vector<float>*> lep_ptvarcone30_map;
  std::map<const std::string, std::vector<float>*> lep_ptvarcone20_map;
  std::map<const std::string, std::vector<float>*> lep_topoetcone20_map;
  std::map<const std::string, std::vector<int>*>   lep_flav_map;
  std::map<const std::string, std::vector<bool>*> lep_passOR_map;
  std::map<const std::string, std::vector<bool>*> lep_Signal_map;
  std::map<const std::string, std::vector<int>*> lep_Origin_map;  
  std::map<const std::string, std::vector<int>*> lep_Type_map;
  std::map<const std::string, std::vector<bool>*> lep_HighPt_map;
  std::map<const std::string, std::vector<bool>*> isTightlep_map;

  std::map<const std::string, std::vector<int>*> bkgTruthType_map;
  std::map<const std::string, std::vector<int>*> bkgTruthOrigin_map;
  std::map<const std::string, std::vector<int>*> bkgMotherPdgId_map;
  std::map<const std::string, std::vector<int>*> firstEgMotherTruthType_map;
  std::map<const std::string, std::vector<int>*> firstEgMotherTruthOrigin_map;
  std::map<const std::string, std::vector<int>*> firstEgMotherPdgId_map;

  std::map<const std::string, std::vector<bool>*> LepIsoFixedCutLoose_map;
  std::map<const std::string, std::vector<bool>*> LepIsoFixedCutTight_map;
  std::map<const std::string, std::vector<bool>*> LepIsoFixedCutTightTrackOnly_map;
  std::map<const std::string, std::vector<bool>*> LepIsoGradient_map;
  std::map<const std::string, std::vector<bool>*> LepIsoGradientLoose_map;
  std::map<const std::string, std::vector<bool>*> LepIsoLoose_map;
  std::map<const std::string, std::vector<bool>*> LepIsoLooseTrackOnly_map;
  std::map<const std::string, std::vector<bool>*> LepIsoFCLoose_map;

  std::map<const std::string, std::vector<float>*> d0siglep_map;
  std::map<const std::string, std::vector<float>*> z0lep_map;

  std::map<const std::string, std::vector<bool>*> gamma_passOR_map;

  std::map<const std::string, float> met_phi_map;
  std::map<const std::string, float> met_map;
  std::map<const std::string, float> metEle_phi_map;
  std::map<const std::string, float> metEle_map;
  std::map<const std::string, float> metMu_phi_map;
  std::map<const std::string, float> metMu_map;
  std::map<const std::string, float> metJet_phi_map;
  std::map<const std::string, float> metJet_map;
  std::map<const std::string, float> metGam_phi_map;
  std::map<const std::string, float> metGam_map;
  std::map<const std::string, float> metSoft_phi_map;
  std::map<const std::string, float> metSoft_map;
  std::map<const std::string, float> met_sig_map;
  std::map<const std::string, bool> CleaningFlag;

  std::map<const std::string, float> elSFwei_map;
  std::map<const std::string, float> muSFwei_map;
  std::map<const std::string, float> globalSFwei_map;
  //std::map<const std::string, float> trigwei_dilep_map;
  //std::map<const std::string, float> trigwei_mixlep_map;
  //std::map<const std::string, float> trigwei_mixlep_map;
  std::map<const std::string, float> trigwei_global_map;
  std::map<const std::string, float> bTagSF_map;
  std::map<const std::string, float> jvfSF_map;

  std::map<const std::string, int> trigger_map;
  std::map<const std::string, std::vector<int>*> trigger_matching_map;
  std::string name_branch;
  std::string name_branch_matching;

protected:

  std::map<std::string, int> m_variations_VV;
  std::map<std::string, int> m_variations_ttV;

  TDirectory *m_outfile;
  std::vector<ST::SystInfo> m_sysList;
  bool m_dotruth;
  bool m_ISDATA;
  std::vector<std::string> m_list_trig;
  std::string m_filter;
  bool m_btagWP;
  bool m_isSherpa;
  bool m_savephotons;
  bool m_multiweight;
  bool m_hasextravar;

  std::vector<float> *jet_pt;
  std::vector<float> *jet_eta;
  std::vector<float> *jet_phi;
  std::vector<float> *jet_m;
  std::vector<bool> *jet_isB70;
  std::vector<bool> *jet_isB77;
  std::vector<bool> *jet_isB85;
  std::vector<bool> *jet_isB70Flat;
  std::vector<bool> *jet_isB77Flat;
  std::vector<bool> *jet_isB85Flat;
  std::vector<float> *jet_BtagWeight;
  std::vector<float> *jet_JVT;
  std::vector<bool>  *jet_passOR;

  std::vector<float> *gamma_pt;
  std::vector<float> *gamma_eta;
  std::vector<float> *gamma_phi;
  std::vector<bool>  *gamma_passOR;

  std::vector<float> *lep_pt;
  std::vector<float> *lep_eta;
  std::vector<float> *lep_phi;
  std::vector<float> *lep_m;
  std::vector<float> *lep_ptvarcone30;
  std::vector<float> *lep_ptvarcone20;
  std::vector<float> *lep_topoetcone20;
  std::vector<int>   *lep_flav;
  std::vector<int>   *lep_Origin;
  std::vector<int>   *lep_Type;
  std::vector<bool>  *lep_passOR;
  std::vector<bool>  *lep_isSignal;
  std::vector<bool>  *lep_isBaseline;
  std::vector<bool>  *lep_HighPt;

  //new stuff for SUSY2 only
  std::vector<int> *bkgTruthType;
  std::vector<int> *bkgTruthOrigin;
  std::vector<int> *bkgMotherPdgId;
  std::vector<int> *firstEgMotherTruthType;
  std::vector<int> *firstEgMotherTruthOrigin;
  std::vector<int> *firstEgMotherPdgId;

  std::vector<float> *mc_eta; //!
  std::vector<float> *mc_pt; //!
  std::vector<float> *mc_mass; //!
  std::vector<float> *mc_phi; //!
  std::vector<int> *mc_pdgid; //!
  std::vector<int> *mc_parent_pdgid; //!

  std::vector<float> *ptjets_truth; //!
  std::vector<float> *etajets_truth; //!
  std::vector<float> *phijets_truth; //!
  std::vector<float> *massjets_truth; //!
  std::vector<int> *labeljets_truth; //!
  std::vector<int> *labeljets_reco; //!

  std::vector<bool> *isTightlep; //!
  std::vector<int> *isMatch; //!

  std::vector<bool> *LepIsoFixedCutLoose;  //!
  std::vector<bool> *LepIsoFixedCutTight;  //!
  std::vector<bool> *LepIsoFixedCutTightTrackOnly;  //!
  std::vector<bool> *LepIsoGradient;  //!
  std::vector<bool> *LepIsoGradientLoose;  //!
  std::vector<bool> *LepIsoLoose;  //!
  std::vector<bool> *LepIsoLooseTrackOnly;  //!
  std::vector<bool> *LepIsoFCLoose;  //!

  std::vector<bool> *gammaIsoFixedCutLoose;  //!
  std::vector<bool> *gammaIsoFixedCutTight;  //!

  std::vector<float> *d0siglep;  //!
  std::vector<float> *z0lep; //!

  std::vector<bool>* TM_2e24_lhvloose_nod0;
  std::vector<bool>* TM_mu22_mu8noL1; 
  std::vector<bool>* TM_e17_lhloose_nod0_mu14; 
  std::vector<bool>* TM_2e15_lhvloose_nod0_L12EM13VH;  
  std::vector<bool>* TM_2e17_lhvloose_nod0; 
  std::vector<bool>* TM_mu20_mu8noL1;  
  std::vector<bool>* TM_2e12_lhloose_L12EM10VH; 
  std::vector<bool>* TM_mu18_mu8noL1; 
  std::vector<bool>* TM_e17_lhloose_mu14;

  std::map<const std::string, std::vector<bool>*> TM_2e24_lhvloose_nod0_map;
  std::map<const std::string, std::vector<bool>*> TM_mu22_mu8noL1_map; 
  std::map<const std::string, std::vector<bool>*> TM_e17_lhloose_nod0_mu14_map; 
  std::map<const std::string, std::vector<bool>*> TM_2e15_lhvloose_nod0_L12EM13VH_map;  
  std::map<const std::string, std::vector<bool>*> TM_2e17_lhvloose_nod0_map; 
  std::map<const std::string, std::vector<bool>*> TM_mu20_mu8noL1_map;  
  std::map<const std::string, std::vector<bool>*> TM_2e12_lhloose_L12EM10VH_map; 
  std::map<const std::string, std::vector<bool>*> TM_mu18_mu8noL1_map; 
  std::map<const std::string, std::vector<bool>*> TM_e17_lhloose_mu14_map;

  float EtMiss_truth; //!
  float EtMiss_truthPhi; //!
  float Etmiss_PVSoftTrk; //!
  float Etmiss_PVSoftTrkPhi; //!
  float Etmiss_significance; //!

  ULong64_t m_EventNumber;
  float m_GenFiltHT;
  float m_GenFiltMET;
  UInt_t m_RunNumber;
  UInt_t m_LumiBlockNumber;
  double m_crossSection;
  float m_WeightEvents;

  float m_WeightEvents_MUR05_MUF1; //!
  float m_WeightEvents_MUR1_MUF05; //!
  float m_WeightEvents_MUR1_MUF2; //!
  float m_WeightEvents_MUR2_MUF1; //!

  //float m_WeightPU;
  float m_mu_av;
  int m_PrimVx;
  int m_decayType;
  int m_HFclass;
  bool m_passMETtrig;

  int TruthPDGID1; 
  int TruthPDGID2; 
  int TruthPDFID1; 
  int TruthPDFID2; 
  float TruthX1; 
  float TruthX2; 
  float TruthXF1; 
  float TruthXF2; 
  float TruthQ;

  AsgElectronLikelihoodTool *m_elecSelLikelihoodTLLH;
  CP::MuonSelectionTool *m_muonTightsel;
  
  CP::IsolationSelectionTool *m_isoToolFixedCutLoose;  //!
  CP::IsolationSelectionTool *m_isoToolFixedCutTight;  //!
  CP::IsolationSelectionTool *m_isoToolFixedCutTightTrackOnly;  //!
  CP::IsolationSelectionTool *m_isoToolGradient;  //!
  CP::IsolationSelectionTool *m_isoToolGradientLoose;  //!
  CP::IsolationSelectionTool *m_isoToolLoose;  //!
  CP::IsolationSelectionTool *m_isoToolLooseTrackOnly;  //!
  CP::IsolationSelectionTool *m_isoToolFCLoose;  //!

};

#endif
