#ifndef MelAnalysis_MyxAODAnalysis_H
#define MelAnalysis_MyxAODAnalysis_H

#include <EventLoop/Algorithm.h>

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

#include <TROOT.h>
#include <TH1.h>
#include <TTree.h>

#include "PATInterfaces/SystematicVariation.h"
#include "PATInterfaces/SystematicRegistry.h"
#include "PATInterfaces/SystematicCode.h"
#include "PATInterfaces/SystematicSet.h"

#include "SUSYTools/SUSYCrossSection.h"
#include "SUSYTools/ISUSYObjDef_xAODTool.h"

#include "MelAnalysis/OutTree.h"
#include "MelAnalysis/ttbarDecay.h"
#include "MelAnalysis/HFSystDataMembers.h"

#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "GoodRunsLists/DQHelperFunctions.h"

namespace ST {
class SUSYObjDef_xAOD;
}
// GRL
class GoodRunsListSelectionTool;
class DQHelperFunctions;

using namespace ST;

class MyxAODAnalysis : public EL::Algorithm
{

  SUSY::CrossSectionDB *my_XsecDB;  //!
  OutTree *mem_leaker; //!
  SUSYObjDef_xAOD *objTool_77; //!
  SUSYObjDef_xAOD *objTool_70; //!
  SUSYObjDef_xAOD *objTool_85; //!
  SUSYObjDef_xAOD *objTool_70Flat; //!
  SUSYObjDef_xAOD *objTool_77Flat; //!
  SUSYObjDef_xAOD *objTool_85Flat; //!
  GoodRunsListSelectionTool *grl; //!


private:

  std::vector<std::string> trig_list; //!
  std::vector<std::string> trig_list_el; //!
  std::vector<std::string> trig_list_mu; //!
  std::vector<bool> trig_pass; //!
  std::vector<ST::SystInfo> systInfoList; //!
  TString muTrigSingle2015; //!
  TString muTrigDouble2015; //!
  TString muTrigMix2015; //!
  TString muTrigSingle2016; //!
  TString muTrigDouble2016; //!
  TString muTrigMix2016; //!
  HFSystDataMembers myHFClassData; //!
  std::string sample_name;

  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:

  int processID;
  bool doSyst;
  bool doTruth;
  bool doBtagWP;
  std::string format;
  bool m_debug;
  bool savePhotons;

  int counter_total;
  int counter_GRL;
  int counter_vertex;
  int counter_badmu;
  int counter_cosmic;
  int counter_badjet;
  int counter_errflags;
  int counter_trig;
  int counter_atfill;
  int counter_2lep_sig_iso;
  int counter_2OR; 
  
  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)

public:

  TH1D *EventsCount; //!
  TH1D *NumberEvents; //!
  TH1D *EventsCount2; //! for 4 body
  TH1D *NumberEvents2; //!
  std::map<const std::string, TH1*> CutFlow; //!

  int whichYear; //!
  xAOD::TEvent *m_event; //!
  xAOD::TStore *m_store; //!

  // this is a standard constructor
  MyxAODAnalysis ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(MyxAODAnalysis, 1);
};

#endif
