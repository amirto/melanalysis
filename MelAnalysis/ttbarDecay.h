#ifndef ttbarDecay_h
#define ttbarDecay_h

#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertexContainer.h"

void Fill_decay_Cat(int &_ttbar_category, const xAOD::TruthParticleContainer*& mc_particles);

#endif


