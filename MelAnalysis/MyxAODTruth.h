#ifndef MelAnalysis_MyxAODTruth_H
#define MelAnalysis_MyxAODTruth_H

#include <EventLoop/Algorithm.h>

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "AsgTools/ToolHandle.h"

#include <TSystem.h>
#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <SUSYTools/SUSYCrossSection.h>

namespace ORUtils {
class IOverlapRemovalTool;
}

class MyxAODTruth : public EL::Algorithm
{

private:
  SUSY::CrossSectionDB *my_XsecDB;  //!
  TTree* truthTree; //!

  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:

  int processID;
  bool m_debug;
  bool isSherpa;
  bool skipCBK;
  bool MultiWeight;

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  
  std::vector<float> *mc_eta; //!
  std::vector<float> *mc_pt; //!
  std::vector<float> *mc_mass; //!
  std::vector<float> *mc_phi; //!
  std::vector<int> *mc_pdgid; //!
  std::vector<int> *mc_parent_pdgid; //!

  std::vector<float> *ptjets_truth; //!
  std::vector<float> *etajets_truth; //!
  std::vector<float> *phijets_truth; //!
  std::vector<float> *massjets_truth; //!
  std::vector<float> *labeljets_truth; //!
  std::vector<float> *labeljets_reco; //!

  float EtMiss_truth; //!
  float EtMiss_truthPhi; //!

  ULong64_t m_EventNumber; //!
  UInt_t m_RunNumber; //!
  double m_crossSection; //!
  float m_WeightEvents; //!
  float m_WeightEvents_MUR05_MUF05; //!
  float m_WeightEvents_MUR05_MUF1; //!
  float m_WeightEvents_MUR1_MUF05; //!
  float m_WeightEvents_MUR1_MUF2; //!
  float m_WeightEvents_MUR2_MUF1; //!
  float m_WeightEvents_MUR2_MUF2; //!

  TH1D *NumberEvents; //!

  xAOD::TEvent *m_event;  //!

  float  m_finalSumOfWeights; //!
  float  m_initialSumOfWeights; //!

  // this is a standard constructor
  MyxAODTruth ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(MyxAODTruth, 1);
};

#endif
