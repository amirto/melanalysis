#include "MelAnalysis/ttbarDecay.h"

using namespace std;

void Fill_decay_Cat(int &ttbar_category, const xAOD::TruthParticleContainer*& mc_particles) {

  int nWs = 0;
  vector<int> pdg_child1;
  vector<int> pdg_child2;
  int nTauLep = 0;
  int nTauHad = 0;
  ttbar_category = -999;
  for (const auto& particle : *mc_particles) {

    if (fabs(particle->pdgId()) == 15 ) {

      if (!particle->hasDecayVtx()) continue;

      bool isLeptonic = false;
      for (UInt_t c = 0; c < particle->nChildren(); c++) {
        const auto & child = particle->decayVtx()->outgoingParticle(c);
        if (fabs(child->pdgId()) == 11 || fabs(child->pdgId()) == 13)
          isLeptonic = true;
      }

      if (isLeptonic) nTauLep += 1;
      else            nTauHad += 1;
    }

    if (fabs(particle->pdgId()) == 24 ) {

      if (!particle->hasDecayVtx()) continue;

      if (particle->nChildren() != 2) continue;

      const auto & child1 = particle->decayVtx()->outgoingParticle(0);
      const auto & child2 = particle->decayVtx()->outgoingParticle(1);

      if (fabs(child1->pdgId()) != 24 && fabs(child2->pdgId()) != 24 && child1->status() == 3 &&  child2->status() == 3) {
        nWs += 1;
        pdg_child1.push_back(child1->pdgId());
        pdg_child2.push_back(child2->pdgId());
      }

    }
  }

  // Assign categories
  // -1 not enough W

  // 0  2L
  // 1  1L1taulep
  // 2  2taulep

  // 3  1Lhad
  // 4  1L1tauhad
  // 5  1taulep1had
  // 6  1taulep1tauhad

  // 7  2had
  // 8  1had1tauhad
  // 9  2tauhad

  switch (nWs) {
  case 2:
    if ( fabs(pdg_child1[0]) == 11 || fabs(pdg_child1[0]) == 13 || fabs(pdg_child2[0]) == 11 || fabs(pdg_child2[0]) == 13 ) {
      // first leptonic W
      if ( fabs(pdg_child1[1]) == 11 || fabs(pdg_child1[1]) == 13 || fabs(pdg_child2[1]) == 11 || fabs(pdg_child2[1] == 13) )
        ttbar_category = 0; // 2L
      else if ( fabs(pdg_child1[1]) == 15 || fabs(pdg_child2[1]) == 15 ) {
        // 1L + taus
        if      (nTauHad > 0) ttbar_category = 4;
        else if (nTauLep > 0) ttbar_category = 1;
      } else {
        ttbar_category = 3; // 1L + had
      }
    } else if ( fabs(pdg_child1[0]) == 15 || fabs(pdg_child2[0]) == 15 ) {
      // the first W is a tau W
      if ( fabs(pdg_child1[1]) == 15 || fabs(pdg_child2[1]) == 15 ) {
        // the second W is a tau as well
        if      (nTauLep > 1) ttbar_category = 2;   // 7  tauleptaulep
        else if (nTauHad > 0 && nTauLep > 0) ttbar_category = 6;  // 6  tauleptauhad
        else if (nTauHad > 1) ttbar_category = 9;  // 9 2tauhad
      } else if ( fabs(pdg_child1[1]) == 11 || fabs(pdg_child1[1]) == 13 || fabs(pdg_child2[1]) == 11 || fabs(pdg_child2[1] == 13)) {
        // the second W is a lepton
        if      (nTauLep > 0) ttbar_category = 1;   //  1L taulep
        else if (nTauHad > 0) ttbar_category = 4;   //  1L tauhad
      } else {
        // the second W is hadronic
        if      (nTauHad > 0) ttbar_category = 8;   // 8  had1tauhad
        else if (nTauLep > 0) ttbar_category = 5;   // 5  had1taulep
      }
    } else {
      // is an hadronic W
      if ( fabs(pdg_child1[1]) == 15 || fabs(pdg_child2[1]) == 15 ) {
        // the second W is a tau
        if      (nTauLep > 0) ttbar_category = 5;   // 5  had1taulep
        else if (nTauHad > 0) ttbar_category = 8;   // 8  had1tauhad
      } else if ( fabs(pdg_child1[1]) == 11 || fabs(pdg_child1[1]) == 13 || fabs(pdg_child2[1]) == 11 || fabs(pdg_child2[1] == 13)) {
        ttbar_category = 3; // 1L + had
      } else {
        ttbar_category = 7;
      }
    }
    break;
  case 1:
    ttbar_category = -1;
    break;
  case 0:
    ttbar_category = -1;
    break;
  default:
    ttbar_category = -1;
    break;
  }

  return;
}
