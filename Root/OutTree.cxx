#include "MelAnalysis/OutTree.h"
#include <AsgTools/MessageCheck.h>
#include "TRandom3.h"

static SG::AuxElement::Accessor<int>  acc_Origin("truthOrigin");
static SG::AuxElement::Accessor<int>  acc_Type("truthType");
static SG::AuxElement::Accessor<int>  acc_bkgTruthType("bkgTruthType");
static SG::AuxElement::Accessor<int>  acc_bkgTruthOrigin("bkgTruthOrigin");
static SG::AuxElement::Accessor<int>  acc_bkgMotherPdgId("bkgMotherPdgId");
static SG::AuxElement::Accessor<int>  acc_firstEgMotherTruthType("firstEgMotherTruthType");
static SG::AuxElement::Accessor<int>  acc_firstEgMotherTruthOrigin("firstEgMotherTruthOrigin");
static SG::AuxElement::Accessor<int>  acc_firstEgMotherPdgId("firstEgMotherPdgId");
const static SG::AuxElement::ConstAccessor<char>  acc_passJvt("passJvt");
static SG::AuxElement::Accessor<float>  acc_Jvt("Jvt");

OutTree::OutTree( const std::string& name): asg::AsgMetadataTool( name) {

	declareProperty( "SystematicList", m_sysList); //pass here the list of systematics
	declareProperty( "OutFile", m_outfile); //here we should pass *file = wk()->getOutputFile ("output");
	declareProperty( "DoTruth", m_dotruth);
	declareProperty( "Format", m_filter);
	declareProperty( "DoBtagWP", m_btagWP);
	declareProperty( "IsData", m_ISDATA);
	declareProperty( "ListTrig", m_list_trig);
	declareProperty( "AddPhotons", m_savephotons);
	declareProperty( "IsSherpa", m_isSherpa);
	declareProperty( "MultiWeight", m_multiweight);
	declareProperty( "HasExtraVar", m_hasextravar);

	newTree = 0;
	m_dotruth = false;
	m_filter = ""; // formats: stop2L, stop4b, stopZ, stopH
	m_btagWP = false;
	m_ISDATA = 0;
	m_savephotons = 0;
	m_isSherpa = false;
	m_multiweight = false;
	m_hasextravar = false;

	m_elecSelLikelihoodTLLH = 0;
	m_muonTightsel = 0;
	
	m_isoToolFixedCutLoose = 0;
	m_isoToolFixedCutTight = 0;
	m_isoToolFixedCutTightTrackOnly = 0;
	m_isoToolGradient = 0;
	m_isoToolGradientLoose = 0;
	m_isoToolLoose = 0;
	m_isoToolLooseTrackOnly = 0;
	m_isoToolFCLoose = 0;
}

OutTree::~OutTree() {
	for (const auto& sys : m_sysList) {
		const CP::SystematicSet& sysSet = sys.systset;
		std::string sys_name = sysSet.name().c_str();

		for (UInt_t i = 0; i < m_list_trig.size(); i++) {
			name_branch_matching =  m_list_trig.at(i) + "_match" + sys_name;
			delete trigger_matching_map[name_branch_matching];
		}

		if (sys.affectsKinematics) {
			delete jet_pt_map[sys_name];
			delete jet_eta_map[sys_name];
			delete jet_phi_map[sys_name];
			delete jet_m_map[sys_name];
			delete jet_passOR_map[sys_name];
			delete jet_isB85_map[sys_name];
			delete jet_BtagWeight_map[sys_name];
			delete labeljets_reco_map[sys_name];
			delete jet_JVT_map[sys_name];

			delete lep_pt_map[sys_name];
			delete lep_eta_map[sys_name];
			delete lep_phi_map[sys_name];
			delete lep_m_map[sys_name];
			delete lep_ptvarcone30_map[sys_name];
			delete lep_ptvarcone20_map[sys_name];
			delete lep_topoetcone20_map[sys_name];
			delete lep_flav_map[sys_name];
			delete lep_passOR_map[sys_name];
			delete lep_Signal_map[sys_name];
			delete lep_Origin_map[sys_name];
			delete lep_HighPt_map[sys_name];

			delete isTightlep_map[sys_name];

			delete bkgTruthType_map[sys_name];
			delete bkgTruthOrigin_map[sys_name];
			delete bkgMotherPdgId_map[sys_name];
			delete firstEgMotherTruthType_map[sys_name];
			delete firstEgMotherTruthOrigin_map[sys_name];
			delete firstEgMotherPdgId_map[sys_name];

			delete LepIsoFixedCutLoose_map[sys_name];
			delete LepIsoFixedCutTight_map[sys_name];
			delete LepIsoFixedCutTightTrackOnly_map[sys_name];
			delete LepIsoGradient_map[sys_name];
			delete LepIsoGradientLoose_map[sys_name];
			delete LepIsoLoose_map[sys_name];
			delete LepIsoLooseTrackOnly_map[sys_name];
			delete LepIsoFCLoose_map[sys_name];

			delete d0siglep_map[sys_name];
			delete z0lep_map[sys_name];

                 	if (m_ISDATA) {
                         	delete TM_2e24_lhvloose_nod0_map[sys_name];   
                        	delete TM_mu22_mu8noL1_map[sys_name]; 
                           	delete TM_e17_lhloose_nod0_mu14_map[sys_name]; 
                            	delete TM_2e15_lhvloose_nod0_L12EM13VH_map[sys_name];  
                               	delete TM_2e17_lhvloose_nod0_map[sys_name]; 
                          	delete TM_mu20_mu8noL1_map[sys_name];  
                        	delete TM_2e12_lhloose_L12EM10VH_map[sys_name]; 
                            	delete TM_mu18_mu8noL1_map[sys_name]; 
                        	delete TM_e17_lhloose_mu14_map[sys_name];
                 	}
		}
	}

	delete m_elecSelLikelihoodTLLH;
	delete m_muonTightsel;

	delete m_isoToolFixedCutLoose;
	delete m_isoToolFixedCutTight;
	delete m_isoToolFixedCutTightTrackOnly;
	delete m_isoToolGradient;
	delete m_isoToolGradientLoose;
	delete m_isoToolLoose;
	delete m_isoToolLooseTrackOnly;
	delete m_isoToolFCLoose;

	if (m_btagWP) {
		delete jet_isB70;
		delete jet_isB85;
		delete jet_isB70Flat;
		delete jet_isB77Flat;
		delete jet_isB85Flat;
	}
	if (m_savephotons) {
		delete gamma_pt;
		delete gamma_phi;
		delete gamma_eta;
		delete gamma_passOR;
		delete gammaIsoFixedCutLoose;
		delete gammaIsoFixedCutTight;
	}

	if (m_dotruth) {
		delete mc_eta;
		delete mc_pt;
		delete mc_mass;
		delete mc_phi;
		delete mc_pdgid;
		delete mc_parent_pdgid;

		delete ptjets_truth;
		delete etajets_truth;
		delete phijets_truth;
		delete massjets_truth;
		delete labeljets_truth;
	}
}

TString OutTree::bookName(TString branch, TString sys_name ) {
	TString ret = branch + "_" + sys_name ;
	return ret;
}

StatusCode OutTree::initialize() {
	const char *APP_NAME = "initialize()";

	//Init the nominal tree
	jet_pt = new std::vector<float>();
	jet_eta = new std::vector<float>();
	jet_phi = new std::vector<float>();
	jet_m = new std::vector<float>();
	jet_isB85 = new std::vector<bool>();
	jet_BtagWeight = new std::vector<float>();
	jet_JVT = new std::vector<float>();
	labeljets_reco = new std::vector<int>();
	if (m_btagWP) {
		jet_isB70 = new std::vector<bool>();
		jet_isB85 = new std::vector<bool>();
		jet_isB70Flat = new std::vector<bool>();
		jet_isB77Flat = new std::vector<bool>();
		jet_isB85Flat = new std::vector<bool>();
	}
	jet_passOR = new std::vector<bool>();
	if (m_ISDATA) {
         	TM_2e24_lhvloose_nod0 = new std::vector<bool>(); 
        	TM_mu22_mu8noL1 = new std::vector<bool>();
        	TM_e17_lhloose_nod0_mu14 = new std::vector<bool>();
        	TM_2e15_lhvloose_nod0_L12EM13VH = new std::vector<bool>();
        	TM_2e17_lhvloose_nod0 = new std::vector<bool>();
        	TM_mu20_mu8noL1 = new std::vector<bool>();
        	TM_2e12_lhloose_L12EM10VH = new std::vector<bool>();
        	TM_mu18_mu8noL1 = new std::vector<bool>();
        	TM_e17_lhloose_mu14 = new std::vector<bool>();
	}
	lep_pt = new std::vector<float>();
	lep_eta = new std::vector<float>();
	lep_phi = new std::vector<float>();
	lep_m = new std::vector<float>();
	lep_ptvarcone30 = new std::vector<float>();
	lep_ptvarcone20 = new std::vector<float>();
	lep_topoetcone20 = new std::vector<float>();
	lep_flav = new std::vector<int>();
	lep_Origin = new std::vector<int>();
	lep_Type = new std::vector<int>();
	bkgTruthType = new std::vector<int>();
	bkgTruthOrigin = new std::vector<int>();
	bkgMotherPdgId = new std::vector<int>();
	firstEgMotherTruthType = new std::vector<int>();
	firstEgMotherTruthOrigin = new std::vector<int>();
	firstEgMotherPdgId = new std::vector<int>();
	lep_passOR = new std::vector<bool>();
	lep_isSignal = new std::vector<bool>();
	lep_HighPt = new std::vector<bool>();
	isTightlep = new std::vector<bool>();
	LepIsoFixedCutLoose =  new std::vector<bool>();
	LepIsoFixedCutTight =  new std::vector<bool>();
	LepIsoFixedCutTightTrackOnly =  new std::vector<bool>();
	LepIsoGradient =  new std::vector<bool>();
	LepIsoGradientLoose =  new std::vector<bool>();
	LepIsoLoose =  new std::vector<bool>();
	LepIsoLooseTrackOnly =  new std::vector<bool>();
	LepIsoFCLoose = new std::vector<bool>();
	d0siglep = new std::vector<float>();
	z0lep = new std::vector<float>();

	if (m_savephotons) {
		gamma_pt = new std::vector<float>();
		gamma_eta = new std::vector<float>();
		gamma_phi = new std::vector<float>();
		gamma_passOR = new std::vector<bool>();
		gammaIsoFixedCutLoose =  new std::vector<bool>();
		gammaIsoFixedCutTight =  new std::vector<bool>();
	}

	if (m_dotruth) {
		mc_eta = new std::vector<float>();
		mc_pt = new std::vector<float>();
		mc_mass = new std::vector<float>();
		mc_phi = new std::vector<float>();
		mc_pdgid = new std::vector<int>();
		mc_parent_pdgid = new std::vector<int>();

		ptjets_truth = new std::vector<float>();
		etajets_truth = new std::vector<float>();
		phijets_truth = new std::vector<float>();
		massjets_truth = new std::vector<float>();
		labeljets_truth = new std::vector<int>();
	}

	TString tree_name = "myTree";
	//Get ready to leak memory all over the place
	newTree = new TTree(tree_name, tree_name);
	newTree->SetDirectory(m_outfile);

	newTree->Branch("EventNumber", &m_EventNumber, "EventNumber/l");
	newTree->Branch("RunNumber", &m_RunNumber, "RunNumber/i");
	newTree->Branch("GenFiltMET", &m_GenFiltMET);
	newTree->Branch("GenFiltHT", &m_GenFiltHT);
	newTree->Branch("LumiBlockNumber", &m_LumiBlockNumber, "LumiBlock/i");
	newTree->Branch("xsec", &m_crossSection);
	newTree->Branch("WeightEvents", &m_WeightEvents);
	if (m_multiweight) {
		newTree->Branch("WeightEvents_MUR05_MUF1", &m_WeightEvents_MUR05_MUF1);
		newTree->Branch("WeightEvents_MUR1_MUF05", &m_WeightEvents_MUR1_MUF05);
		newTree->Branch("WeightEvents_MUR1_MUF2", &m_WeightEvents_MUR1_MUF2);
		newTree->Branch("WeightEvents_MUR2_MUF1", &m_WeightEvents_MUR2_MUF1);
	}
	newTree->Branch("WeightEventsPU", &m_WeightPU_map[""]);
	newTree->Branch("WeightEventselSF", &elSFwei_map[""] );
	newTree->Branch("WeightEventsmuSF", &muSFwei_map[""] );
	newTree->Branch("WeightEventsSF_global", &globalSFwei_map[""] );
	//newTree->Branch("WeightEvents_trigger_single", &trigwei_singlelep_map[""]);
	//newTree->Branch("WeightEvents_trigger_dilep", &trigwei_dilep_map[""]);
	//newTree->Branch("WeightEvents_trigger_mixlep", &trigwei_mixlep_map[""]);
	newTree->Branch("WeightEvents_trigger_global", &trigwei_global_map[""]);
	newTree->Branch("WeightEventsbTag", &bTagSF_map[""]);
	newTree->Branch("WeightEventsJVT", &jvfSF_map[""]);
	newTree->Branch("AverageInteractionsPerCrossing", &m_mu_av);
	newTree->Branch("NumPrimaryVertices", &m_PrimVx);
	newTree->Branch("DecayType", &m_decayType);
	newTree->Branch("HFclass", &m_HFclass);
	newTree->Branch("passMETtrig", &m_passMETtrig);

	if (m_dotruth) {
		newTree->Branch("ptjets_truth", &ptjets_truth);
		newTree->Branch("etajets_truth", &etajets_truth);
		newTree->Branch("phijets_truth", &phijets_truth);
		newTree->Branch("massjets_truth", &massjets_truth);
		newTree->Branch("labeljets_truth", &labeljets_truth);

		newTree->Branch("mc_eta", &mc_eta);
		newTree->Branch("mc_pt", &mc_pt);
		newTree->Branch("mc_mass", &mc_mass);
		newTree->Branch("mc_phi", &mc_phi);
		newTree->Branch("mc_pdgid", &mc_pdgid);
		newTree->Branch("mc_parent_pdgid", &mc_parent_pdgid);

		newTree->Branch("EtMiss_truthPhi", &EtMiss_truthPhi );
		newTree->Branch("EtMiss_truth", &EtMiss_truth);
	}

	//PDF junk
	newTree->Branch("TruthPDGID1", &TruthPDGID1);
	newTree->Branch("TruthPDGID2", &TruthPDGID2);
	newTree->Branch("TruthPDFID1", &TruthPDFID1);
	newTree->Branch("TruthPDFID2", &TruthPDFID2);
	newTree->Branch("TruthX1", &TruthX1);
	newTree->Branch("TruthX2", &TruthX2);
	newTree->Branch("TruthXF1", &TruthXF1);
	newTree->Branch("TruthXF2", &TruthXF2);
	newTree->Branch("TruthQ",&TruthQ);

	newTree->Branch("labeljets_reco", &labeljets_reco);
	newTree->Branch("ptjets", &jet_pt_map[""]);
	newTree->Branch("etajets", &jet_eta_map[""]);
	newTree->Branch("phijets", &jet_phi_map[""]);
	newTree->Branch("massjets", &jet_m_map[""]);
	newTree->Branch("isB85jets", &jet_isB85_map[""]);
	newTree->Branch("BtagWeightjets", &jet_BtagWeight_map[""]);
	newTree->Branch("JVTjets", &jet_JVT_map[""]);

	if (m_btagWP) {
		newTree->Branch("isB70jets", &jet_isB70);
		newTree->Branch("isB85jets", &jet_isB85);
		newTree->Branch("isB70Flatjets", &jet_isB70Flat);
		newTree->Branch("isB77Flatjets", &jet_isB77Flat);
		newTree->Branch("isB85Flatjets", &jet_isB85Flat);
	}
	newTree->Branch("passORjet", &jet_passOR_map[""]);

	if (m_ISDATA) {
         	newTree->Branch("TrigMatch_2e24_lhvloose_nod0", &TM_2e24_lhvloose_nod0_map[""]);   
        	newTree->Branch("TrigMatch_mu22_mu8noL1", &TM_mu22_mu8noL1_map[""]); 
        	newTree->Branch("TrigMatch_e17_lhloose_nod0_mu14", &TM_e17_lhloose_nod0_mu14_map[""]); 
        	newTree->Branch("TrigMatch_2e15_lhvloose_nod0_L12EM13VH", &TM_2e15_lhvloose_nod0_L12EM13VH_map[""]);  
        	newTree->Branch("TrigMatch_2e17_lhvloose_nod0", &TM_2e17_lhvloose_nod0_map[""]); 
        	newTree->Branch("TrigMatch_mu20_mu8noL1", &TM_mu20_mu8noL1_map[""]);  
        	newTree->Branch("TrigMatch_2e12_lhloose_L12EM10VH", &TM_2e12_lhloose_L12EM10VH_map[""]); 
        	newTree->Branch("TrigMatch_mu18_mu8noL1", &TM_mu18_mu8noL1_map[""]); 
        	newTree->Branch("TrigMatch_e17_lhloose_mu14", &TM_e17_lhloose_mu14_map[""]);
	}

	if (m_savephotons) {
		newTree->Branch("ptgamma", &gamma_pt);
		newTree->Branch("etagamma", &gamma_eta);
		newTree->Branch("phigamma", &gamma_phi);
		newTree->Branch("passORgamma", &gamma_passOR);
		newTree->Branch("gammaIsoFixedCutLoose", &gammaIsoFixedCutLoose );
		newTree->Branch("gammaIsoFixedCutTight", &gammaIsoFixedCutTight );
	}
	newTree->Branch("etaleptons", &lep_eta_map[""]);
	newTree->Branch("ptleptons", &lep_pt_map[""]);
	newTree->Branch("massleptons", &lep_m_map[""]); //This is here for sanity checks
	newTree->Branch("ptvarcone30leptons", &lep_ptvarcone30);
	newTree->Branch("ptvarcone20leptons", &lep_ptvarcone20);
	newTree->Branch("topoetcone20leptons", &lep_topoetcone20);
	newTree->Branch("phileptons", &lep_phi_map[""]);
	newTree->Branch("flavlep", &lep_flav_map[""]);
	if (!m_ISDATA) {
		newTree->Branch("Originlep", &lep_Origin_map[""]);
		newTree->Branch("Typelep", &lep_Type_map[""]);
	}
	if (!m_ISDATA && m_hasextravar) {
		newTree->Branch("bkgTruthType", &bkgTruthType);
		newTree->Branch("bkgTruthOrigin", &bkgTruthOrigin);
		newTree->Branch("bkgMotherPdgId", &bkgMotherPdgId);
		newTree->Branch("firstEgMotherTruthType", &firstEgMotherTruthType);
		newTree->Branch("firstEgMotherTruthOrigin", &firstEgMotherTruthOrigin);
		newTree->Branch("firstEgMotherPdgId", &firstEgMotherPdgId);
	}

	newTree->Branch("passORlep", &lep_passOR_map[""]);
	newTree->Branch("isSignallep", &lep_Signal_map[""]);
	newTree->Branch("isHighPtlep", &lep_HighPt_map[""]);
	newTree->Branch("isTightlep", &isTightlep_map[""]);

	newTree->Branch("LepIsoFixedCutLoose", &LepIsoFixedCutLoose_map[""] );
	newTree->Branch("LepIsoFixedCutTight", &LepIsoFixedCutTight_map[""] );
	newTree->Branch("LepIsoFixedCutTightTrackOnly", &LepIsoFixedCutTightTrackOnly_map[""] );
	newTree->Branch("LepIsoGradient", &LepIsoGradient_map[""] );
	newTree->Branch("LepIsoGradientLoose", &LepIsoGradientLoose_map[""] );
	newTree->Branch("LepIsoLoose", &LepIsoLoose_map[""] );
	newTree->Branch("LepIsoLooseTrackOnly", &LepIsoLooseTrackOnly_map[""] );
	newTree->Branch("LepIsoFCLoose", &LepIsoFCLoose_map[""]);

	newTree->Branch("d0sig", &d0siglep_map[""]);
	newTree->Branch("z0sinTheta", &z0lep_map[""]);

	newTree->Branch("cleaningVeto", &CleaningFlag[""]);

	newTree->Branch("EtMiss_tstPhi", &met_phi_map[""] );
	newTree->Branch("EtMiss_tst", &met_map[""]);
	newTree->Branch("EtMiss_ElePhi", &metEle_phi_map[""] );
	newTree->Branch("EtMiss_Ele", &metEle_map[""]);
	newTree->Branch("EtMiss_MuPhi", &metMu_phi_map[""] );
	newTree->Branch("EtMiss_Mu", &metMu_map[""]);
	newTree->Branch("EtMiss_JetPhi", &metJet_phi_map[""] );
	newTree->Branch("EtMiss_Jet", &metJet_map[""]);
	newTree->Branch("EtMiss_GamPhi", &metGam_phi_map[""] );
	newTree->Branch("EtMiss_Gam", &metGam_map[""]);
	newTree->Branch("EtMiss_SoftPhi", &metSoft_phi_map[""] );
	newTree->Branch("EtMiss_Soft", &metSoft_map[""]);
	newTree->Branch("EtMiss_significance", &met_sig_map[""]);

	newTree->Branch("Etmiss_PVSoftTrkPhi", &Etmiss_PVSoftTrkPhi );
	newTree->Branch("Etmiss_PVSoftTrk", &Etmiss_PVSoftTrk);


	for (UInt_t i = 0; i < m_list_trig.size(); i++) {

		name_branch = m_list_trig.at(i);
		trigger_map.insert(std::pair<std::string, int>(name_branch.c_str(), 0));
		newTree->Branch(name_branch.c_str(), &trigger_map[name_branch.c_str()]);

		name_branch_matching =  m_list_trig.at(i) + "_match";
		isMatch = new std::vector<int>();
		trigger_matching_map.insert(std::pair<std::string, std::vector<int>*>(name_branch_matching.c_str(), isMatch));

		std::size_t found_XE = m_list_trig.at(i).find("HLT_xe");
		if ( found_XE != std::string::npos ) continue;

		//newTree->Branch(name_branch_matching.c_str(), &trigger_matching_map[name_branch_matching]);

	}

	//Extra tools for optimisation
	//Isolationf
	m_isoToolFixedCutLoose = new CP::IsolationSelectionTool("isoToolFixedCutLoose" + m_filter);
	ANA_CHECK(m_isoToolFixedCutLoose->setProperty("ElectronWP", "FixedCutLoose"));
	ANA_CHECK(m_isoToolFixedCutLoose->setProperty("MuonWP", "FixedCutLoose"));
	ANA_CHECK(m_isoToolFixedCutLoose->setProperty("PhotonWP", "FixedCutLoose"));
	ANA_CHECK(m_isoToolFixedCutLoose->initialize());

	m_isoToolFixedCutTight = new CP::IsolationSelectionTool("isoToolFixedCutTight" + m_filter);
	ANA_CHECK(m_isoToolFixedCutTight->setProperty("ElectronWP", "FixedCutTight"));
	ANA_CHECK(m_isoToolFixedCutTight->setProperty("MuonWP", "FixedCutTightTrackOnly"));
	ANA_CHECK(m_isoToolFixedCutTight->setProperty("PhotonWP", "FixedCutTight"));
	ANA_CHECK(m_isoToolFixedCutTight->initialize());

	m_isoToolFixedCutTightTrackOnly = new CP::IsolationSelectionTool("isoToolFixedCutTightTrackOnly" + m_filter);
	ANA_CHECK(m_isoToolFixedCutTightTrackOnly->setProperty("ElectronWP", "FixedCutTightTrackOnly"));
	ANA_CHECK(m_isoToolFixedCutTightTrackOnly->setProperty("MuonWP", "FixedCutTightTrackOnly"));
	ANA_CHECK(m_isoToolFixedCutTightTrackOnly->initialize());

	m_isoToolGradient = new CP::IsolationSelectionTool("isoToolGradient" + m_filter);
	ANA_CHECK(m_isoToolGradient->setProperty("ElectronWP", "Gradient"));
	ANA_CHECK(m_isoToolGradient->setProperty("MuonWP", "Gradient"));
	ANA_CHECK(m_isoToolGradient->initialize());

	m_isoToolGradientLoose = new CP::IsolationSelectionTool("isoToolGradientLoose" + m_filter);
	ANA_CHECK(m_isoToolGradientLoose->setProperty("ElectronWP", "GradientLoose"));
	ANA_CHECK(m_isoToolGradientLoose->setProperty("MuonWP", "GradientLoose"));
	ANA_CHECK(m_isoToolGradientLoose->initialize());

	m_isoToolLoose = new CP::IsolationSelectionTool("isoToolLoose" + m_filter);
	ANA_CHECK(m_isoToolLoose->setProperty("ElectronWP", "Loose"));
	ANA_CHECK(m_isoToolLoose->setProperty("MuonWP", "Loose"));
	ANA_CHECK(m_isoToolLoose->initialize());

	m_isoToolLooseTrackOnly = new CP::IsolationSelectionTool("isoToolLooseTrackOnly" + m_filter);
	ANA_CHECK(m_isoToolLooseTrackOnly->setProperty("ElectronWP", "LooseTrackOnly"));
	ANA_CHECK(m_isoToolLooseTrackOnly->setProperty("MuonWP", "LooseTrackOnly"));
	ANA_CHECK(m_isoToolLooseTrackOnly->initialize());

	bool m_debug = false;

	m_isoToolFCLoose = new CP::IsolationSelectionTool("isoToolFCLoose" + m_filter);
	ANA_CHECK(m_isoToolFCLoose->setProperty("ElectronWP", "FCLoose"));
	ANA_CHECK(m_isoToolFCLoose->setProperty("MuonWP", "FCLoose"));
	ANA_CHECK(m_isoToolFCLoose->initialize());
	if (m_isoToolFCLoose->initialize() != StatusCode::SUCCESS) return EL::StatusCode::FAILURE;
        else Info(APP_NAME, "FCLoose Isolation initialized... " );

//	m_isoToolFCLoose = new CP::IsolationSelectionTool("isoToolFixedCutLoose1" + m_filter);
//	ANA_CHECK(m_isoToolFCLoose->setProperty("ElectronWP", "FixedCutLoose"));
//	ANA_CHECK(m_isoToolFCLoose->setProperty("MuonWP", "FixedCutLoose"));
//	ANA_CHECK(m_isoToolFCLoose->initialize());

	//TightLH ele ID
	m_elecSelLikelihoodTLLH = new AsgElectronLikelihoodTool("AsgElectronLikelihoodTool_OutTree" + m_filter);
	ANA_CHECK( m_elecSelLikelihoodTLLH->setProperty("WorkingPoint", "TightLHElectron")); //TODO    
	ANA_CHECK( m_elecSelLikelihoodTLLH->initialize() );

	//Tight Muons
	m_muonTightsel = new CP::MuonSelectionTool("MuonSelectionTool_OutTree" + m_filter);
	ANA_CHECK(  m_muonTightsel->setProperty( "MaxEta", 2.7 ) );
	ANA_CHECK(  m_muonTightsel->setProperty( "MuQuality", 0 ) ); // 0 == tight
	ANA_CHECK(  m_muonTightsel->setProperty( "TrtCutOff", false ) );
	ANA_CHECK(  m_muonTightsel->initialize() );

	std::string sys_name = "";

	elSFwei_map.insert (std::pair<std::string, float>(sys_name, 1.));
	muSFwei_map.insert (std::pair<std::string, float>(sys_name, 1.));
	globalSFwei_map.insert (std::pair<std::string, float>(sys_name, 1.));
	m_WeightPU_map.insert (std::pair<std::string, float>(sys_name, 1.));
	//trigwei_singlelep_map.insert (std::pair<std::string, float>(sys_name, 1.));
	//trigwei_dilep_map.insert (std::pair<std::string, float>(sys_name, 1.));
	trigwei_global_map.insert (std::pair<std::string, float>(sys_name, 1.));
	bTagSF_map.insert (std::pair<std::string, float>(sys_name, 1.));
	jvfSF_map.insert (std::pair<std::string, float>(sys_name, 1.));

	jet_pt_map.insert (std::pair<std::string, std::vector<float>*>(sys_name, jet_pt));
	jet_eta_map.insert(std::pair<std::string, std::vector<float>*>(sys_name, jet_eta));
	jet_phi_map.insert(std::pair<std::string, std::vector<float>*>(sys_name, jet_phi));
	jet_m_map.insert  (std::pair<std::string, std::vector<float>*>(sys_name, jet_m));
	jet_isB85_map.insert(std::pair<std::string, std::vector<bool>*> (sys_name, jet_isB85));
	jet_BtagWeight_map.insert(std::pair<std::string, std::vector<float>*> (sys_name, jet_BtagWeight));
	jet_passOR_map.insert(std::pair<std::string, std::vector<bool>*>(sys_name, jet_passOR));
	jet_JVT_map.insert(std::pair<std::string, std::vector<float>*> (sys_name, jet_JVT));
	labeljets_reco_map.insert(std::pair<std::string, std::vector<int>*>(sys_name, labeljets_reco));

	lep_pt_map.insert (std::pair<std::string, std::vector<float>*>(sys_name, lep_pt));
	lep_eta_map.insert(std::pair<std::string, std::vector<float>*>(sys_name, lep_eta));
	lep_phi_map.insert(std::pair<std::string, std::vector<float>*>(sys_name, lep_phi));
	lep_m_map.insert(std::pair<std::string, std::vector<float>*>(sys_name, lep_m));
	lep_ptvarcone30_map.insert(std::pair<std::string, std::vector<float>*>(sys_name, lep_ptvarcone30));
	lep_ptvarcone20_map.insert(std::pair<std::string, std::vector<float>*>(sys_name, lep_ptvarcone20));
	lep_topoetcone20_map.insert(std::pair<std::string, std::vector<float>*>(sys_name, lep_topoetcone20));
	lep_flav_map.insert (std::pair<std::string, std::vector<int>*>  (sys_name, lep_flav));
	lep_passOR_map.insert(std::pair<std::string, std::vector<bool>*>(sys_name, lep_passOR));
	lep_Signal_map.insert(std::pair<std::string, std::vector<bool>*>(sys_name, lep_isSignal));
	lep_Type_map.insert(std::pair<std::string, std::vector<int>*>(sys_name, lep_Type));
	lep_Origin_map.insert(std::pair<std::string, std::vector<int>*>(sys_name, lep_Origin));
	lep_HighPt_map.insert(std::pair<std::string, std::vector<bool>*>(sys_name, lep_HighPt));
	isTightlep_map.insert(std::pair<std::string, std::vector<bool>*>(sys_name, isTightlep));

	bkgTruthType_map.insert(std::pair<std::string, std::vector<int>*>(sys_name, bkgTruthType));
	bkgTruthOrigin_map.insert(std::pair<std::string, std::vector<int>*>(sys_name, bkgTruthOrigin));
	bkgMotherPdgId_map.insert(std::pair<std::string, std::vector<int>*>(sys_name, bkgMotherPdgId));
	firstEgMotherTruthType_map.insert(std::pair<std::string, std::vector<int>*>(sys_name, firstEgMotherTruthType));
	firstEgMotherTruthOrigin_map.insert(std::pair<std::string, std::vector<int>*>(sys_name, firstEgMotherTruthOrigin));
	firstEgMotherPdgId_map.insert(std::pair<std::string, std::vector<int>*>(sys_name, firstEgMotherPdgId));

	LepIsoFixedCutLoose_map.insert(std::pair<std::string, std::vector<bool>*>(sys_name, LepIsoFixedCutLoose));
	LepIsoFixedCutTight_map.insert(std::pair<std::string, std::vector<bool>*>(sys_name, LepIsoFixedCutTight));
	LepIsoFixedCutTightTrackOnly_map.insert(std::pair<std::string, std::vector<bool>*>(sys_name, LepIsoFixedCutTightTrackOnly));
	LepIsoGradient_map.insert(std::pair<std::string, std::vector<bool>*>(sys_name, LepIsoGradient));
	LepIsoGradientLoose_map.insert(std::pair<std::string, std::vector<bool>*>(sys_name, LepIsoGradientLoose));
	LepIsoLoose_map.insert(std::pair<std::string, std::vector<bool>*>(sys_name, LepIsoLoose));
	LepIsoLooseTrackOnly_map.insert(std::pair<std::string, std::vector<bool>*>(sys_name, LepIsoLooseTrackOnly));
	LepIsoFCLoose_map.insert(std::pair<std::string, std::vector<bool>*>(sys_name, LepIsoFCLoose));

	d0siglep_map.insert(std::pair<std::string, std::vector<float>*>(sys_name, d0siglep));
	z0lep_map.insert(std::pair<std::string, std::vector<float>*>(sys_name, z0lep));

	CleaningFlag.insert(std::pair<std::string, bool>(sys_name, false));
	met_map.insert(std::pair<std::string, float>(sys_name, 0.));
	met_phi_map.insert(std::pair<std::string, float>(sys_name, 0.));
	metEle_map.insert(std::pair<std::string, float>(sys_name, 0.));
	metEle_phi_map.insert(std::pair<std::string, float>(sys_name, 0.));
	metMu_map.insert(std::pair<std::string, float>(sys_name, 0.));
	metMu_phi_map.insert(std::pair<std::string, float>(sys_name, 0.));
	metJet_map.insert(std::pair<std::string, float>(sys_name, 0.));
	metJet_phi_map.insert(std::pair<std::string, float>(sys_name, 0.));
	metGam_map.insert(std::pair<std::string, float>(sys_name, 0.));
	metGam_phi_map.insert(std::pair<std::string, float>(sys_name, 0.));
	metSoft_map.insert(std::pair<std::string, float>(sys_name, 0.));
	metSoft_phi_map.insert(std::pair<std::string, float>(sys_name, 0.));
	met_sig_map.insert(std::pair<std::string, float>(sys_name, 0.));

	gamma_passOR_map.insert(std::pair<std::string, std::vector<bool>*>(sys_name, gamma_passOR));

	//Here I have to initialize all the trees
	for (const auto& sys : m_sysList) {

		const CP::SystematicSet& sysset = sys.systset;
		sys_name = sysset.name().c_str();

		if (sys.affectsKinematics || sys.affectsWeights ) {

		for (UInt_t itrig = 0; itrig < m_list_trig.size(); itrig++) {
			name_branch_matching =  m_list_trig.at(itrig) + "_match" + sys_name;
			isMatch = new std::vector<int>();
			trigger_matching_map.insert(std::pair<std::string, std::vector<int>*>(name_branch_matching.c_str(), isMatch));
		}

		bool syst_affectsPhotons = ST::testAffectsObject(xAOD::Type::Photon, sys.affectsType);
		bool syst_affectsElectrons = ST::testAffectsObject(xAOD::Type::Electron, sys.affectsType);
		bool syst_affectsMuons = ST::testAffectsObject(xAOD::Type::Muon, sys.affectsType);
		bool syst_affectsJets = ST::testAffectsObject(xAOD::Type::Jet, sys.affectsType);
		bool syst_affectsTaus = ST::testAffectsObject(xAOD::Type::Tau, sys.affectsType);
		bool syst_affectsBTag = ST::testAffectsObject(xAOD::Type::BTag, sys.affectsType);

		if ( syst_affectsPhotons && !syst_affectsElectrons  ) continue;
		if ( syst_affectsTaus ) continue;

		std::size_t found_PRW = sysset.name().find("PRW");
		//if ( found_PRW != std::string::npos ) continue;
		std::size_t found_Rtrk = sysset.name().find("JET_Rtrk");
		if ( found_Rtrk != std::string::npos ) continue;

		jet_pt = new std::vector<float>();
		jet_eta = new std::vector<float>();
		jet_phi = new std::vector<float>();
		jet_m = new std::vector<float>();
		jet_isB85 = new std::vector<bool>();
		jet_BtagWeight = new std::vector<float>();
		jet_JVT = new std::vector<float>();
		labeljets_reco = new std::vector<int>();
		jet_passOR = new std::vector<bool>();

		lep_pt = new std::vector<float>();
		lep_eta = new std::vector<float>();
		lep_phi = new std::vector<float>();
		lep_m = new std::vector<float>();
		lep_ptvarcone30 = new std::vector<float>();
		lep_ptvarcone20 = new std::vector<float>();
		lep_topoetcone20 = new std::vector<float>();
		lep_flav = new std::vector<int>();
		lep_Origin = new std::vector<int>();
		lep_Type = new std::vector<int>();
		bkgTruthType = new std::vector<int>();
		bkgTruthOrigin = new std::vector<int>();
		bkgMotherPdgId = new std::vector<int>();
		firstEgMotherTruthType = new std::vector<int>();
		firstEgMotherTruthOrigin = new std::vector<int>();
		firstEgMotherPdgId = new std::vector<int>();
		lep_passOR = new std::vector<bool>();
		lep_isSignal = new std::vector<bool>();
		lep_HighPt = new std::vector<bool>();
		isTightlep = new std::vector<bool>();
		LepIsoFixedCutLoose =  new std::vector<bool>();
		LepIsoFixedCutTight =  new std::vector<bool>();
		LepIsoFixedCutTightTrackOnly =  new std::vector<bool>();
		LepIsoGradient =  new std::vector<bool>();
		LepIsoGradientLoose =  new std::vector<bool>();
		LepIsoLoose =  new std::vector<bool>();
		LepIsoLooseTrackOnly =  new std::vector<bool>();
		LepIsoFCLoose = new std::vector<bool>();
		d0siglep = new std::vector<float>();
		z0lep = new std::vector<float>();

		if (sys.affectsKinematics) {
			newTree->Branch(bookName("cleaningVeto", sys_name), &CleaningFlag[sys_name]);
		}

		if (sys.affectsWeights && !(syst_affectsJets || syst_affectsElectrons || syst_affectsMuons || sys.affectsKinematics)){
      			newTree->Branch(bookName("WeightEventsPU", sys_name), &m_WeightPU_map[sys_name]);
	    	}

		if ( syst_affectsJets || syst_affectsBTag) {
			if (sys.affectsWeights) {
				if (syst_affectsBTag) {
					newTree->Branch( bookName("WeightEventsbTag", sys_name), &bTagSF_map[sys_name]);
				}
				else {
					newTree->Branch( bookName("WeightEventsJVT", sys_name), &jvfSF_map[sys_name]);
				}
			} 
			else {
				newTree->Branch( bookName("ptjets", sys_name), &jet_pt_map[sys_name]);
				newTree->Branch( bookName("etajets", sys_name), &jet_eta_map[sys_name]);
				newTree->Branch( bookName("phijets", sys_name), &jet_phi_map[sys_name]);
				newTree->Branch( bookName("massjets", sys_name), &jet_m_map[sys_name]);
				newTree->Branch( bookName("isB85jets", sys_name), &jet_isB85_map[sys_name]);
				newTree->Branch( bookName("BtagWeightjets", sys_name), &jet_BtagWeight_map[sys_name]);
				newTree->Branch( bookName("JVTjets", sys_name), &jet_JVT_map[sys_name]);
				newTree->Branch( bookName("passORjet", sys_name), &jet_passOR_map[sys_name]);
				newTree->Branch( bookName("WeightEventsbTag", sys_name), &bTagSF_map[sys_name]);
				newTree->Branch( bookName("WeightEventsJVT", sys_name), &jvfSF_map[sys_name]);
				newTree->Branch( bookName("passORlep", sys_name), &lep_passOR_map[sys_name]);
			}
		}

		if ( syst_affectsElectrons || syst_affectsMuons ) {
			if (sys.affectsWeights) {
				if ( syst_affectsElectrons ) newTree->Branch( bookName("WeightEventselSF", sys_name) , &elSFwei_map[sys_name]);
				if ( syst_affectsMuons ) newTree->Branch( bookName("WeightEventsmuSF", sys_name) , &muSFwei_map[sys_name]);
				if ( syst_affectsElectrons || syst_affectsMuons ) newTree->Branch( bookName("WeightEventsSF_global", sys_name) , &globalSFwei_map[sys_name]);
				//newTree->Branch( bookName("WeightEvents_trigger_single", sys_name) , &trigwei_singlelep_map[sys_name]);
				//newTree->Branch( bookName("WeightEvents_trigger_dilep", sys_name) , &trigwei_dilep_map[sys_name]);
				//newTree->Branch( bookName("WeightEvents_trigger_mixlep", sys_name) , &trigwei_mixlep_map[sys_name]);
				newTree->Branch( bookName("WeightEvents_trigger_global", sys_name) , &trigwei_global_map[sys_name]);
			} 
			else {
				newTree->Branch( bookName("ptleptons", sys_name), &lep_pt_map[sys_name]);
				newTree->Branch( bookName("etaleptons", sys_name), &lep_eta_map[sys_name]);
				newTree->Branch( bookName("phileptons", sys_name), &lep_phi_map[sys_name]);
				newTree->Branch( bookName("massleptons", sys_name), &lep_m_map[sys_name]);
				newTree->Branch( bookName("flavleptons", sys_name), &lep_flav_map[sys_name]);
				newTree->Branch( bookName("passORlep", sys_name), &lep_passOR_map[sys_name]);
				newTree->Branch( bookName("isSignallep", sys_name), &lep_Signal_map[sys_name]);
				newTree->Branch( bookName("Originlep", sys_name), &lep_Origin_map[sys_name]);

				newTree->Branch( bookName("isTightlep", sys_name), &isTightlep_map[sys_name]);
				newTree->Branch( bookName("LepIsoFixedCutTight", sys_name), &LepIsoFixedCutTight_map[sys_name] );
				newTree->Branch( bookName("LepIsoFixedCutTightTrackOnly", sys_name), &LepIsoFixedCutTightTrackOnly_map[sys_name] );
				newTree->Branch( bookName("LepIsoGradientLoose", sys_name), &LepIsoGradientLoose_map[sys_name] );
				newTree->Branch( bookName("LepIsoFCLoose", sys_name), &LepIsoFCLoose_map[sys_name] );
				newTree->Branch( bookName("d0sig", sys_name), &d0siglep_map[sys_name]);
				newTree->Branch( bookName("z0sinTheta", sys_name), &z0lep_map[sys_name]);

				newTree->Branch( bookName("passORjet", sys_name), &jet_passOR_map[sys_name]);
				newTree->Branch( bookName("WeightEventselSF", sys_name) , &elSFwei_map[sys_name]);
				newTree->Branch( bookName("WeightEventsmuSF", sys_name) , &muSFwei_map[sys_name]);
				newTree->Branch( bookName("WeightEventsSF_global", sys_name) , &globalSFwei_map[sys_name]);
				//newTree->Branch( bookName("WeightEvents_trigger_single", sys_name) , &trigwei_singlelep_map[sys_name]);
				//newTree->Branch( bookName("WeightEvents_trigger_dilep", sys_name) , &trigwei_dilep_map[sys_name]);
				//newTree->Branch( bookName("WeightEvents_trigger_mixlep", sys_name) , &trigwei_mixlep_map[sys_name]);
				newTree->Branch( bookName("WeightEvents_trigger_global", sys_name) , &trigwei_global_map[sys_name]);

				for (UInt_t i = 0; i < m_list_trig.size(); i++) {

					std::size_t found_XE = m_list_trig.at(i).find("HLT_xe");
					if ( found_XE != std::string::npos ) continue;

					name_branch_matching =  m_list_trig.at(i) + "_match" + sys_name;
					//newTree->Branch(name_branch_matching.c_str(), &trigger_matching_map[name_branch_matching]);

				}
			}
		}

		if (sys.affectsKinematics) {
			newTree->Branch( bookName("EtMiss_tst", sys_name), &met_map[sys_name]);
			newTree->Branch( bookName("EtMiss_tstPhi", sys_name), &met_phi_map[sys_name]);
			newTree->Branch( bookName("EtMiss_Ele", sys_name), &metEle_map[sys_name]);
			newTree->Branch( bookName("EtMiss_ElePhi", sys_name), &metEle_phi_map[sys_name]);
			newTree->Branch( bookName("EtMiss_Mu", sys_name), &metMu_map[sys_name]);
			newTree->Branch( bookName("EtMiss_MuPhi", sys_name), &metMu_phi_map[sys_name]);
			newTree->Branch( bookName("EtMiss_Jet", sys_name), &metJet_map[sys_name]);
			newTree->Branch( bookName("EtMiss_JetPhi", sys_name), &metJet_phi_map[sys_name]);
			newTree->Branch( bookName("EtMiss_Gam", sys_name), &metGam_map[sys_name]);
			newTree->Branch( bookName("EtMiss_GamPhi", sys_name), &metGam_phi_map[sys_name]);
			newTree->Branch( bookName("EtMiss_Soft", sys_name), &metSoft_map[sys_name]);
			newTree->Branch( bookName("EtMiss_SoftPhi", sys_name), &metSoft_phi_map[sys_name]);
			newTree->Branch( bookName("EtMiss_significance", sys_name), &met_sig_map[sys_name]);		
		}

		elSFwei_map.insert (std::pair<std::string, float>(sys_name, 1.));
		muSFwei_map.insert (std::pair<std::string, float>(sys_name, 1.));
		globalSFwei_map.insert (std::pair<std::string, float>(sys_name, 1.));
		//trigwei_singlelep_map.insert (std::pair<std::string, float>(sys_name, 1.));
		//trigwei_dilep_map.insert (std::pair<std::string, float>(sys_name, 1.));
		//trigwei_mixlep_map.insert (std::pair<std::string, float>(sys_name, 1.));
		trigwei_global_map.insert (std::pair<std::string, float>(sys_name, 1.));
		bTagSF_map.insert (std::pair<std::string, float>(sys_name, 1.));
		jvfSF_map.insert (std::pair<std::string, float>(sys_name, 1.));

		jet_pt_map.insert (std::pair<std::string, std::vector<float>*>(sys_name, jet_pt));
		jet_eta_map.insert(std::pair<std::string, std::vector<float>*>(sys_name, jet_eta));
		jet_phi_map.insert(std::pair<std::string, std::vector<float>*>(sys_name, jet_phi));
		jet_m_map.insert  (std::pair<std::string, std::vector<float>*>(sys_name, jet_m));
		jet_isB85_map.insert(std::pair<std::string, std::vector<bool>*> (sys_name, jet_isB85));
		jet_BtagWeight_map.insert(std::pair<std::string, std::vector<float>*> (sys_name, jet_BtagWeight));
		jet_JVT_map.insert(std::pair<std::string, std::vector<float>*> (sys_name, jet_JVT));
		jet_passOR_map.insert(std::pair<std::string, std::vector<bool>*>(sys_name, jet_passOR));
		labeljets_reco_map.insert(std::pair<std::string, std::vector<int>*>(sys_name, labeljets_reco));

		lep_pt_map.insert (std::pair<std::string, std::vector<float>*>(sys_name, lep_pt));
		lep_eta_map.insert(std::pair<std::string, std::vector<float>*>(sys_name, lep_eta));
		lep_phi_map.insert(std::pair<std::string, std::vector<float>*>(sys_name, lep_phi));
		lep_m_map.insert(std::pair<std::string, std::vector<float>*>(sys_name, lep_m));
		lep_ptvarcone30_map.insert(std::pair<std::string, std::vector<float>*>(sys_name, lep_ptvarcone30));
		lep_ptvarcone20_map.insert(std::pair<std::string, std::vector<float>*>(sys_name, lep_ptvarcone20));
		lep_topoetcone20_map.insert(std::pair<std::string, std::vector<float>*>(sys_name, lep_topoetcone20));
		lep_flav_map.insert (std::pair<std::string, std::vector<int>*>  (sys_name, lep_flav));
		lep_passOR_map.insert(std::pair<std::string, std::vector<bool>*>(sys_name, lep_passOR));
		lep_Signal_map.insert(std::pair<std::string, std::vector<bool>*>(sys_name, lep_isSignal));
		lep_Type_map.insert(std::pair<std::string, std::vector<int>*>(sys_name, lep_Type));
		lep_Origin_map.insert(std::pair<std::string, std::vector<int>*>(sys_name, lep_Origin));
		lep_HighPt_map.insert(std::pair<std::string, std::vector<bool>*>(sys_name, lep_HighPt));
		isTightlep_map.insert(std::pair<std::string, std::vector<bool>*>(sys_name, isTightlep));

		bkgTruthType_map.insert(std::pair<std::string, std::vector<int>*>(sys_name, bkgTruthType));
		bkgTruthOrigin_map.insert(std::pair<std::string, std::vector<int>*>(sys_name, bkgTruthOrigin));
		bkgMotherPdgId_map.insert(std::pair<std::string, std::vector<int>*>(sys_name, bkgMotherPdgId));
		firstEgMotherTruthType_map.insert(std::pair<std::string, std::vector<int>*>(sys_name, firstEgMotherTruthType));
		firstEgMotherTruthOrigin_map.insert(std::pair<std::string, std::vector<int>*>(sys_name, firstEgMotherTruthOrigin));
		firstEgMotherPdgId_map.insert(std::pair<std::string, std::vector<int>*>(sys_name, firstEgMotherPdgId));

		LepIsoFixedCutLoose_map.insert(std::pair<std::string, std::vector<bool>*>(sys_name, LepIsoFixedCutLoose));
		LepIsoFixedCutTight_map.insert(std::pair<std::string, std::vector<bool>*>(sys_name, LepIsoFixedCutTight));
		LepIsoFixedCutTightTrackOnly_map.insert(std::pair<std::string, std::vector<bool>*>(sys_name, LepIsoFixedCutTightTrackOnly));
		LepIsoGradient_map.insert(std::pair<std::string, std::vector<bool>*>(sys_name, LepIsoGradient));
		LepIsoGradientLoose_map.insert(std::pair<std::string, std::vector<bool>*>(sys_name, LepIsoGradientLoose));
		LepIsoLoose_map.insert(std::pair<std::string, std::vector<bool>*>(sys_name, LepIsoLoose));
		LepIsoLooseTrackOnly_map.insert(std::pair<std::string, std::vector<bool>*>(sys_name, LepIsoLooseTrackOnly));
		LepIsoFCLoose_map.insert(std::pair<std::string, std::vector<bool>*>(sys_name, LepIsoFCLoose));

		d0siglep_map.insert(std::pair<std::string, std::vector<float>*>(sys_name, d0siglep));
		z0lep_map.insert(std::pair<std::string, std::vector<float>*>(sys_name, z0lep));

		CleaningFlag.insert(std::pair<std::string, bool>(sys_name, false));
		met_map.insert(std::pair<std::string, float>(sys_name, 0.));
		met_phi_map.insert(std::pair<std::string, float>(sys_name, 0.));
		metEle_map.insert(std::pair<std::string, float>(sys_name, 0.));
		metEle_phi_map.insert(std::pair<std::string, float>(sys_name, 0.));
		metMu_map.insert(std::pair<std::string, float>(sys_name, 0.));
		metMu_phi_map.insert(std::pair<std::string, float>(sys_name, 0.));
		metJet_map.insert(std::pair<std::string, float>(sys_name, 0.));
		metJet_phi_map.insert(std::pair<std::string, float>(sys_name, 0.));
		metGam_map.insert(std::pair<std::string, float>(sys_name, 0.));
		metGam_phi_map.insert(std::pair<std::string, float>(sys_name, 0.));
		metSoft_map.insert(std::pair<std::string, float>(sys_name, 0.));
		metSoft_phi_map.insert(std::pair<std::string, float>(sys_name, 0.));
		met_sig_map.insert(std::pair<std::string, float>(sys_name, 0.));

		}
	}

	return StatusCode::SUCCESS;
}

bool OutTree::processEmpty() { //xAOD::TEvent*& event, xAOD::ElectronContainer*& electrons, xAOD::PhotonContainer*& photons, xAOD::MuonContainer*& muons, xAOD::JetContainer*& jets, std::string sysname) {
	const char *APP_NAME = "processEmpty()";

	bool m_debug = false;
	std::string sysname = "";
	//Setting up a barebone output tree
	jet_pt_map[sysname]->clear();
	jet_eta_map[sysname]->clear();
	jet_phi_map[sysname]->clear();
	jet_m_map[sysname]->clear();
	jet_isB85_map[sysname]->clear();
	jet_BtagWeight_map[sysname]->clear();
	if (m_btagWP && sysname == "") {
		jet_isB70->clear();
		jet_isB85->clear();
		jet_isB70Flat->clear();
		jet_isB77Flat->clear();
		jet_isB85Flat->clear();
	}
	if (m_ISDATA) {
         	TM_2e24_lhvloose_nod0_map[sysname]->clear(); 
        	TM_mu22_mu8noL1_map[sysname]->clear();
        	TM_e17_lhloose_nod0_mu14_map[sysname]->clear();
        	TM_2e15_lhvloose_nod0_L12EM13VH_map[sysname]->clear();
        	TM_2e17_lhvloose_nod0_map[sysname]->clear();
        	TM_mu20_mu8noL1_map[sysname]->clear();
        	TM_2e12_lhloose_L12EM10VH_map[sysname]->clear();
        	TM_mu18_mu8noL1_map[sysname]->clear();
        	TM_e17_lhloose_mu14_map[sysname]->clear();
	}
	jet_passOR_map[sysname]->clear();
	labeljets_reco_map[sysname]->clear();
	if (m_savephotons) {
		gamma_pt->clear();
		gamma_eta->clear();
		gamma_phi->clear();
		gamma_passOR->clear();
		gamma_passOR_map[sysname]->clear();
	}

	lep_pt_map[sysname]->clear();
	lep_eta_map[sysname]->clear();
	lep_phi_map[sysname]->clear();
	lep_m_map[sysname]->clear();
	lep_ptvarcone30_map[sysname]->clear();
	lep_ptvarcone20_map[sysname]->clear();
	lep_topoetcone20_map[sysname]->clear();
	lep_flav_map[sysname]->clear();
	lep_passOR_map[sysname]->clear();
	lep_Signal_map[sysname]->clear();
	lep_passOR_map[sysname]->clear();
	lep_HighPt_map[sysname]->clear();
	lep_Origin_map[sysname]->clear();
	lep_Type_map[sysname]->clear();

	bkgTruthType_map[sysname]->clear();
	bkgTruthOrigin_map[sysname]->clear();
	bkgMotherPdgId_map[sysname]->clear();
	firstEgMotherTruthType_map[sysname]->clear();
	firstEgMotherTruthOrigin_map[sysname]->clear();
	firstEgMotherPdgId_map[sysname]->clear();

	d0siglep_map[sysname]->clear();
	z0lep_map[sysname]->clear();

	LepIsoFixedCutTight_map[sysname]->clear();
	LepIsoFixedCutTightTrackOnly_map[sysname]->clear();
	LepIsoGradient_map[sysname]->clear();
	LepIsoGradientLoose_map[sysname]->clear();
	LepIsoLoose_map[sysname]->clear();
	LepIsoLooseTrackOnly_map[sysname]->clear();
	LepIsoFCLoose_map[sysname]->clear();

	if (1) {
		if (m_savephotons) {
			gammaIsoFixedCutLoose->clear();
			gammaIsoFixedCutTight->clear();
		}
	}

	for (UInt_t i = 0; i < m_list_trig.size(); i++) {
		name_branch_matching =  m_list_trig.at(i) + "_match" + sysname;
		if ( m_debug ) std::cout << "  " << name_branch_matching << std::endl;
		trigger_matching_map[name_branch_matching]->clear();
	}

	if (m_dotruth && !m_ISDATA) {
		mc_eta->clear();
		mc_pt->clear();
		mc_mass->clear();
		mc_phi->clear();
		mc_pdgid->clear();
		mc_parent_pdgid->clear();
		ptjets_truth->clear();
		etajets_truth->clear();
		phijets_truth->clear();
		massjets_truth->clear();
		labeljets_truth->clear();
	}


	//   if (sysname="") {

	m_EventNumber = 0;
	m_PrimVx = 0;
	m_LumiBlockNumber = 0;//
	m_crossSection = 0; // eventInfo->auxdata<double>("CrossSection");
	m_passMETtrig = 0; // eventInfo->auxdata<bool>("passMETtrigger");

	if ( !m_ISDATA ) {
		m_RunNumber = 0; // eventInfo->auxdata<int>("RndRunNum");
		m_decayType = 0; // eventInfo->auxdata<int>("DecayType");
		m_HFclass = 0; // eventInfo->auxdata<int>("HFclass");
		m_GenFiltHT = 0; // eventInfo->auxdata<float>("GenFiltHT");
		m_GenFiltMET = 0; // eventInfo->auxdata<float>("GenFiltMET");
		m_WeightEvents = 0; // (eventInfo->mcEventWeights()).at(0) * eventInfo->auxdata<float>("Sherpa_weight");

		if ( m_multiweight ) {
			m_WeightEvents_MUR05_MUF1 = 0; // eventInfo->mcEventWeights().at(5); // MUR05_MUF1
			m_WeightEvents_MUR1_MUF2 = 0; // eventInfo->mcEventWeights().at(8); // MUR1_MUF2
			m_WeightEvents_MUR2_MUF1 = 0; // eventInfo->mcEventWeights().at(9); // MUR2_MUF1
			m_WeightEvents_MUR1_MUF05 = 0; // eventInfo->mcEventWeights().at(6); // "MUR1_MUF0.5
			m_WeightEvents_MUR1_MUF2 = 0; // eventInfo->mcEventWeights().at(1); // MUR1_MUF2
			m_WeightEvents_MUR05_MUF1 = 0; // eventInfo->mcEventWeights().at(6); // MUR05_MUF1
			m_WeightEvents_MUR1_MUF05 = 0; // eventInfo->mcEventWeights().at(2); // MUR1_MUF05
			m_WeightEvents_MUR2_MUF1 = 0; // eventInfo->mcEventWeights().at(3); // MUR2_MUF1
		}

		//m_WeightPU = 0; // eventInfo->auxdata< float >( "PileupWeight" );
		m_mu_av = 0; // eventInfo->averageInteractionsPerCrossing();
	}
	else {
		m_RunNumber = 0;//eventInfo->runNumber();
		m_decayType = 0; // -1;
		m_HFclass = 0; // -1;
		m_GenFiltHT = 0; // -1;
		m_GenFiltMET = 0;//-1;
		m_WeightEvents = 0;//1;
		//m_WeightPU = 0; // 1;
		m_mu_av = 0; // eventInfo->auxdata< float >( "corrected_averageInteractionsPerCrossing" );
	}

	//}

	//  std::vector<bool> trig_pass;// = 0;//eventInfo->auxdata<std::vector<bool>>("TrigPass");

	//  for (UInt_t itrig = 0; itrig < m_list_trig.size(); itrig++) {
	//    if ( trig_pass.at(itrig)) {
	//      trigger_map[m_list_trig.at(itrig)] = 0;//1;
	//    }
	//    else {
	//      trigger_map[m_list_trig.at(itrig)] = 0;
	//    }
	//  }
	//////////////////////////////////////////////
	if ( !m_ISDATA ) {
		elSFwei_map[""] = 0; // eventInfo->auxdata<float>("eleSF_weight");
		muSFwei_map[""] = 0; // eventInfo->auxdata<float>("muSF_weight");
		globalSFwei_map[""] =0 ;
		//trigwei_singlelep_map[""] = 0; // eventInfo->auxdata< double >("trig_SF_single_e") * eventInfo->auxdata< double >("trig_SF_single_m");
		//trigwei_dilep_map[""] = 0; // eventInfo->auxdata< double >("trig_SF_double_e") * eventInfo->auxdata< double >("trig_SF_double_m");
		//trigwei_mixlep_map[""] = 0; // eventInfo->auxdata< double >("trig_SF_mix_e") * eventInfo->auxdata< double >("trig_SF_mix_m");
		trigwei_global_map[""]= 0;
		bTagSF_map[""] = 0; // eventInfo->auxdata<float>("bTag_weight");
		jvfSF_map[""] = 0; // eventInfo->auxdata<float>("JVT_weight");
		m_WeightPU_map[""]=0;
	}
	else {
		elSFwei_map[""] = 0; // 1.;
		muSFwei_map[""] = 0; // 1.;
		globalSFwei_map[""] =0 ;
		//trigwei_singlelep_map[""] = 0; // 1.;
		//trigwei_dilep_map[""] = 0; // 1.;
		//trigwei_mixlep_map[""] = 0; // 1.;
		trigwei_global_map[""]= 0;
		bTagSF_map[""] = 0; // 1.;
		jvfSF_map[""] = 0; // 1.;
		m_WeightPU_map[""]=0;
	}

	return true;
}

bool OutTree::process(xAOD::TEvent*& event, xAOD::ElectronContainer*& electrons, xAOD::PhotonContainer*& photons, xAOD::MuonContainer*& muons, xAOD::JetContainer*& jets, std::string sysname) {

	const char *APP_NAME = "process()";

	bool m_debug = false;

	if ( m_debug ) std::cout << "Syst " << sysname << std::endl;
	if ( m_debug ) std::cout << "Clearing jets" << std::endl;

	//Setting up a barebone output tree
	jet_pt_map[sysname]->clear();
	jet_eta_map[sysname]->clear();
	jet_phi_map[sysname]->clear();
	jet_m_map[sysname]->clear();
	jet_isB85_map[sysname]->clear();
	jet_BtagWeight_map[sysname]->clear();
	jet_JVT_map[sysname]->clear();
	if (m_btagWP && sysname == "") {
		jet_isB70->clear();
		jet_isB85->clear();
		jet_isB70Flat->clear();
		jet_isB77Flat->clear();
		jet_isB85Flat->clear();
	}
	if (m_ISDATA) {
         	TM_2e24_lhvloose_nod0_map[sysname]->clear(); 
        	TM_mu22_mu8noL1_map[sysname]->clear();
        	TM_e17_lhloose_nod0_mu14_map[sysname]->clear();
        	TM_2e15_lhvloose_nod0_L12EM13VH_map[sysname]->clear();
        	TM_2e17_lhvloose_nod0_map[sysname]->clear();
        	TM_mu20_mu8noL1_map[sysname]->clear();
        	TM_2e12_lhloose_L12EM10VH_map[sysname]->clear();
        	TM_mu18_mu8noL1_map[sysname]->clear();
        	TM_e17_lhloose_mu14_map[sysname]->clear();
	}
	jet_passOR_map[sysname]->clear();
	labeljets_reco_map[sysname]->clear();
	if (m_savephotons) {
		gamma_pt->clear();
		gamma_eta->clear();
		gamma_phi->clear();
		gamma_passOR->clear();
		gamma_passOR_map[sysname]->clear();
	}

	if ( m_debug ) std::cout << "Clearing leptons" << std::endl;

	lep_pt_map[sysname]->clear();
	lep_eta_map[sysname]->clear();
	lep_phi_map[sysname]->clear();
	lep_m_map[sysname]->clear();
	lep_ptvarcone30_map[sysname]->clear();
	lep_ptvarcone20_map[sysname]->clear();
	lep_topoetcone20_map[sysname]->clear();
	lep_flav_map[sysname]->clear();
	lep_passOR_map[sysname]->clear();
	lep_Signal_map[sysname]->clear();
	lep_passOR_map[sysname]->clear();
	lep_HighPt_map[sysname]->clear();
	lep_Origin_map[sysname]->clear();
	lep_Type_map[sysname]->clear();

	if ( m_debug ) std::cout << "Clearing truth mothers" << std::endl;

	bkgTruthType_map[sysname]->clear();
	bkgTruthOrigin_map[sysname]->clear();
	bkgMotherPdgId_map[sysname]->clear();
	firstEgMotherTruthType_map[sysname]->clear();
	firstEgMotherTruthOrigin_map[sysname]->clear();
	firstEgMotherPdgId_map[sysname]->clear();

	if ( m_debug ) std::cout << "Clearing ID" << std::endl;
	isTightlep_map[sysname]->clear();
	d0siglep_map[sysname]->clear();
	z0lep_map[sysname]->clear();

	if ( m_debug ) std::cout << "Clearing iso" << std::endl;
	LepIsoFixedCutLoose_map[sysname]->clear();
	LepIsoFixedCutTight_map[sysname]->clear();
	LepIsoFixedCutTightTrackOnly_map[sysname]->clear();
	LepIsoGradient_map[sysname]->clear();
	LepIsoGradientLoose_map[sysname]->clear();
	LepIsoLoose_map[sysname]->clear();
	LepIsoLooseTrackOnly_map[sysname]->clear();
	LepIsoFCLoose_map[sysname]->clear();

	if (sysname == "") {
		if (m_savephotons) {
		gammaIsoFixedCutLoose->clear();
		gammaIsoFixedCutTight->clear();
		}
	}

	if ( m_debug ) std::cout << "Clearing trigger" << std::endl;

	for (UInt_t i = 0; i < m_list_trig.size(); i++) {
		name_branch_matching =  m_list_trig.at(i) + "_match" + sysname;
		if ( m_debug ) std::cout << "  " << name_branch_matching << std::endl;
		trigger_matching_map[name_branch_matching]->clear();
	}

	if ( m_debug ) std::cout << "Towards the event" << std::endl;

	if (sysname == "" && !m_ISDATA) {
		// PDF INFO!!!
		// Get the Truth Event information from the event:
		const xAOD::TruthEventContainer* truthE = 0;
		if(!event->retrieve( truthE, "TruthEvents" ).isSuccess()) {
			Warning( APP_NAME, "Cannot retrieve TruthEvent container !" );
			return false;
		}

		xAOD::TruthEventContainer::const_iterator truthE_itr = truthE->begin();
		xAOD::TruthEventContainer::const_iterator truthE_end = truthE->end();
		for ( ; truthE_itr != truthE_end; ++truthE_itr ) {

			if ((*truthE_itr)->isAvailable< float >( "X1" ) ) {
				TruthX1 = (*truthE_itr)->auxdata<float>("X1");
				TruthX2 = (*truthE_itr)->auxdata<float>("X2");
				TruthXF1 = (*truthE_itr)->auxdata<float>("XF1");
				TruthXF2 = (*truthE_itr)->auxdata<float>("XF2");
				TruthQ   = (*truthE_itr)->auxdata<float>("Q");
				TruthPDGID1 = (*truthE_itr)->auxdata<int>("PDGID1");
				TruthPDGID2 = (*truthE_itr)->auxdata<int>("PDGID2");
				TruthPDFID1 = (*truthE_itr)->auxdata<int>("PDFID1");
				TruthPDFID2 = (*truthE_itr)->auxdata<int>("PDFID2");
			} 
			else { // Somehow it does not exist
				TruthX1     = -1;
				TruthX2     = -1;
				TruthXF1    = -1;
				TruthXF2    = -1;
				TruthQ      = -1;
				TruthPDGID1 = -1;
				TruthPDGID2 = -1;
				TruthPDFID1 = -1;
				TruthPDFID2 = -1;
			}
		}
	}

	if (m_dotruth && sysname == "" && !m_ISDATA) {
		mc_eta->clear();
		mc_pt->clear();
		mc_mass->clear();
		mc_phi->clear();
		mc_pdgid->clear();
		mc_parent_pdgid->clear();

		ptjets_truth->clear();
		etajets_truth->clear();
		phijets_truth->clear();
		massjets_truth->clear();
		labeljets_truth->clear();

		int parent = -1;

		//Retrieve truth particles
		const xAOD::TruthParticleContainer* truth_particles(nullptr);
		if (!event->retrieve(truth_particles, "TruthParticles").isSuccess()) {
			Warning( APP_NAME, "Cannot retrieve TruthParticles containers !" );
			return false;
		}

		for ( const auto& truthP_itr : *truth_particles) {
			bool save = false;

			if (m_isSherpa) {
				//Electrons
				if (truthP_itr->pt() > 7000. && truthP_itr->status() == 3 && truthP_itr->auxdata<unsigned int>("classifierParticleType") == 21 && fabs(truthP_itr->pdgId()) == 11 ) {
					if (truthP_itr->nParents() == 1)parent = truthP_itr->parent(0)->pdgId();
					else parent = -1;
					save = true;
				}
				//Muons
				if (truthP_itr->pt() > 5000. && truthP_itr->status() == 3 && truthP_itr->auxdata<unsigned int>("classifierParticleType") == 21 && fabs(truthP_itr->pdgId()) == 13 ) {
					if (truthP_itr->nParents() == 1)parent = truthP_itr->parent(0)->pdgId();
					else parent = -1;
					save = true;
				}
			}
			else {
				//Electrons
				if (truthP_itr->pt() > 7000. && truthP_itr->status() == 1 && fabs(truthP_itr->pdgId()) == 11) {
					if (truthP_itr->nParents() == 1)parent = truthP_itr->parent(0)->pdgId();
					else parent = -1;
					for (unsigned int nparents = 0; nparents < truthP_itr->nParents(); ++nparents) {
						if (truthP_itr->parent(nparents)) {
							if ((fabs(truthP_itr->parent(nparents)->pdgId()) == 15) || (fabs(truthP_itr->parent(nparents)->pdgId()) >= 23 &&
							fabs(truthP_itr->parent(nparents)->pdgId()) <= 25)) {
								save = true;
							}
						}
					}
				}
				//Muons
				if (truthP_itr->pt() > 5000. && truthP_itr->status() == 1 && fabs(truthP_itr->pdgId()) == 13) {
					if (truthP_itr->nParents() == 1)parent = truthP_itr->parent(0)->pdgId();
					else parent = -1;
						for (unsigned int nparents = 0; nparents < truthP_itr->nParents(); ++nparents) {
							if (truthP_itr->parent(nparents)) {
							if ((fabs(truthP_itr->parent(nparents)->pdgId()) == 15) ||
							(fabs(truthP_itr->parent(nparents)->pdgId()) >= 23 && fabs(truthP_itr->parent(nparents)->pdgId()) <= 25)) {
								save = true;
							}
						}
					}
				}
			}

			// B - take only bot from W Z H + SUSY PARTICLES and TOP
			if (truthP_itr->pt() > 15000. && fabs(truthP_itr->pdgId()) == 5) {
				if (truthP_itr->nParents() == 1)parent = truthP_itr->parent(0)->pdgId();
				else parent = -1;
				for (unsigned int nparents = 0; nparents < truthP_itr->nParents(); ++nparents) {
					if (truthP_itr->parent(nparents)) {
						if ((fabs(truthP_itr->parent(nparents)->pdgId()) >= 1000001 && fabs(truthP_itr->parent(nparents)->pdgId()) <= 2000016) ||
						(fabs(truthP_itr->parent(nparents)->pdgId()) >= 23 && fabs(truthP_itr->parent(nparents)->pdgId()) <= 25) ||
						(fabs(truthP_itr->parent(nparents)->pdgId()) == 6)) {
							save = true;
						}
					}
				}
			}

			// TOP - take all
			if (truthP_itr->pt() > 9000. && fabs(truthP_itr->pdgId()) == 6) {
				if (truthP_itr->nParents() == 1)parent = truthP_itr->parent(0)->pdgId();
				else parent = -1;
				for (unsigned int nparents = 0; nparents < truthP_itr->nParents(); ++nparents) {
					if (truthP_itr->parent(nparents)) {
						if (truthP_itr->parent(nparents)->pdgId() != truthP_itr->pdgId()) {
							save = true;
						}
					}
				}
			}

			// Vector Boson (W,Z,H)
			if (truthP_itr->pt() > 9000. && fabs(truthP_itr->pdgId()) >= 23 && fabs(truthP_itr->pdgId()) <= 25) {
				if (truthP_itr->nParents() == 1)parent = truthP_itr->parent(0)->pdgId();
				else parent = -1;
				for (unsigned int nparents = 0; nparents < truthP_itr->nParents(); ++nparents) {
					if (truthP_itr->parent(nparents)) {
						if ((truthP_itr->parent(nparents)->pdgId() != truthP_itr->pdgId()) && truthP_itr->p4().M() > 40000.) {
							save = true;
						}
					}
				}
			}

			// SUSY!
			if (truthP_itr->pt() > 9000. && fabs(truthP_itr->pdgId()) >= 1000001 && fabs(truthP_itr->pdgId()) <= 2000016) save = true;

			if (save == true) {
				mc_eta->push_back(truthP_itr->eta());
				mc_pt->push_back(truthP_itr->pt());
				mc_mass->push_back(truthP_itr->p4().M());
				mc_phi->push_back(truthP_itr->phi());
				mc_pdgid->push_back(truthP_itr->pdgId());
				mc_parent_pdgid->push_back(parent);
			}
		}

		//Retrieve the truth JETS
		const xAOD::JetContainer* truthJets(nullptr);
		if (!event->retrieve(truthJets, "AntiKt4TruthJets").isSuccess()) {
			Warning( APP_NAME, "Cannot retrieve TruthJets containers !" );
			return false;
		}
		for (const auto& truthJ_itr : *truthJets) {
			if (truthJ_itr->pt() > 20000. && fabs(truthJ_itr->eta()) < 4.5) {
				ptjets_truth->push_back(truthJ_itr->pt());
				etajets_truth->push_back(truthJ_itr->eta());
				phijets_truth->push_back(truthJ_itr->phi());
				massjets_truth->push_back(truthJ_itr->m());
				labeljets_truth->push_back(truthJ_itr->auxdata<int>("PartonTruthLabelID"));
			}
		}

		//Retrieve the truth MET
		const xAOD::MissingETContainer* truth_metcontainer(nullptr);
		ANA_CHECK( event->retrieve( truth_metcontainer, "MET_Truth" ));
		const xAOD::MissingET* met_nonint = (*truth_metcontainer)["NonInt"];
		float EtmissTruth_Etx = met_nonint->mpx();
		float EtmissTruth_Ety = met_nonint->mpy();
		float EtmissTruth_Et = sqrt(EtmissTruth_Etx * EtmissTruth_Etx + EtmissTruth_Ety * EtmissTruth_Ety);
		TLorentzVector MET_Truth(EtmissTruth_Etx, EtmissTruth_Ety, 0., EtmissTruth_Et);

		EtMiss_truth = MET_Truth.Perp();
		EtMiss_truthPhi = MET_Truth.Phi();
	}

	//Setting up some basic filtering rule
	int n_ele10 = 0;
	int n_mu10 = 0;
	int n_sigmu = 0;
	int n_sigele = 0;
	int n_ele4 = 0;
	int n_mu5 = 0;
	int n_gamma40 = 0;
	int n_bjets = 0;
	int n_poslep = 0;
	int n_neglep = 0;

	const xAOD::EventInfo *eventInfo(nullptr);
	if ( !event->retrieve( eventInfo, "EventInfo").isSuccess() ) {
		Warning(APP_NAME, "Failed to retrieve event info collection. Exiting." );
		return false;
	}

	CleaningFlag[sysname] = eventInfo->auxdata<bool>("cleaning_veto");

	if (sysname == "") {

		m_EventNumber = eventInfo->eventNumber();
		m_PrimVx = eventInfo->auxdata<int>("NumPrimaryVertices");
		m_LumiBlockNumber = eventInfo->lumiBlock();
		m_crossSection = eventInfo->auxdata<double>("CrossSection");
		m_passMETtrig = eventInfo->auxdata<bool>("passMETtrigger");

		if ( !m_ISDATA ) {
			m_RunNumber = eventInfo->auxdata<int>("RndRunNum");
			m_decayType = eventInfo->auxdata<int>("DecayType");
			m_HFclass = eventInfo->auxdata<int>("HFclass");
			m_GenFiltHT = eventInfo->auxdata<float>("GenFiltHT");
			m_GenFiltMET = eventInfo->auxdata<float>("GenFiltMET");
			m_WeightEvents = (eventInfo->mcEventWeights()).at(0) * eventInfo->auxdata<float>("Sherpa_weight");

			if ( m_multiweight ) {
				if ( eventInfo->mcChannelNumber() < 400000 ) {
					m_WeightEvents_MUR05_MUF1 = eventInfo->mcEventWeights().at(5); // MUR05_MUF1
					m_WeightEvents_MUR1_MUF2 = eventInfo->mcEventWeights().at(8); // MUR1_MUF2
					m_WeightEvents_MUR2_MUF1 = eventInfo->mcEventWeights().at(9); // MUR2_MUF1
					m_WeightEvents_MUR1_MUF05 = eventInfo->mcEventWeights().at(6); // "MUR1_MUF0.5
				}
				else {
					m_WeightEvents_MUR1_MUF2 = eventInfo->mcEventWeights().at(1); // MUR1_MUF2
					m_WeightEvents_MUR05_MUF1 = eventInfo->mcEventWeights().at(6); // MUR05_MUF1
					m_WeightEvents_MUR1_MUF05 = eventInfo->mcEventWeights().at(2); // MUR1_MUF05
					m_WeightEvents_MUR2_MUF1 = eventInfo->mcEventWeights().at(3); // MUR2_MUF1
				}
			}

			//m_WeightPU = eventInfo->auxdata< float >( "PileupWeight" );
			m_mu_av = eventInfo->averageInteractionsPerCrossing();
		}
		else {
			m_RunNumber = eventInfo->runNumber();
			m_decayType = -1;
			m_HFclass = -1;
			m_GenFiltHT = -1;
			m_GenFiltMET = -1;
			m_WeightEvents = 1;
			m_WeightPU_map[sysname] = 1;
			m_mu_av = eventInfo->auxdata< float >( "averageInteractionsPerCrossing" );
			//m_mu_av = eventInfo->auxdata< float >( "corrected_averageInteractionsPerCrossing" ); corrected_* for MC?
                        //std::cout << "m_mu_av " << m_mu_av << std::endl;
		}
	}

	std::vector<bool> trig_pass = eventInfo->auxdata<std::vector<bool>>("TrigPass");

	for (UInt_t itrig = 0; itrig < m_list_trig.size(); itrig++) {
		if ( trig_pass.at(itrig)) {
			trigger_map[m_list_trig.at(itrig)] = 1;
		}
		else {
			trigger_map[m_list_trig.at(itrig)] = 0;
		}
	}

	if ( m_debug ) std::cout << "Dump Ele" << std::endl;
	//come sono bello

	for (const auto& el_itr : *electrons) {
		if (el_itr->auxdata< char >("baseline") == 1) {
			lep_pt_map[sysname]->push_back(el_itr->pt());
			lep_eta_map[sysname]->push_back(el_itr->eta());
			lep_phi_map[sysname]->push_back(el_itr->phi());
			lep_m_map[sysname]->push_back(el_itr->m());
			//lep_ptvarcone30_map[sysname]->push_back(el_itr->isolation( xAOD::Iso::ptvarcone30 )); //Not working with ptag3564? 
			lep_ptvarcone20_map[sysname]->push_back(el_itr->isolation( xAOD::Iso::ptvarcone20 ));
			lep_topoetcone20_map[sysname]->push_back(el_itr->isolation( xAOD::Iso::topoetcone20 ));
			lep_flav_map[sysname]->push_back(11 * el_itr->trackParticle()->charge());
			lep_passOR_map[sysname]->push_back( el_itr->auxdata< char >("passOR") == 1 );
			lep_Signal_map[sysname]->push_back( el_itr->auxdata< char >("signal") == 1 );
			lep_HighPt_map[sysname]->push_back( true );

			LepIsoFixedCutLoose_map[sysname]->push_back(m_isoToolFixedCutLoose->accept(*el_itr));
			LepIsoFixedCutTight_map[sysname]->push_back(m_isoToolFixedCutTight->accept(*el_itr));
			LepIsoFixedCutTightTrackOnly_map[sysname]->push_back(m_isoToolFixedCutTightTrackOnly->accept(*el_itr));
			LepIsoGradient_map[sysname]->push_back(m_isoToolGradient->accept(*el_itr));
			LepIsoGradientLoose_map[sysname]->push_back(m_isoToolGradientLoose->accept(*el_itr));
			LepIsoLoose_map[sysname]->push_back(m_isoToolLoose->accept(*el_itr));
			LepIsoLooseTrackOnly_map[sysname]->push_back(m_isoToolLooseTrackOnly->accept(*el_itr));
			LepIsoFCLoose_map[sysname]->push_back(m_isoToolFCLoose->accept(*el_itr));

			d0siglep_map[sysname]->push_back(el_itr->auxdata<float>("d0sig") );
			z0lep_map[sysname]->push_back(el_itr->auxdata<float>("z0sinTheta") );
			//isTightlep_map[sysname]->push_back(m_elecSelLikelihoodTLLH->accept(*el_itr)); //TODO

			if ( m_hasextravar) {
				bkgTruthType_map[sysname]->push_back(acc_bkgTruthType(*el_itr));
				bkgTruthOrigin_map[sysname]->push_back(acc_bkgTruthOrigin(*el_itr));
				bkgMotherPdgId_map[sysname]->push_back(acc_bkgMotherPdgId(*el_itr));
				firstEgMotherTruthType_map[sysname]->push_back(acc_firstEgMotherTruthType(*el_itr));
				firstEgMotherTruthOrigin_map[sysname]->push_back(acc_firstEgMotherTruthOrigin(*el_itr));
				firstEgMotherPdgId_map[sysname]->push_back(acc_firstEgMotherPdgId(*el_itr));
			}

			if ( !m_ISDATA ) {
				lep_Origin_map[sysname]->push_back(acc_Origin(*el_itr));
				lep_Type_map[sysname]->push_back(acc_Type(*el_itr));
			}

			for (UInt_t itrig = 0; itrig < m_list_trig.size(); itrig++) {
				name_branch_matching =  m_list_trig.at(itrig) + "_match" + sysname;
				trigger_matching_map[name_branch_matching]->push_back(el_itr->auxdata<char>(m_list_trig.at(itrig)));
			}

                        if ( el_itr->pt() > 7000.) n_ele10++;

			if ( el_itr->auxdata< char >("passOR") == 1) {

			if ( el_itr->auxdata< char >("signal") == 1 && el_itr->pt() > 20000.) n_sigele++;
			//if ( el_itr->pt() > 7000.) n_ele10++;
			if ( el_itr->pt() > 4000.) n_ele4++;
			if ( el_itr->trackParticle()->charge() > 0 ) n_poslep++;
			else n_neglep++;
			}
                        if (m_ISDATA) {
                               if ( m_debug ) std::cout << "Only for data: TM" << std::endl;
                               TM_2e24_lhvloose_nod0_map[sysname]->push_back( el_itr->auxdata< bool >("match_2e24_lhvloose_nod0") == 1 ); 
                               TM_mu22_mu8noL1_map[sysname]->push_back( el_itr->auxdata< bool >("match_mu22_mu8noL1") == 1 );
                               TM_e17_lhloose_nod0_mu14_map[sysname]->push_back( el_itr->auxdata< bool >("match_e17_lhloose_nod0_mu14") == 1 );
                               TM_2e15_lhvloose_nod0_L12EM13VH_map[sysname]->push_back( el_itr->auxdata< bool >("match_2e15_lhvloose_nod0_L12EM13VH") == 1 );
                               TM_2e17_lhvloose_nod0_map[sysname]->push_back( el_itr->auxdata< bool >("match_2e17_lhvloose_nod0") == 1 );
                               TM_mu20_mu8noL1_map[sysname]->push_back( el_itr->auxdata< bool >("match_mu20_mu8noL1") == 1 );
                               TM_2e12_lhloose_L12EM10VH_map[sysname]->push_back( el_itr->auxdata< bool >("match_2e12_lhloose_L12EM10VH") == 1 );
                               TM_mu18_mu8noL1_map[sysname]->push_back( el_itr->auxdata< bool >("match_mu18_mu8noL1") == 1 );
                               TM_e17_lhloose_mu14_map[sysname]->push_back( el_itr->auxdata< bool >("match_e17_lhloose_mu14") == 1 );
                        }
		}
	}

	if (m_savephotons) {
		if (sysname == "") {
			for (const auto& y_itr : *photons) {
				if (y_itr->pt() > 40000. && y_itr->auxdata< char >("signal") == 1) {
					gamma_pt->push_back(y_itr->pt());
					gamma_eta->push_back(y_itr->eta());
					gamma_phi->push_back(y_itr->phi());
					gamma_passOR->push_back( y_itr->auxdata< char >("passOR") == 1 );
					gammaIsoFixedCutLoose->push_back(m_isoToolFixedCutLoose->accept(*y_itr));
					gammaIsoFixedCutTight->push_back(m_isoToolFixedCutTight->accept(*y_itr));
					n_gamma40++;
				}
			}
		}
	}

	if ( m_debug ) std::cout << "Dump Muons" << std::endl;

	for (const auto& mu_itr : *muons) {

	if (mu_itr->auxdata< char >("baseline") == 1) {

		if (fabs( mu_itr->eta()) < 2.7) {

			lep_pt_map[sysname]->push_back(mu_itr->pt());
			lep_eta_map[sysname]->push_back(mu_itr->eta());
			lep_phi_map[sysname]->push_back(mu_itr->phi());
			lep_m_map[sysname]->push_back(mu_itr->m());
			lep_ptvarcone30_map[sysname]->push_back(mu_itr->isolation( xAOD::Iso::ptvarcone30 ));
			lep_ptvarcone20_map[sysname]->push_back(mu_itr->isolation( xAOD::Iso::ptvarcone20 ));
			lep_topoetcone20_map[sysname]->push_back(mu_itr->isolation( xAOD::Iso::topoetcone20 ));
			lep_flav_map[sysname]->push_back(13 * mu_itr->primaryTrackParticle()->charge());
			lep_passOR_map[sysname]->push_back( mu_itr->auxdata< char >("passOR") == 1 );
			lep_Signal_map[sysname]->push_back( mu_itr->auxdata< char >("signal") == 1 );
			lep_HighPt_map[sysname]->push_back( mu_itr->auxdata< char>("passedHighPtCuts") == 1 );

			LepIsoFixedCutLoose_map[sysname]->push_back(m_isoToolFixedCutLoose->accept(*mu_itr));
			LepIsoFixedCutTight_map[sysname]->push_back(m_isoToolFixedCutTight->accept(*mu_itr));
			LepIsoFixedCutTightTrackOnly_map[sysname]->push_back(m_isoToolFixedCutTightTrackOnly->accept(*mu_itr));
			LepIsoGradient_map[sysname]->push_back(m_isoToolGradient->accept(*mu_itr));
			LepIsoGradientLoose_map[sysname]->push_back(m_isoToolGradientLoose->accept(*mu_itr));
			LepIsoLoose_map[sysname]->push_back(m_isoToolLoose->accept(*mu_itr));
			LepIsoLooseTrackOnly_map[sysname]->push_back(m_isoToolLooseTrackOnly->accept(*mu_itr));
			LepIsoFCLoose_map[sysname]->push_back(m_isoToolFCLoose->accept(*mu_itr));

			d0siglep_map[sysname]->push_back(mu_itr->auxdata<float>("d0sig") );
			z0lep_map[sysname]->push_back(mu_itr->auxdata<float>("z0sinTheta") );
			//isTightlep_map[sysname]->push_back(m_muonTightsel->accept(*mu_itr));

			for (UInt_t itrig = 0; itrig < m_list_trig.size(); itrig++) {
				name_branch_matching =  m_list_trig.at(itrig) + "_match" + sysname;
				trigger_matching_map[name_branch_matching]->push_back(mu_itr->auxdata<char>(m_list_trig.at(itrig)));
			}

			if ( !m_ISDATA ) {
				int muonTruthOrigin = -1;
				int muonTruthType = -1;

				// Access MC type/origin
				const xAOD::TrackParticle* trackParticle = mu_itr->primaryTrackParticle();
				if (trackParticle) {
					if (acc_Origin.isAvailable(*trackParticle)) muonTruthOrigin = acc_Origin(*trackParticle);
					if (acc_Type.isAvailable(*trackParticle)  ) muonTruthType   = acc_Type(*trackParticle);
				}
				lep_Origin_map[sysname]->push_back(muonTruthOrigin);
				lep_Type_map[sysname]->push_back(muonTruthType);

				if ( m_hasextravar) {
					bkgTruthType_map[sysname]->push_back(-1);
					bkgTruthOrigin_map[sysname]->push_back(-1);
					bkgMotherPdgId_map[sysname]->push_back(-1);
					firstEgMotherTruthType_map[sysname]->push_back(-1);
					firstEgMotherTruthOrigin_map[sysname]->push_back(-1);
					firstEgMotherPdgId_map[sysname]->push_back(-1);
				}
			}


                        if ( mu_itr->pt() > 7000.) n_mu10++;

			if ( mu_itr->auxdata< char >("passOR") == 1 ) {
				if ( mu_itr->auxdata< char >("signal") == 1 && mu_itr->pt() > 20000.) n_sigmu++;
				//if ( mu_itr->pt() > 7000.) n_mu10++;
				if ( mu_itr->pt() > 5000.) n_mu5++;
				if ( mu_itr->primaryTrackParticle()->charge() > 0 ) n_poslep++;
				else n_neglep++;
			}

                        if (m_ISDATA) {
                               TM_2e24_lhvloose_nod0_map[sysname]->push_back( mu_itr->auxdata< bool >("match_2e24_lhvloose_nod0") == 1 ); 
                               TM_mu22_mu8noL1_map[sysname]->push_back( mu_itr->auxdata< bool >("match_mu22_mu8noL1") == 1 );
                               TM_e17_lhloose_nod0_mu14_map[sysname]->push_back( mu_itr->auxdata< bool >("match_e17_lhloose_nod0_mu14") == 1 );
                               TM_2e15_lhvloose_nod0_L12EM13VH_map[sysname]->push_back( mu_itr->auxdata< bool >("match_2e15_lhvloose_nod0_L12EM13VH") == 1 );
                               TM_2e17_lhvloose_nod0_map[sysname]->push_back( mu_itr->auxdata< bool >("match_2e17_lhvloose_nod0") == 1 );
                               TM_mu20_mu8noL1_map[sysname]->push_back( mu_itr->auxdata< bool >("match_mu20_mu8noL1") == 1 );
                               TM_2e12_lhloose_L12EM10VH_map[sysname]->push_back( mu_itr->auxdata< bool >("match_2e12_lhloose_L12EM10VH") == 1 );
                               TM_mu18_mu8noL1_map[sysname]->push_back( mu_itr->auxdata< bool >("match_mu18_mu8noL1") == 1 );
                               TM_e17_lhloose_mu14_map[sysname]->push_back( mu_itr->auxdata< bool >("match_e17_lhloose_mu14") == 1 );
                        }

			}
		}
	}

	if ( m_debug ) std::cout << "Dump Jets" << std::endl;

	int ijets = 0;
	//std::cout<<sysname<<std::endl;
	for (const auto& jet_itr : *jets) {
		if ( jet_itr->auxdata< char >("signal") == 1  && jet_itr->pt() > 20000. && ( fabs( jet_itr->eta()) < 4.5) ) {

			float jvt = acc_Jvt(*jet_itr);
			jet_JVT_map[sysname]->push_back(jvt);
			jet_pt_map[sysname]->push_back(jet_itr->pt());
			jet_eta_map[sysname]->push_back(jet_itr->eta());
			jet_phi_map[sysname]->push_back(jet_itr->phi());
			jet_m_map[sysname]->push_back(jet_itr->m());
			jet_isB85_map[sysname]->push_back( jet_itr->auxdata< bool >("B85tag") == 1 );
			if (jet_itr->auxdata< bool >("B85tag") == 1) n_bjets++;

			double weight_mv2(-10.);
			ANA_CHECK( jet_itr->btagging()->MVx_discriminant("MV2c10", weight_mv2));

			jet_BtagWeight_map[sysname]->push_back( weight_mv2 );
			jet_passOR_map[sysname]->push_back( jet_itr->auxdata< char >("passOR") == 1 );

			if ( !m_ISDATA ) {
				labeljets_reco_map[sysname]->push_back(jet_itr->auxdata<int>("HadronConeExclTruthLabelID"));
			}
			else {
				labeljets_reco_map[sysname]->push_back(-1);
			}

			if (m_btagWP && sysname == "") {
				jet_isB70->push_back( jet_itr->auxdata< bool >("B70tag") == 1 );
				jet_isB85->push_back( jet_itr->auxdata< bool >("B85tag") == 1 );
				jet_isB70Flat->push_back( jet_itr->auxdata< bool >("B70tagFlat") == 1 );
				jet_isB77Flat->push_back( jet_itr->auxdata< bool >("B77tagFlat") == 1 );
				jet_isB85Flat->push_back( jet_itr->auxdata< bool >("B85tagFlat") == 1 );
			}
		}
	}

	met_map[sysname] = eventInfo->auxdata<float>("MET");
	met_phi_map[sysname] = eventInfo->auxdata<float>("METphi");
	metEle_map[sysname] = eventInfo->auxdata<float>("MET_RefEle");
	metEle_phi_map[sysname] = eventInfo->auxdata<float>("MET_RefElePhi");
	metMu_map[sysname] = eventInfo->auxdata<float>("MET_RefMu");
	metMu_phi_map[sysname] = eventInfo->auxdata<float>("MET_RefMuPhi");
	metJet_map[sysname] = eventInfo->auxdata<float>("MET_RefJet");
	metJet_phi_map[sysname] = eventInfo->auxdata<float>("MET_RefJetPhi");
	metGam_map[sysname] = eventInfo->auxdata<float>("MET_RefGam");
	metGam_phi_map[sysname] = eventInfo->auxdata<float>("MET_RefGamPhi");
	metSoft_map[sysname] = eventInfo->auxdata<float>("MET_RefSoft");
	metSoft_phi_map[sysname] = eventInfo->auxdata<float>("MET_RefSoftPhi");
	met_sig_map[sysname] = eventInfo->auxdata<float>("MET_sig"); //TODO
	if (sysname == "") {
		Etmiss_PVSoftTrk = eventInfo->auxdata<float>("MET_soft");
		Etmiss_PVSoftTrkPhi = eventInfo->auxdata<float>("MET_softphi");
	}

	//////////////////////////////////////////////
	//                                          //
	//    Filter definition for ntuples here    //
	//                                          //
	//////////////////////////////////////////////

	//Baseline filter for 2L analysis
	bool two_loose_lep = false;
	if ((n_mu10 + n_ele10) > 0) two_loose_lep = true;

	//Filter for MET based triggering
	bool two_vloose_lep_andmet = false;
	if ( eventInfo->auxdata<float>("MET") > 150000. && ((n_ele4 + n_mu5) > 1)) two_vloose_lep_andmet = true;

	//Filter for 1Lgamma
	bool one_tight_lep_and_one_gamma = false;
	if ((((n_sigmu + n_sigele) == 1) || ((n_sigmu + n_sigele) == 2)) && n_gamma40 == 1) one_tight_lep_and_one_gamma = true;

	//Filter for stopZ
	bool three_lep = false;
	if ((n_ele4 + n_mu5) > 2) three_lep = true;

	//Filter for high-pT for stopH
	bool one_lep = false;
	if ((n_sigmu + n_sigele) >= 1) one_lep = true;

	//Filter for fakes
	bool two_SS_lep = false;
	if (n_poslep > 1 || n_neglep > 1) two_SS_lep = true;

	//Filter for stopH
	bool three_bjets = false;
	if (n_bjets > 2 ) three_bjets = true;
	bool combined_filter = false;
	if (m_filter == "stop2L") {
	combined_filter = two_loose_lep;
	if (m_savephotons) combined_filter = two_loose_lep || one_tight_lep_and_one_gamma;
	}
	else if (m_filter == "fakes2L") {
	combined_filter = two_SS_lep;
	}
	else if (m_filter == "stopZ") combined_filter = three_lep;
	else if (m_filter == "stop4b") {
	combined_filter = two_vloose_lep_andmet;
	if (m_savephotons) combined_filter = two_vloose_lep_andmet || one_tight_lep_and_one_gamma;
	}
	else if (m_filter == "stopH") combined_filter = one_lep && three_bjets;
	else combined_filter = true;

	if ( !m_ISDATA ) {
	elSFwei_map[sysname] = eventInfo->auxdata<float>("eleSF_weight");
	muSFwei_map[sysname] = eventInfo->auxdata<float>("muSF_weight");
	globalSFwei_map[sysname] = eventInfo->auxdata<float>("globalSF_weight");
	//trigwei_singlelep_map[sysname] = eventInfo->auxdata< double >("trig_SF_single_e") * eventInfo->auxdata< double >("trig_SF_single_m");
	//trigwei_dilep_map[sysname] = eventInfo->auxdata< double >("trig_SF_double_e") * eventInfo->auxdata< double >("trig_SF_double_m");
	//trigwei_mixlep_map[sysname] = eventInfo->auxdata< double >("trig_SF_mix_e") * eventInfo->auxdata< double >("trig_SF_mix_m");
	trigwei_global_map[sysname] =  eventInfo->auxdata<float>("globalTrig_weight");//FILLME
	m_WeightPU_map[sysname] = eventInfo->auxdata< float >( "PileupWeight" );
	bTagSF_map[sysname] = eventInfo->auxdata<float>("bTag_weight");
	jvfSF_map[sysname] = eventInfo->auxdata<float>("JVT_weight");
	}
	else {
	elSFwei_map[sysname] = 1.;
	muSFwei_map[sysname] = 1.;
	globalSFwei_map[sysname] =1. ;
	//trigwei_singlelep_map[sysname] = 1.;
	//trigwei_dilep_map[sysname] = 1.;
	//trigwei_mixlep_map[sysname] = 1.;
	trigwei_global_map[sysname] = 1.;
	bTagSF_map[sysname] = 1.;
	jvfSF_map[sysname] = 1.;
	}

	if ( m_debug ) {
	for (UInt_t i = 0; i < m_list_trig.size(); i++) {
	name_branch_matching =  m_list_trig.at(i) + "_match" + sysname;
	if (trigger_matching_map[name_branch_matching]->size() != lep_pt_map[sysname]->size() ) std::cout << name_branch_matching << " " << trigger_matching_map[name_branch_matching.c_str()]->size() << " vs " << lep_pt_map[sysname]->size() << std::endl;
	}
	}

	if ( m_debug ) std::cout << "GGWP" << std::endl;

	return combined_filter;

}

// Call after all the syst have been processed and ONLY IF one of them passed the event selection criteria
StatusCode OutTree::FillTree() {

newTree->Fill();

return StatusCode::SUCCESS;
}
