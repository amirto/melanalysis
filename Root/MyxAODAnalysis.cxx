#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <MelAnalysis/MyxAODAnalysis.h>
#include "MelAnalysis/ClassifyAndCalculateHF.h"
#include "EventLoop/OutputStream.h"
#include "FourMomUtils/xAODP4Helpers.h"
#include <TTreeFormula.h>
#include <memory.h>
#include <TSystem.h>
#include <TRandom3.h>
#include <stdlib.h>

// EDM includes:
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODMuon/Muon.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthEvent.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODBTaggingEfficiency/BTaggingEfficiencyTool.h"
#include "xAODBTagging/BTagging.h"
#include "JetMomentTools/JetVertexTaggerTool.h"

#include <AsgTools/MessageCheck.h>
#include "SUSYTools/SUSYObjDef_xAOD.h"

// Amg include
#include "EventPrimitives/EventPrimitivesHelpers.h"
#include "xAODEgamma/EgammaxAODHelpers.h"

//Trigger Include
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TrigConfxAOD/xAODConfigTool.h"

#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"

// Path Resolver, needed from rel.21, it took over ROOTBINCORE
#include "PathResolver/PathResolver.h"
#ifdef ROOTCORE
#   include "xAODRootAccess/Init.h"
#   include "xAODRootAccess/TEvent.h"
#   include "xAODRootAccess/TStore.h"
#endif // ROOTCORE

// GRL
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "GoodRunsLists/DQHelperFunctions.h"

using namespace xAOD;

static SG::AuxElement::Decorator<double> dec_xsec("CrossSection");
static SG::AuxElement::Decorator<float> dec_Sherpawei("Sherpa_weight");
static SG::AuxElement::Decorator<int> dec_RndRunNum("RndRunNum");
static SG::AuxElement::Decorator<int> dec_PrimVx("NumPrimaryVertices");
static SG::AuxElement::Decorator<int> dec_DecayType("DecayType");
static SG::AuxElement::Decorator<int> dec_HFclass("HFclass");

static SG::AuxElement::Decorator<bool> dec_cleaning("cleaning_veto");
static SG::AuxElement::Decorator<bool> dec_passMETtrigger("passMETtrigger");
static SG::AuxElement::Decorator<float> dec_MET("MET");
static SG::AuxElement::Decorator<float> dec_MET_sig("MET_sig");
static SG::AuxElement::Decorator<float> dec_MET_Soft("MET_soft");
static SG::AuxElement::Decorator<float> dec_METphi("METphi");
static SG::AuxElement::Decorator<float> dec_MET_Softphi("MET_softphi");

static SG::AuxElement::Decorator<double> dec_single_lep_e("trig_SF_single_e");
static SG::AuxElement::Decorator<double> dec_double_lep_e("trig_SF_double_e");
static SG::AuxElement::Decorator<double> dec_mix_lep_e("trig_SF_mix_e");
static SG::AuxElement::Decorator<double> dec_single_lep_m("trig_SF_single_m");
static SG::AuxElement::Decorator<double> dec_double_lep_m("trig_SF_double_m");
static SG::AuxElement::Decorator<double> dec_mix_lep_m("trig_SF_mix_m");

static SG::AuxElement::Decorator<bool> dec_isB70tag("B70tag");
static SG::AuxElement::Decorator<bool> dec_isB77tag("B77tag");
static SG::AuxElement::Decorator<bool> dec_isB85tag("B85tag");
static SG::AuxElement::Decorator<bool> dec_isB70tagFlat("B70tagFlat");
static SG::AuxElement::Decorator<bool> dec_isB77tagFlat("B77tagFlat");
static SG::AuxElement::Decorator<bool> dec_isB85tagFlat("B85tagFlat");

static SG::AuxElement::Accessor<int>  acc_Origin("truthOrigin");


static SG::AuxElement::Decorator<bool> dec_match_2e24_lhvloose_nod0("match_2e24_lhvloose_nod0");    
static SG::AuxElement::Decorator<bool> dec_match_mu22_mu8noL1("match_mu22_mu8noL1");  
static SG::AuxElement::Decorator<bool> dec_match_e17_lhloose_nod0_mu14("match_e17_lhloose_nod0_mu14");  
static SG::AuxElement::Decorator<bool> dec_match_2e15_lhvloose_nod0_L12EM13VH("match_2e15_lhvloose_nod0_L12EM13VH");   
static SG::AuxElement::Decorator<bool> dec_match_2e17_lhvloose_nod0("match_2e17_lhvloose_nod0");  
static SG::AuxElement::Decorator<bool> dec_match_mu20_mu8noL1("match_mu20_mu8noL1");   
static SG::AuxElement::Decorator<bool> dec_match_2e12_lhloose_L12EM10VH("match_2e12_lhloose_L12EM10VH");  
static SG::AuxElement::Decorator<bool> dec_match_mu18_mu8noL1("match_mu18_mu8noL1");  
static SG::AuxElement::Decorator<bool> dec_match_e17_lhloose_mu14("match_e17_lhloose_mu14"); 

  // 2017 ee HLT_2e24_lhvloose_nod0      
  //      mm HLT_mu22_mu8noL1 
  //      em HLT_e17_lhloose_nod0_mu14
  // 2016 ee HLT_2e15_lhvloose_nod0_L12EM13VH || HLT_2e17_lhvloose_nod0
  //      mm HLT_mu22_mu8noL1 || HLT_mu20_mu8noL1 
  //      em HLT_e17_lhloose_nod0_mu14
  // 2015 ee HLT_2e12_lhloose_L12EM10VH
  //      mm HLT_mu18_mu8noL1
  //      em HLT_e17_lhloose_mu14



// this is needed to distribute the algorithm to the workers
ClassImp(MyxAODAnalysis)

MyxAODAnalysis :: MyxAODAnalysis (): my_XsecDB(0), objTool_77(0), objTool_70(0), objTool_85(0), objTool_70Flat(0), objTool_77Flat(0), objTool_85Flat(0)
{
    // Here you put any code for the base initialization of variables,
    // e.g. initialize all pointers to 0.  Note that you should only put
    // the most basic initialization here, since this method will be
    // called on both the submission and the worker node.  Most of your
    // initialization code will go into histInitialize() and
    // initialize().
}

EL::StatusCode MyxAODAnalysis :: setupJob (EL::Job & job)
{
    // Here you put code that sets up the job on the submission object
    // so that it is ready to work with your algorithm, e.g. you can
    // request the D3PDReader service or add output files.  Any code you
    // put here could instead also go into the submission script.  The
    // sole advantage of putting it here is that it gets automatically
    // activated/deactivated when you add/remove the algorithm from your
    // job, which may or may not be of value to you.
    job.useXAOD ();

    // let's initialize the algorithm to use the xAODRootAccess package
    xAOD::Init( "MyxAODAnalysis" ).ignore(); // call before opening first file

    // tell EventLoop about our output:
    EL::OutputStream out ("output");
    job.outputAdd (out);

    return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: histInitialize ()
{
    // Here you do everything that needs to be done at the very
    // beginning on each worker node, e.g. create histograms and output
    // trees.  This method gets called before any input files are
    // connected.

    NumberEvents = new TH1D("NumberEvents", "Number Events", 3, 0, 3);
    NumberEvents->GetXaxis()->SetBinLabel(1, "Raw");
    NumberEvents->GetXaxis()->SetBinLabel(2, "Weights");
    NumberEvents->GetXaxis()->SetBinLabel(3, "WeightsSquared");

        //NumberEvents->SetBinContent(4,1);
    EventsCount = new TH1D("EventsCount", "EventsCount", 3, 0, 2);
        //EventsCount->SetBinContent(4,1);
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: fileExecute ()
{
    // Here you do everything that needs to be done exactly once for every
    // single file, e.g. collect a list of all lumi-blocks processed
    return EL::StatusCode::SUCCESS;
}



    EL::StatusCode MyxAODAnalysis :: changeInput (bool firstFile)
    {

            // Here you do everything you need to do when we change input files,
            // e.g. resetting branch addresses on trees.  If you are using
            // D3PDReader or a similar service this method is not needed.

            const char *APP_NAME = "changeInput()";

            m_event = wk()->xaodEvent();
            Info( APP_NAME, "Number of events in this file = %lli", m_event->getEntries() );

            //Read the CutBookkeeper container
            const xAOD::CutBookkeeperContainer* completeCBC = 0;
            if (!m_event->retrieveMetaInput(completeCBC, "CutBookkeepers").isSuccess()) {
                    Error( APP_NAME, "Failed to retrieve CutBookkeepers from MetaData! Exiting.");
            }

            const xAOD::CutBookkeeper* allEventsCBK = 0;
            int maxcycle = -1;

            //let's find the right CBK (latest with StreamAOD input before derivations)
            for ( auto cbk : *completeCBC ) {
                    if ( cbk->name() == "AllExecutedEvents" && cbk->inputStream() == "StreamAOD" && cbk->cycle() > maxcycle) {
                            maxcycle = cbk->cycle();
                            allEventsCBK = cbk;
                    }
            }

            uint64_t nEventsProcessed  = 0;
            double sumOfWeights        = 0.;
            double sumOfWeightsSquared = 0.;
            if (allEventsCBK) {
                    nEventsProcessed  = allEventsCBK->nAcceptedEvents();
                    sumOfWeights        = allEventsCBK->sumOfEventWeights();
                    sumOfWeightsSquared = allEventsCBK->sumOfEventWeightsSquared();
                    Info( APP_NAME, "CutBookkeepers Accepted %lu SumWei %f sumWei2 %f ", nEventsProcessed, sumOfWeights, sumOfWeightsSquared);
            } else {
                    Info( APP_NAME, "No relevant CutBookKeepers found" );
            }
            if(m_event->getEntries() && wk()->metaData()->castDouble("isdata") != 1){
                NumberEvents->Fill(0., nEventsProcessed);
                NumberEvents->Fill(1., sumOfWeights);
                NumberEvents->Fill(2., sumOfWeightsSquared);
            }
            //if( !m_event->getEntries() )
            //      mem_leaker->processEmpty();//m_event, electrons, photons, muons, jets, objTool_77->currentSystematic().name());
    return EL::StatusCode::SUCCESS;

}



EL::StatusCode MyxAODAnalysis :: initialize ()
{
    // Here you do everything that you need to do after the first input
    // file has been connected and before the first event is processed,
    // e.g. create additional histograms based on which variables are
    // available in the input files.  You can also create all of your
    // histograms and trees in here, but be aware that this method
    // doesn't get called if no events are processed.  So any objects
    // you create here won't be available in the output if you have no
    // input events.
    const char *APP_NAME = "MyxAODAnalysis::initialize()";

    trig_list = {
        "HLT_2e12_lhloose_L12EM10VH",
        "HLT_2e15_lhvloose_nod0_L12EM13VH",
        "HLT_2e17_lhvloose_nod0",
        "HLT_2e17_lhvloose_nod0_L12EM15VHI",
        "HLT_2e24_lhvloose_nod0",
        "HLT_e24_lhvloose_nod0_2e12_lhvloose_nod0_L1EM20VH_3EM10VH",
        "HLT_e17_lhloose_mu14",
        "HLT_e17_lhloose_nod0_mu14",
        "HLT_mu22_mu8noL1",
        "HLT_mu20_mu8noL1",
        "HLT_mu18_mu8noL1",
        "HLT_e24_lhmedium_L1EM20VH",
        "HLT_e24_lhmedium_nod0_L1EM20VH",
        "HLT_e24_lhtight_nod0_ivarloose",
        "HLT_e26_lhtight_nod0_ivarloose",
        "HLT_e60_lhmedium",
        "HLT_e60_lhmedium_nod0",
        "HLT_e120_lhloose",
        "HLT_e140_lhloose_nod0",
        "HLT_mu20_iloose_L1MU15",
        "HLT_mu24_ivarloose",
        "HLT_mu24_ivarloose_L1MU15",
        "HLT_mu24_ivarmedium",
        "HLT_mu26_ivarmedium",
        "HLT_mu50",
        "HLT_xe70_mht",
        "HLT_xe70_tc_lcw",
        "HLT_xe80_tc_lcw_L1XE50",
        "HLT_xe90_mht_L1XE50",
        "HLT_xe100_mht_L1XE50",
        "HLT_xe110_mht_L1XE50"
    };

    trig_list_el = {
        "HLT_2e12_lhloose_L12EM10VH",
        "HLT_2e15_lhvloose_nod0_L12EM13VH",
        "HLT_2e17_lhvloose_nod0",
        "HLT_2e17_lhvloose_nod0_L12EM15VHI",
        "HLT_e17_lhloose_mu14",
        "HLT_e17_lhloose_nod0_mu14",
        "HLT_e24_lhmedium_L1EM20VH",
        "HLT_e24_lhmedium_nod0_L1EM20VH",
        "HLT_e24_lhtight_nod0_ivarloose",
        "HLT_e26_lhtight_nod0_ivarloose",
        "HLT_e60_lhmedium",
        "HLT_e60_lhmedium_nod0",
        "HLT_e120_lhloose",
        "HLT_e140_lhloose_nod0"
    };

    trig_list_mu = {
        "HLT_e17_lhloose_mu14",
        "HLT_e17_lhloose_nod0_mu14",
        "HLT_mu22_mu8noL1",
        "HLT_mu20_mu8noL1",
        "HLT_mu18_mu8noL1",
        "HLT_mu20_iloose_L1MU15",
        "HLT_mu24_ivarloose",
        "HLT_mu24_ivarloose_L1MU15",
        "HLT_mu24_ivarmedium",
        "HLT_mu26_ivarmedium",
        "HLT_mu50"
    };

    muTrigSingle2015 = "HLT_mu20_iloose_L1MU15_OR_HLT_mu50";
    muTrigDouble2015 = "HLT_mu18_mu8noL1";
    muTrigMix2015 = "HLT_mu14";
    muTrigSingle2016 = "HLT_mu26_ivarmedium_OR_HLT_mu50";
    muTrigDouble2016 = "HLT_mu22_mu8noL1";
    muTrigMix2016 = "HLT_mu14";

    bool isAtlfast=false;
    bool isData=false;

    bool isA = false, isD = false, isE = false;
	if (wk()->metaData()->castDouble("isatlfast") == 1) isAtlfast = true;
	if (wk()->metaData()->castDouble("isdata") == 1) isData = true;
	if (wk()->metaData()->castDouble("isA") == 1) isA = true;
	if (wk()->metaData()->castDouble("isD") == 1) isD = true;
	if (wk()->metaData()->castDouble("isE") == 1) isE = true;

	sample_name = wk()->metaData()->castString("samplename");


    if (isData){
        Info( APP_NAME, "Initializing GRL");

        grl = new GoodRunsListSelectionTool("GoodRunsListSelectionTool/grl");
        std::vector<std::string> vecStringGRL;
        std::string grl1 = PathResolverFindDataFile("MelAnalysis/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml");
        std::string grl2 = PathResolverFindDataFile("MelAnalysis/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml");
        std::string grl3 = PathResolverFindDataFile("MelAnalysis/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml");
	std::string grl4 = PathResolverFindDataFile("MelAnalysis/data18_13TeV.periodAllYear_DetStatus-v102-pro22-03_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml");


      
        vecStringGRL.push_back(grl1.c_str());
        vecStringGRL.push_back(grl2.c_str());
        vecStringGRL.push_back(grl3.c_str());
	vecStringGRL.push_back(grl4.c_str());
	std::cout<<"-----------------GRL FILE---------------"<<std::endl;
        std::cout<< grl1 << std::endl;
        std::cout<< grl2 << std::endl;
        std::cout<< grl3 << std::endl;
	std::cout<< grl4 << std::endl;
	std::cout<<"---------------------------------------"<<std::endl;
      
        ANA_CHECK( grl->setProperty( "GoodRunsListVec", vecStringGRL) );
        ANA_CHECK( grl->setProperty( "PassThrough", false) ); // if true (default) will ignore result of GRL and will just pass all events
        if (!grl->initialize().isSuccess()) { // check this isSuccess
          Error(APP_NAME, "Failed to properly initialize the GRL. Exiting." );
          return EL::StatusCode::FAILURE;
        }
        std::cout << grl->getGRLCollection().IsEmpty() << std::endl;
        std::cout << grl->getBRLCollection().IsEmpty() << std::endl;

    }

    if (wk()->xaodEvent()) {
        m_event = wk()->xaodEvent();
    }
    m_store =  new xAOD::TStore();

    ST::ISUSYObjDef_xAODTool::DataSource datasource = (isData ? ST::ISUSYObjDef_xAODTool::Data : (isAtlfast ? ST::ISUSYObjDef_xAODTool::AtlfastII : ST::ISUSYObjDef_xAODTool::FullSim));

    objTool_77 = new ST::SUSYObjDef_xAOD( "SUSYObjDef_xAOD77" );
    objTool_77->msg().setLevel( MSG::FATAL );
    if (doBtagWP) {
        objTool_70 = new ST::SUSYObjDef_xAOD( "SUSYObjDef_xAOD" );
        objTool_70->msg().setLevel( MSG::FATAL);
        objTool_85 = new ST::SUSYObjDef_xAOD( "SUSYObjDef_xAOD85" );
        objTool_85->msg().setLevel( MSG::FATAL);
        objTool_70Flat = new ST::SUSYObjDef_xAOD( "SUSYObjDef_xAODFlat" );
        objTool_70Flat->msg().setLevel( MSG::FATAL);
        objTool_77Flat = new ST::SUSYObjDef_xAOD( "SUSYObjDef_xAOD77Flat" );
        objTool_77Flat->msg().setLevel( MSG::FATAL);
        objTool_85Flat = new ST::SUSYObjDef_xAOD( "SUSYObjDef_xAOD85Flat" );
        objTool_85Flat->msg().setLevel( MSG::FATAL);
    }

    std::string MyConfigFile;
    if (format == "stop2L") MyConfigFile = PathResolverFindCalibFile("MelAnalysis/SUSYTools_stop2L_tag77.conf");
    else if (format == "fakes2L") MyConfigFile = PathResolverFindCalibFile("MelAnalysis/SUSYTools_fakes2L_tag77.conf");
    else if (format == "stop4b") MyConfigFile = PathResolverFindCalibFile("MelAnalysis/SUSYTools_stop2L_tag77.conf");
    else if (format == "stopZ") MyConfigFile = PathResolverFindCalibFile("MelAnalysis/SUSYTools_stopZ_tag77.conf");
    else if (format == "stopH") MyConfigFile = PathResolverFindCalibFile("MelAnalysis/SUSYTools_stopH_tag77.conf");
    else if (format == "test") MyConfigFile = PathResolverFindCalibFile("SUSYTools/SUSYTools_Default.conf");
    else {
        Error(APP_NAME, "Format not supported" );
        return EL::StatusCode::FAILURE;
    }
        std::cout << MyConfigFile << std::endl;
    // Configure the SUSYObjDef instance
    ANA_CHECK( objTool_77->setProperty("ConfigFile", MyConfigFile) );
    if (doBtagWP) {
        ANA_CHECK( objTool_70->setProperty("ConfigFile", MyConfigFile) );
        ANA_CHECK( objTool_70->setProperty("BtagWP", "FixedCutBEff_70") );
        ANA_CHECK( objTool_85->setProperty("ConfigFile", MyConfigFile) );
        ANA_CHECK( objTool_85->setProperty("BtagWP", "FixedCutBEff_85") );
        ANA_CHECK( objTool_70Flat->setProperty("ConfigFile", MyConfigFile) );
        ANA_CHECK( objTool_70Flat->setProperty("BtagWP", "FlatBEff_70") );
        ANA_CHECK( objTool_77Flat->setProperty("ConfigFile", MyConfigFile) );
        ANA_CHECK( objTool_77Flat->setProperty("BtagWP", "FlatBEff_77") );
        ANA_CHECK( objTool_85Flat->setProperty("ConfigFile", MyConfigFile) );
        ANA_CHECK( objTool_85Flat->setProperty("BtagWP", "FlatBEff_85") );
    }


    ANA_CHECK(objTool_77->setProperty("DataSource", datasource) ) ;
    if (doBtagWP) {
        ANA_CHECK(objTool_70->setProperty("DataSource", datasource) ) ;
        ANA_CHECK(objTool_85->setProperty("DataSource", datasource) ) ;
        ANA_CHECK(objTool_70Flat->setProperty("DataSource", datasource) ) ;
        ANA_CHECK(objTool_77Flat->setProperty("DataSource", datasource) ) ;
        ANA_CHECK(objTool_85Flat->setProperty("DataSource", datasource) ) ;
    }


    ///*******COMMENTING PILEUP TO TRY AUTOCONF**************////


    //std::vector<std::string> prw_conf; //use the generic file since we re-do this locally anyway
    //prw_conf.push_back("/afs/cern.ch/atlas/www/GROUPS/DATABASE/GroupData/dev/SUSYTools/PRW_AUTOCONFIG_SIM/files/pileup_mc16e_dsid410472_FS.root");
    //prw_conf.push_back("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/SUSYTools/merged_prw_mc16a_latest.root");
    //prw_conf.push_back("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/SUSYTools/PRW_AUTOCONGIF/files_old/pileup_mc16a_dsid364254.root");
    // Changed by python script
    
    //ANA_CHECK( objTool_77->setProperty("PRWConfigFiles", prw_conf) );


    std::vector<std::string> prw_lumicalc;
	if (isA) {
		prw_lumicalc.push_back("GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root");
		prw_lumicalc.push_back("GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root");
	}
    	if (isD) prw_lumicalc.push_back("GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root");
    	if (isE) prw_lumicalc.push_back("GoodRunsLists/data18_13TeV/20180924/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-001.root");

    ANA_CHECK( objTool_77->setProperty("PRWLumiCalcFiles", prw_lumicalc) );
    if (doBtagWP) {
        ANA_CHECK(objTool_70->setProperty("PRWLumiCalcFiles", prw_lumicalc) ) ;
        ANA_CHECK(objTool_85->setProperty("PRWLumiCalcFiles", prw_lumicalc) ) ;
        ANA_CHECK(objTool_70Flat->setProperty("PRWLumiCalcFiles", prw_lumicalc) ) ;
        ANA_CHECK(objTool_77Flat->setProperty("PRWLumiCalcFiles", prw_lumicalc) ) ;
        ANA_CHECK(objTool_85Flat->setProperty("PRWLumiCalcFiles", prw_lumicalc) ) ;
    }

    ANA_CHECK( objTool_77->setBoolProperty("AutoconfigurePRWTool", true));
    //ANA_CHECK( objTool_77->setProperty("AutoconfigurePRWToolPath","/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/SUSYTools/PRW_AUTOCONFIG_SIM/files/") ); -- OLD FOLDER 
    ANA_CHECK( objTool_77->setProperty("AutoconfigurePRWToolPath","/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PileupReweighting/mc16_13TeV/") ); // -- NEW FOLDER SINCE 11/2018

    if (m_debug) ANA_CHECK( objTool_77->setProperty("OutputLevel", MSG::VERBOSE));

    if ( objTool_77->initialize() != StatusCode::SUCCESS) {
        Error(APP_NAME, "Cannot intialize SUSYObjDef_xAOD77..." );
        Error(APP_NAME, "Exiting... " );
        return EL::StatusCode::FAILURE;
    } else {
        Info(APP_NAME, "SUSYObjDef_xAOD77 initialized... " );
    }

    if (doBtagWP) {
        if ( objTool_70->initialize() != StatusCode::SUCCESS) {
            Error(APP_NAME, "Cannot intialize SUSYObjDef_xAOD70..." );
            Error(APP_NAME, "Exiting... " );
            return EL::StatusCode::FAILURE;
        } else {
            Info(APP_NAME, "SUSYObjDef_xAOD initialized... " );
        }
        if ( objTool_85->initialize() != StatusCode::SUCCESS) {
            Error(APP_NAME, "Cannot intialize SUSYObjDef_xAOD85..." );
            Error(APP_NAME, "Exiting... " );
            return EL::StatusCode::FAILURE;
        } else {
            Info(APP_NAME, "SUSYObjDef_xAOD85 initialized... " );
        }
        if ( objTool_70Flat->initialize() != StatusCode::SUCCESS) {
            Error(APP_NAME, "Cannot intialize SUSYObjDef_xAOD70Flat..." );
            Error(APP_NAME, "Exiting... " );
            return EL::StatusCode::FAILURE;
        } else {
            Info(APP_NAME, "SUSYObjDef_xAOD initialized... " );
        }
        if ( objTool_77Flat->initialize() != StatusCode::SUCCESS) {
            Error(APP_NAME, "Cannot intialize SUSYObjDef_xAOD77Flat..." );
            Error(APP_NAME, "Exiting... " );
            return EL::StatusCode::FAILURE;
        } else {
            Info(APP_NAME, "SUSYObjDef_xAOD77 initialized... " );
        }
        if ( objTool_85Flat->initialize() != StatusCode::SUCCESS) {
            Error(APP_NAME, "Cannot intialize SUSYObjDef_xAOD85Flat..." );
            Error(APP_NAME, "Exiting... " );
            return EL::StatusCode::FAILURE;
        } else {
            Info(APP_NAME, "SUSYObjDef_xAOD85 initialized... " );
        }
    }

    if (m_debug) Info(APP_NAME, "SUSYTools initialized" );
    std::string fullPathToFile = PathResolverFindCalibDirectory("SUSYTools/mc15_13TeV/"); //returns "" if directory not found
    my_XsecDB = new SUSY::CrossSectionDB(gSystem->ExpandPathName(fullPathToFile.c_str()));
    if (m_debug) Info(APP_NAME, "xsec DB initialized" );

    TH1F *Nomcut = new TH1F("Nominal", "Nominal", 27, 0, 27);
    const std::string banana = "Nominal";
    CutFlow.insert(std::pair<const std::string, TH1*>(banana, Nomcut));
    wk()->addOutput(CutFlow[banana]);

    if (doSyst) {
        systInfoList = objTool_77->getSystInfoList();
        for (const auto& sysInfo : systInfoList) {
            const CP::SystematicSet& sys = sysInfo.systset;
            TH1F *Syscut = new TH1F((sys.name()).c_str(), (sys.name()).c_str(), 27, 0, 27);
            CutFlow.insert(std::pair<const std::string, TH1*>((sys.name()).c_str(), Syscut));
            wk()->addOutput(CutFlow[(sys.name()).c_str()]);
        }
    }
    else {
        //This adds the "nominal variation"
        ST::SystInfo infodef;
        infodef.affectsKinematics = false;
        infodef.affectsWeights = false;
        infodef.affectsType = ST::Unknown;
        systInfoList.push_back(infodef);
    }

    TDirectory *out_TDir = (TDirectory*) wk()->getOutputFile ("output");

    mem_leaker = new OutTree("myTree");

    ANA_CHECK(mem_leaker->setProperty("SystematicList", systInfoList) ) ;
    ANA_CHECK(mem_leaker->setProperty("OutFile", out_TDir) );
    ANA_CHECK(mem_leaker->setProperty("DoTruth", doTruth) );
    ANA_CHECK(mem_leaker->setProperty("Format", format) );
    ANA_CHECK(mem_leaker->setProperty("DoBtagWP", doBtagWP) );
    ANA_CHECK(mem_leaker->setProperty("AddPhotons", savePhotons) );
    ANA_CHECK(mem_leaker->setProperty("IsData", isData) );
    ANA_CHECK(mem_leaker->setProperty("ListTrig", trig_list) );
    std::size_t found_sherpa = sample_name.find("Sherpa");
    if ( found_sherpa != std::string::npos ) ANA_CHECK(mem_leaker->setProperty("IsSherpa", true) );

    //List multi-weight samples here
    bool ismultiwei = false;
    std::size_t found_sherpa221 = sample_name.find("Sherpa_221_NNPDF30NNLO_l");
    if ( found_sherpa221 != std::string::npos ) ismultiwei = true;
    std::size_t found_aMcAtNlo = sample_name.find("aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_t");
    if ( found_aMcAtNlo != std::string::npos ) ismultiwei = true;
    ANA_CHECK(mem_leaker->setProperty("MultiWeight", ismultiwei) );

    bool hasextra = false;
    std::size_t found_SUSY2 = sample_name.find("SUSY2");
    if ( found_SUSY2 != std::string::npos ) hasextra = true;
    ANA_CHECK(mem_leaker->setProperty("HasExtraVar", hasextra) );

    ANA_CHECK(mem_leaker->initialize());
        
    if(m_event->getEntries() && wk()->metaData()->castDouble("isdata") != 1){
        NumberEvents->SetDirectory(out_TDir);
        EventsCount->SetDirectory(out_TDir);
    }
    
    counter_total=0;
    counter_vertex=0;
    counter_badmu=0;
    counter_cosmic=0;
    counter_badjet=0;
    counter_GRL=0;
    counter_errflags=0;
    counter_trig=0;
    counter_atfill=0;
    counter_2lep_sig_iso=0; 
    counter_2OR=0;
    //trig_list={"HLT_2e15_lhvloose_nod0_L12EM13VH","HLT_mu20_mu8noL1","HLT_e17_lhloose_nod0_mu14"};
    //trig_list_el={"HLT_2e15_lhvloose_nod0_L12EM13VH"};
    //trig_list_mu={"HLT_mu20_mu8noL1"};
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: execute ()
{
	const char *APP_NAME = "execute()";

	int count_baseline_leptons=0;
	int count_baseline_leptons_OR=0;
	int count_signal_leptons=0;



	if (m_debug) Info(APP_NAME, "New event" );

	if(m_event->getEntries() && wk()->metaData()->castDouble("isdata") != 1) EventsCount->Fill(1);
	counter_total++;

	trig_pass.clear();

	//Total Number of events, will be superseeded by InitialSumOfWeights
	for (auto iter : CutFlow) {
		CutFlow[iter.first]->Fill(0);
	}

	//----------------------------
	// Event information
	//---------------------------
	if (m_debug) Info(APP_NAME, "Retrieving event info collection." );

	const xAOD::EventInfo *eventInfo = 0;
	if ( ! m_event->retrieve( eventInfo, "EventInfo").isSuccess() ) {
		Error(APP_NAME, "Failed to retrieve event info collection. Exiting." );
		return EL::StatusCode::FAILURE;
	}

	// check if the event is data or MC
	bool isMC = eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION );
	ANA_CHECK( objTool_77->ApplyPRWTool());
	whichYear = objTool_77->treatAsYear();
	if (isMC) dec_RndRunNum(*eventInfo) = objTool_77->GetRunNumber();

	if (m_debug) Info(APP_NAME, "EventNumber %llu, isMC %d, year %d", eventInfo->eventNumber(), (int)isMC, whichYear );

	if (isMC) {
		if (processID == -1)dec_xsec(*eventInfo) = my_XsecDB->xsectTimesEff(eventInfo->mcChannelNumber());
		else dec_xsec(*eventInfo) = my_XsecDB->xsectTimesEff(eventInfo->mcChannelNumber(), processID);
	}
	else {
		dec_xsec(*eventInfo) = 1.;
	}

	dec_Sherpawei(*eventInfo) = 1.;

	const xAOD::VertexContainer* vertices = 0;
	if ( m_event->retrieve( vertices, "PrimaryVertices").isSuccess() ) {
		dec_PrimVx(*eventInfo) = vertices->size();
	}
	else {
		Error(APP_NAME, "Failed to retrieve Vertex container. Exiting." );
		return EL::StatusCode::FAILURE;
	}

	//------------
	// CLEANING CUTS HERE
	//------------
	for (auto iter : CutFlow) {
		CutFlow[iter.first]->Fill(1);
	}

	if (!isMC){
		bool testgrl = grl->passRunLB(*eventInfo);
		if(testgrl==0)  {
			m_store->clear();
			return EL::StatusCode::SUCCESS;
		}
	}
	counter_GRL++;

	if (!isMC &&
	   ((eventInfo->errorState(xAOD::EventInfo::LAr) == xAOD::EventInfo::Error ) ||
	   (eventInfo->errorState(xAOD::EventInfo::Tile) == xAOD::EventInfo::Error ) ||
	   (eventInfo->errorState(xAOD::EventInfo::SCT) == xAOD::EventInfo::Error) ||
	   (eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18) ))) {
		m_store->clear();
		return EL::StatusCode::SUCCESS; // go to next event
	}
	counter_errflags++;

	for (auto iter : CutFlow) {
		CutFlow[iter.first]->Fill(2);
	}

	//Filling Trigger decision
	bool at_least_one_trig = false;
	for (UInt_t it = 0; it < trig_list.size(); it++) {
		bool passed = objTool_77->IsTrigPassed(trig_list.at(it));
		trig_pass.push_back(passed);
		if (passed) at_least_one_trig = true;
		eventInfo->auxdecor< std::vector<bool> >("TrigPass") = trig_pass;
	}
	bool passMETtrig = objTool_77->IsMETTrigPassed();
	dec_passMETtrigger(*eventInfo) = passMETtrig;
	at_least_one_trig |= passMETtrig;

	//If no trigger has fired, don't bother going ahead 
	if (!at_least_one_trig) {
		m_store->clear();
		return EL::StatusCode::SUCCESS; // go to next event
	}
	counter_trig++;

	//Get the event Vertex
	if (m_debug) Info(APP_NAME, "Primary Vertex");
	const xAOD::Vertex* PrimVx = 0;
	PrimVx = objTool_77->GetPrimVtx();
	if (!PrimVx) {
		m_store->clear();
		return EL::StatusCode::SUCCESS;
	}
	counter_vertex++;

	// Get the nominal object containers from the event
	// Electrons
	if (m_debug) Info(APP_NAME, "Electrons");
	xAOD::ElectronContainer* electrons_nominal(0);
	xAOD::ShallowAuxContainer* electrons_nominal_aux(0);
	ANA_CHECK( objTool_77->GetElectrons(electrons_nominal, electrons_nominal_aux) );

	// Photons
	if (m_debug) Info(APP_NAME, "Photons");
	xAOD::PhotonContainer* photons_nominal(0);
	xAOD::ShallowAuxContainer* photons_nominal_aux(0);
	ANA_CHECK( objTool_77->GetPhotons(photons_nominal, photons_nominal_aux) );

	// Muons
	if (m_debug) Info(APP_NAME, "Muons");
	xAOD::MuonContainer* muons_nominal(0);
	xAOD::ShallowAuxContainer* muons_nominal_aux(0);
	ANA_CHECK( objTool_77->GetMuons(muons_nominal, muons_nominal_aux) );

	// Jets
	if (m_debug) Info(APP_NAME, "Jets");
	xAOD::JetContainer* jets_nominal(0);
	xAOD::ShallowAuxContainer* jets_nominal_aux(0);
	ANA_CHECK(objTool_77->GetJets(jets_nominal, jets_nominal_aux));

	// MET 
	if (m_debug) Info(APP_NAME, "MET");

	xAOD::MissingETContainer* met_nominal = new xAOD::MissingETContainer;
	xAOD::MissingETAuxContainer* met_nominal_aux = new xAOD::MissingETAuxContainer;
	met_nominal->setStore(met_nominal_aux);

	int ttbarHF = 0, ttbarHF_ext = 0, ttbarHF_prompt = 0;

	// Ttbar + HF classification
	if (isMC) {

		if (m_debug) Info(APP_NAME, "Running TTbar classification");

		if (eventInfo->mcChannelNumber() == 410000 || eventInfo->mcChannelNumber() == 407012 || eventInfo->mcChannelNumber() == 407009 || eventInfo->mcChannelNumber() == 407010) {
			ClassifyAndCalculateHF classHF(m_event, "TruthParticles", "AntiKt4TruthJets", true);
			classHF.apply(myHFClassData, ttbarHF, ttbarHF_ext, ttbarHF_prompt);
		}
		dec_HFclass(*eventInfo) = ttbarHF_ext;
	}
	//------------
	// LOOP ON SYSTEMATICS
	//------------
	int ret = 0;
	bool isNominal = true;

	// Generic pointers for either nominal or systematics copy
	xAOD::ElectronContainer* electrons(electrons_nominal);
	xAOD::PhotonContainer* photons(photons_nominal);
	xAOD::MuonContainer* muons(muons_nominal);
	xAOD::JetContainer* jets(jets_nominal);
	xAOD::MissingETContainer* met(met_nominal);
	xAOD::MissingETAuxContainer* met_aux(met_nominal_aux);

	for ( const auto& sysInfo : systInfoList ) {

		const CP::SystematicSet& sys = sysInfo.systset;

		bool syst_affectsElectrons = ST::testAffectsObject(xAOD::Type::Electron, sysInfo.affectsType);
		bool syst_affectsMuons = ST::testAffectsObject(xAOD::Type::Muon, sysInfo.affectsType);
		bool syst_affectsTaus = ST::testAffectsObject(xAOD::Type::Tau, sysInfo.affectsType);
		bool syst_affectsPhotons = ST::testAffectsObject(xAOD::Type::Photon, sysInfo.affectsType);
		bool syst_affectsJets = ST::testAffectsObject(xAOD::Type::Jet, sysInfo.affectsType);
		//bool syst_affectsBTag = ST::testAffectsObject(xAOD::Type::BTag, sysInfo.affectsType);

		if ( syst_affectsPhotons && !syst_affectsElectrons ) continue;
		if ( syst_affectsTaus ) continue;
		std::size_t found_PRW = sys.name().find("PRW");
		//if ( found_PRW != std::string::npos ) continue;

		std::size_t found_Rtrk = sys.name().find("JET_Rtrk");
		if ( found_Rtrk != std::string::npos ) continue;

		// Tell the SUSYObjDef_xAOD which variation to apply
		if ( objTool_77->applySystematicVariation(sys) != CP::SystematicCode::Ok ) {
			Error(APP_NAME, "Cannot configure SUSYTools for systematic var. %s", (sys.name()).c_str() );
		} 
		else {
			if (m_debug) Info(APP_NAME, "Variation \"%s\" configured...", (sys.name()).c_str() );
			if ( found_PRW != std::string::npos ) ANA_CHECK( objTool_77->ApplyPRWTool());
			//std::cout << sys.name() << " - " << eventInfo->auxdata< float >( "PileupWeight" ) << std::endl;
		}
		if ( sysInfo.affectsKinematics || sysInfo.affectsWeights ) isNominal = false;

		if ( sysInfo.affectsKinematics ) {

			if ( syst_affectsElectrons ) {
				xAOD::ElectronContainer* electrons_syst(0);
				xAOD::ShallowAuxContainer* electrons_syst_aux(0);
				ANA_CHECK( objTool_77->GetElectrons(electrons_syst, electrons_syst_aux) );
				electrons = electrons_syst;
			}

			if ( syst_affectsMuons ) {
				xAOD::MuonContainer* muons_syst(0);
				xAOD::ShallowAuxContainer* muons_syst_aux(0);
				ANA_CHECK( objTool_77->GetMuons(muons_syst, muons_syst_aux) );
				muons = muons_syst;
			}

			if ( syst_affectsPhotons ) {
				xAOD::PhotonContainer* photons_syst(0);
				xAOD::ShallowAuxContainer* photons_syst_aux(0);
				ANA_CHECK( objTool_77->GetPhotons(photons_syst, photons_syst_aux) );
				photons = photons_syst;
			}

			if ( syst_affectsJets ) {
				xAOD::JetContainer* jets_syst(0);
				xAOD::ShallowAuxContainer* jets_syst_aux(0);
				ANA_CHECK( objTool_77->GetJets(jets_syst, jets_syst_aux) );
				jets = jets_syst;
			}

			xAOD::MissingETContainer*    met_syst = new xAOD::MissingETContainer;
			xAOD::MissingETAuxContainer* met_syst_aux = new xAOD::MissingETAuxContainer;
			met_syst->setStore(met_syst_aux);
			met = met_syst;
			met_aux = met_syst_aux;

		}

		if (m_debug) Info(APP_NAME, "Objects retrieved");

		//------------
		// OVERLAP REMOVAL
		//------------
		if ( isNominal || ( sysInfo.affectsKinematics && (syst_affectsElectrons || syst_affectsMuons || syst_affectsJets ) ) ) {
			if (savePhotons) {
				ANA_CHECK( objTool_77->OverlapRemoval(electrons, muons, jets, photons) );
			}
			else {
				ANA_CHECK( objTool_77->OverlapRemoval(electrons, muons, jets) );
			}
		}

		if (m_debug) Info(APP_NAME, "Overlaps Removed");

		if ( isNominal || sysInfo.affectsKinematics ) {
			double metSignificance = 0;

			ANA_CHECK( objTool_77->GetMET(*met, jets, electrons, muons, photons, 0, true, true) );
			ANA_CHECK( objTool_77->GetMETSig(*met, metSignificance, true, true) );
			//ANA_CHECK( objTool_77->GetMETSig(*met, metSignificance, jets, electrons, muons, photons, 0, true, true, 0) );


			float Etmiss_Etx = 0.;
			float Etmiss_Ety = 0.;
			float Etmiss_Et = 0.;

                        TLorentzVector MET_E(0.,0.,0.,0.), MET_M(0.,0.,0.,0.), MET_J(0.,0.,0.,0.), MET_G(0.,0.,0.,0.), MET_Soft2(0.,0.,0.,0.);

			xAOD::MissingETContainer::const_iterator met_iterEle = met->find("RefEle");
                        if (met_iterEle == met->end()) Error( APP_NAME, "No RefEle inside MET container" );
                        else {
                            Etmiss_Etx=(*met_iterEle)->mpx();
                            Etmiss_Ety=(*met_iterEle)->mpy();
                            Etmiss_Et=sqrt(Etmiss_Etx * Etmiss_Etx + Etmiss_Ety * Etmiss_Ety);
                            if(m_debug) std::cout << "Xe: " << (*met_iterEle)->mpx() << " Ye: " << (*met_iterEle)->mpy() << " MODULE: " << Etmiss_Et << std::endl;

			    MET_E.SetPxPyPzE(Etmiss_Etx, Etmiss_Ety, 0., Etmiss_Et);
                            if(m_debug) std::cout << "ElePerp: " << MET_E.Perp() << "\n" << std::endl;

			    eventInfo->auxdecor<float>("MET_RefEle") = MET_E.Perp();
			    eventInfo->auxdecor<float>("MET_RefElePhi") = MET_E.Phi();
                        }

			xAOD::MissingETContainer::const_iterator met_iterMu = met->find("Muons");
                        if (met_iterMu == met->end()) Error( APP_NAME, "No Muons MET inside MET container" );
                        else {
                            Etmiss_Etx=(*met_iterMu)->mpx();
                            Etmiss_Ety=(*met_iterMu)->mpy();
                            Etmiss_Et=sqrt(Etmiss_Etx * Etmiss_Etx + Etmiss_Ety * Etmiss_Ety);
                            if(m_debug) std::cout << "Xm: " << (*met_iterMu)->mpx() << " Ym: " << (*met_iterMu)->mpy() << " MODULE: " << Etmiss_Et << std::endl;

			    MET_M.SetPxPyPzE(Etmiss_Etx, Etmiss_Ety, 0., Etmiss_Et);
                            if(m_debug) std::cout << "MuonPerp: " << MET_M.Perp() << "\n" << std::endl;

			    eventInfo->auxdecor<float>("MET_RefMu") = MET_M.Perp();
			    eventInfo->auxdecor<float>("MET_RefMuPhi") = MET_M.Phi();
                        }

			xAOD::MissingETContainer::const_iterator met_iterJet = met->find("RefJet");
                        if (met_iterJet == met->end()) Error( APP_NAME, "No RefJet inside MET container" );
                        else {
                            Etmiss_Etx=(*met_iterJet)->mpx();
                            Etmiss_Ety=(*met_iterJet)->mpy();
                            Etmiss_Et=sqrt(Etmiss_Etx * Etmiss_Etx + Etmiss_Ety * Etmiss_Ety);
                            if(m_debug) std::cout << "Xj: " << (*met_iterJet)->mpx() << " Yj: " << (*met_iterJet)->mpy() << " MODULE: " << Etmiss_Et << std::endl;

			    MET_J.SetPxPyPzE(Etmiss_Etx, Etmiss_Ety, 0., Etmiss_Et);
                            if(m_debug) std::cout << "JetPerp: " << MET_J.Perp() << "\n" << std::endl;

			    eventInfo->auxdecor<float>("MET_RefJet") = MET_J.Perp();
			    eventInfo->auxdecor<float>("MET_RefJetPhi") = MET_J.Phi();
                        }

			xAOD::MissingETContainer::const_iterator met_iterGam = met->find("RefGamma");
                        if (met_iterGam == met->end()) Error( APP_NAME, "No RefGamma inside MET container" );
                        else {
                            Etmiss_Etx=(*met_iterGam)->mpx();
                            Etmiss_Ety=(*met_iterGam)->mpy();
                            Etmiss_Et=sqrt(Etmiss_Etx * Etmiss_Etx + Etmiss_Ety * Etmiss_Ety);
                            if(m_debug) std::cout << "Xg: " << (*met_iterGam)->mpx() << " Yg: " << (*met_iterGam)->mpy() << " MODULE: " << Etmiss_Et << std::endl;

			    MET_G.SetPxPyPzE(Etmiss_Etx, Etmiss_Ety, 0., Etmiss_Et);
                            if(m_debug) std::cout << "GammaPerp: " << MET_G.Perp() << "\n" << std::endl;

			    eventInfo->auxdecor<float>("MET_RefGam") = MET_G.Perp();
			    eventInfo->auxdecor<float>("MET_RefGamPhi") = MET_G.Phi();
                        }

			xAOD::MissingETContainer::const_iterator met_it = met->find("Final");
			if (met_it == met->end()) {
				Error( APP_NAME, "No RefFinal inside MET container" );
			} 
			else {
				Etmiss_Etx = (*met_it)->mpx();
				Etmiss_Ety = (*met_it)->mpy();
				Etmiss_Et = sqrt(Etmiss_Etx * Etmiss_Etx + Etmiss_Ety * Etmiss_Ety);

				TLorentzVector MET(Etmiss_Etx, Etmiss_Ety, 0., Etmiss_Et);

				dec_MET(*eventInfo) = MET.Perp();
                                if(m_debug) std::cout << "XFinal: " << (*met_it)->mpx() << " YFinal: " << (*met_it)->mpy() << " MODULE: " << Etmiss_Et << " METFinal Perp: " << MET.Perp() << std::endl;
				dec_MET_sig(*eventInfo) =  metSignificance;//TODO
				dec_METphi(*eventInfo) = MET.Phi();
			}

			//Retrieve the Trk soft term
			xAOD::MissingETContainer::const_iterator met_softtrk = met->find("PVSoftTrk");
			if (met_softtrk == met->end()) {
				Error( APP_NAME, "No PVSoftTrk inside MET container" );
			} 
			else {
				Etmiss_Etx = (*met_softtrk)->mpx();
				Etmiss_Ety = (*met_softtrk)->mpy();
				Etmiss_Et = sqrt(Etmiss_Etx * Etmiss_Etx + Etmiss_Ety * Etmiss_Ety);

				TLorentzVector MET_Soft(Etmiss_Etx, Etmiss_Ety, 0., Etmiss_Et);
			        MET_Soft2.SetPxPyPzE(Etmiss_Etx, Etmiss_Ety, 0., Etmiss_Et);

				dec_MET_Soft(*eventInfo) = MET_Soft.Perp();
                                if(m_debug) std::cout << "MET Soft Track Perp: " << MET_Soft.Perp() << std::endl;
				dec_MET_Softphi(*eventInfo) = MET_Soft.Phi();

				eventInfo->auxdecor<float>("MET_RefSoft") = MET_Soft.Perp();
			 	eventInfo->auxdecor<float>("MET_RefSoftPhi") = MET_Soft.Phi();
			}

                        TLorentzVector metsum = MET_E + MET_M + MET_J + MET_G + MET_Soft2;
                        if(m_debug) std::cout << "------------> MET SUM of electrons, muons, jets, photons, soft terms: " << metsum.Perp() << " <-------------" << std::endl;
		}

		if (m_debug) Info(APP_NAME, "MET recomputed");

		bool skip = false;

		if (m_debug) Info(APP_NAME, "Trigger DEcoration");
		if (isMC && ( isNominal || syst_affectsElectrons ) ) {
			//trigger related decoration
			dec_single_lep_e(*eventInfo) = 1;//objTool_77->GetTotalElectronSF(*electrons, false, false, true, false, objTool_77->TrigSingleLep());
			dec_double_lep_e(*eventInfo) = 1;//objTool_77->GetTotalElectronSF(*electrons, false, false, true, false, objTool_77->TrigDiLep());
			dec_mix_lep_e(*eventInfo) = 1;//objTool_77->GetTotalElectronSF(*electrons, false, false, true, false, objTool_77->TrigMixLep());
		}
		else {
			dec_single_lep_e(*eventInfo) = 1;
			dec_double_lep_e(*eventInfo) = 1;
			dec_mix_lep_e(*eventInfo) = 1;
		}

		if (m_debug) Info(APP_NAME, "Computed Electron Trigger Scale factors");

		bool passTM_single = false;
		bool passTM_double = false;
		bool passTM_mix = false;

		if ( isNominal || ( sysInfo.affectsKinematics && syst_affectsMuons ) ) {
			for (const auto& mu : *muons) {
				objTool_77->IsHighPtMuon(*mu);

				if ( mu->auxdata< char >("passOR") == 1 && mu->auxdata<char>("cosmic") == 1) {
					skip=true;
					//skipcosmic=true;
				}
				if ( mu->auxdata<char>("baseline")==1 && mu->auxdata< char >("bad") == 1){skip=true;/*skipbad=true;*/}
				objTool_77->TrigMatch(mu, trig_list_mu);
				if ( mu->auxdata<char>("baseline")==1) count_baseline_leptons++;
				if ( mu->auxdata<char>("baseline")==1 && mu->auxdata< char >("passOR") == 1) count_baseline_leptons_OR++;
				if ( mu->auxdata< char >("passOR") == 1 && mu->auxdata<char>("signal") == 1) count_signal_leptons++;
                               	if (!isMC){
                                   dec_match_2e24_lhvloose_nod0(*mu) = objTool_77->IsTrigMatched(mu,"HLT_2e24_lhvloose_nod0");   
                                   dec_match_mu22_mu8noL1(*mu) = objTool_77->IsTrigMatched(mu,"HLT_mu22_mu8noL1");  
                                   dec_match_e17_lhloose_nod0_mu14(*mu) = objTool_77->IsTrigMatched(mu,"HLT_e17_lhloose_nod0_mu14");  
                                   dec_match_2e15_lhvloose_nod0_L12EM13VH(*mu) = objTool_77->IsTrigMatched(mu,"HLT_2e15_lhvloose_nod0_L12EM13VH");  
                                   dec_match_2e17_lhvloose_nod0(*mu) = objTool_77->IsTrigMatched(mu,"HLT_2e17_lhvloose_nod0");  
                                   dec_match_mu20_mu8noL1(*mu) = objTool_77->IsTrigMatched(mu,"HLT_mu20_mu8noL1");  
                                   dec_match_2e12_lhloose_L12EM10VH(*mu) = objTool_77->IsTrigMatched(mu,"HLT_2e12_lhloose_L12EM10VH");  
                                   dec_match_mu18_mu8noL1(*mu) = objTool_77->IsTrigMatched(mu,"HLT_mu18_mu8noL1");  
                                   dec_match_e17_lhloose_mu14(*mu) = objTool_77->IsTrigMatched(mu,"HLT_e17_lhloose_mu14");  
                                }

			}
		}

		if (isNominal){
			for (const auto& ele : *electrons) {
				if ( ele->auxdata<char>("baseline")==1) count_baseline_leptons++;
				if ( ele->auxdata< char >("passOR") == 1 && ele->auxdata<char>("signal") == 1) count_signal_leptons++;
				if ( ele->auxdata<char>("baseline")==1 && ele->auxdata< char >("passOR") == 1) count_baseline_leptons_OR++;
                               	if (!isMC){
                                   dec_match_2e24_lhvloose_nod0(*ele) = objTool_77->IsTrigMatched(ele,"HLT_2e24_lhvloose_nod0");   
                                   dec_match_mu22_mu8noL1(*ele) = objTool_77->IsTrigMatched(ele,"HLT_mu22_mu8noL1");  
                                   dec_match_e17_lhloose_nod0_mu14(*ele) = objTool_77->IsTrigMatched(ele,"HLT_e17_lhloose_nod0_mu14");  
                                   dec_match_2e15_lhvloose_nod0_L12EM13VH(*ele)=objTool_77->IsTrigMatched(ele,"HLT_2e15_lhvloose_nod0_L12EM13VH");  
                                   dec_match_2e17_lhvloose_nod0(*ele) = objTool_77->IsTrigMatched(ele,"HLT_2e17_lhvloose_nod0");  
                                   dec_match_mu20_mu8noL1(*ele) = objTool_77->IsTrigMatched(ele,"HLT_mu20_mu8noL1");  
                                   dec_match_2e12_lhloose_L12EM10VH(*ele) = objTool_77->IsTrigMatched(ele,"HLT_2e12_lhloose_L12EM10VH");  
                                   dec_match_mu18_mu8noL1(*ele) = objTool_77->IsTrigMatched(ele,"HLT_mu18_mu8noL1");  
                                   dec_match_e17_lhloose_mu14(*ele) = objTool_77->IsTrigMatched(ele,"HLT_e17_lhloose_mu14");  
                                }
			}
		}

		if (isMC && ( isNominal || syst_affectsMuons ) ) {
			//trigger related decorations
			if (whichYear == 2015) {
				dec_single_lep_m(*eventInfo) = 1.;
				dec_double_lep_m(*eventInfo) = 1.;
				dec_mix_lep_m(*eventInfo) = 1.;
				// f**k this s**t
				if ( passTM_single && (objTool_77->IsTrigPassed("HLT_mu20_iloose_L1MU150") || objTool_77->IsTrigPassed("HLT_mu50")) ) {
					if (m_debug) Info(APP_NAME, "mu single SF 2015");
					dec_single_lep_m(*eventInfo) = objTool_77->GetTotalMuonTriggerSF( *muons, muTrigSingle2015.Data());
				}
				else {
					dec_single_lep_m(*eventInfo) = 1.;
				}
				if ( passTM_double && (objTool_77->IsTrigPassed("HLT_mu18_mu8noL1")) ) {
					if (m_debug) Info(APP_NAME, "mu double SF 2015");
					dec_double_lep_m(*eventInfo) = objTool_77->GetTotalMuonTriggerSF( *muons, muTrigDouble2015.Data());
				}
				else {
					dec_double_lep_m(*eventInfo) = 1.;
				}
				if ( passTM_mix && (objTool_77->IsTrigPassed("HLT_e17_lhloose_mu14")) ) {
					if (m_debug) Info(APP_NAME, "mu mix SF 2015");
					dec_mix_lep_m(*eventInfo) = objTool_77->GetTotalMuonTriggerSF( *muons, muTrigMix2015.Data());
				}
				else {
					dec_mix_lep_m(*eventInfo) = 1.;
				}
			}
			else {
				if ( passTM_single && (objTool_77->IsTrigPassed("HLT_mu26_ivarmedium") || objTool_77->IsTrigPassed("HLT_mu50")) ) {
					if (m_debug) Info(APP_NAME, "mu single SF 2016");
					dec_single_lep_m(*eventInfo) = objTool_77->GetTotalMuonTriggerSF( *muons, muTrigSingle2016.Data());
				}
				else {
					dec_single_lep_m(*eventInfo) = 1.;
				}
				if ( passTM_double && (objTool_77->IsTrigPassed("HLT_mu22_mu8noL1")) ) {
					if (m_debug) Info(APP_NAME, "mu double SF 2016");
					dec_double_lep_m(*eventInfo) = objTool_77->GetTotalMuonTriggerSF( *muons, muTrigDouble2016.Data());
				}
				else {
					dec_double_lep_m(*eventInfo) = 1.;
				}
				if ( passTM_mix && (objTool_77->IsTrigPassed("HLT_e17_lhloose_nod0_mu14")) ) {
					if (m_debug) Info(APP_NAME, "mu mix SF 2016");
					dec_mix_lep_m(*eventInfo) = objTool_77->GetTotalMuonTriggerSF( *muons, muTrigMix2016.Data());
				}
				else {
					dec_mix_lep_m(*eventInfo) = 1.;
				}
			}
		}
		else {
			dec_single_lep_m(*eventInfo) = 1.;
			dec_double_lep_m(*eventInfo) = 1.;
			dec_mix_lep_m(*eventInfo) = 1.;
		}

		if (m_debug) Info(APP_NAME, "Computed Muon Trigger Scale factors");

		if ( isNominal || ( sysInfo.affectsKinematics && (syst_affectsElectrons || syst_affectsMuons || syst_affectsJets ) ) ) {
			int njet=0;
			//std::cout<<objTool_77->currentSystematic().name()<<std::endl;
                        if (m_debug) Info(APP_NAME, "hello 1");
			for (const auto& jet : *jets) {
				if ( (int)jet->auxdata< char >("passOR")==1 && (int)jet->auxdata< char >("bad")==1) {
					skip = true;
					/*skip_badjet=true;*/
				}
				//std::cout<<"\tjet "<<njet<<" pt: "<< jet->pt() << std::endl;
				njet++;
				objTool_77->IsBJet(*jet);
				dec_isB85tag(*jet) = (bool)jet->auxdata< char >("bjet");
				if (doBtagWP) {
					objTool_70->IsBJet(*jet);
					dec_isB70tag(*jet) = (bool)jet->auxdata< char >("bjet");
					objTool_85->IsBJet(*jet);
					dec_isB85tag(*jet) = (bool)jet->auxdata< char >("bjet");
					objTool_70Flat->IsBJet(*jet);
					dec_isB70tagFlat(*jet) = (bool)jet->auxdata< char >("bjet");
					objTool_77Flat->IsBJet(*jet);
					dec_isB77tagFlat(*jet) = (bool)jet->auxdata< char >("bjet");
					objTool_85Flat->IsBJet(*jet);
					dec_isB85tagFlat(*jet) = (bool)jet->auxdata< char >("bjet");
				}
			}
		}
        if (m_debug) Info(APP_NAME, "hello 2");
        std::string tempTrigList[] = {"HLT_2e12_lhloose_L12EM10VH","HLT_2e17_lhvloose_nod0","HLT_2e17_lhvloose_nod0_L12EM15VHI","HLT_mu18_mu8noL1","HLT_mu22_mu8noL1","HLT_e17_lhloose_mu14","HLT_e17_lhloose_nod0_mu14"};
        
        /*bool passedTrig=false;

        for (int i=0; i<7; i++){
                if (objTool_77->IsTrigPassed(tempTrigList[i])){
                    passedTrig=true;
                }
        }

        if(!passedTrig){
            std::cout << "NOT TRIGGERED" << std::endl;
            m_store->clear();
            return EL::StatusCode::SUCCESS;
        }*/


		if (isMC) {
			eventInfo->auxdecor<float>("eleSF_weight") = objTool_77->GetTotalElectronSF(*electrons, true, true, false, true);
			eventInfo->auxdecor<float>("muSF_weight") = objTool_77->GetTotalMuonSF(*muons, true, false, "");
			eventInfo->auxdecor<float>("bTag_weight") = objTool_77->BtagSF(jets);
			eventInfo->auxdecor<float>("JVT_weight") = objTool_77->JVT_SF(jets);
			eventInfo->auxdecor<float>("globalSF_weight") = objTool_77->GetTriggerGlobalEfficiencySF(*electrons_nominal, *muons_nominal, "diLepton");
          	eventInfo->auxdecor<float>("globalTrig_weight") = objTool_77->GetTriggerGlobalEfficiency(*electrons_nominal, *muons_nominal, "diLepton");


			if (eventInfo->mcChannelNumber() >= 363102 && eventInfo->mcChannelNumber() <= 363435) {
				float sherpa22_wei = objTool_77->getSherpaVjetsNjetsWeight();
				dec_Sherpawei(*eventInfo) = sherpa22_wei;
			}

			//Retrieve the truth particles
			const xAOD::TruthParticleContainer* truthP(0);
			ANA_CHECK( m_event->retrieve( truthP, "TruthParticles" ) );
			int ttDecay = -1;

			std::size_t found_powheg = sample_name.find("PowhegPythia");
			if (found_powheg != std::string::npos) {
				Fill_decay_Cat(ttDecay, truthP);
			}
			dec_DecayType(*eventInfo) = ttDecay;

		}
		dec_cleaning(*eventInfo) = skip;
                if ( m_debug ) std::cout << "here come the bug!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! " << std::endl;
		ret +=  mem_leaker->process(m_event, electrons, photons, muons, jets, objTool_77->currentSystematic().name());
		// Clean up the systematics copies
		if (sysInfo.affectsKinematics) {
			delete met;
			delete met_aux;
		}
	}
	if (ret > 0) {
		ANA_CHECK(mem_leaker->FillTree());
		counter_atfill++;
	}

	ANA_CHECK( objTool_77->resetSystematics());
	m_store->clear();

	delete met_nominal;
	delete met_nominal_aux;
	//delete electrons_nominal;
	//delete muons_nominal;
	//delete jets_nominal;

	if (m_debug) Info( APP_NAME, "Passing to next event" );

	return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: postExecute ()
{
    // Here you do everything that needs to be done after the main event
    // processing.  This is typically very rare, particularly in user
    // code.  It is mainly used in implementing the NTupleSvc.
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: finalize ()
{
    // This method is the mirror image of initialize(), meaning it gets
    // called after the last event has been processed on the worker node
    // and allows you to finish up any objects you created in
    // initialize() before they are written to disk.  This is actually
    // fairly rare, since this happens separately for each worker node.
    // Most of the time you want to do your post-processing on the
    // submission node after all your histogram outputs have been
    // merged.  This is different from histFinalize() in that it only
    // gets called on worker nodes that processed input events.

    const char *APP_NAME = "finalize()";
    Info(APP_NAME, "All done. Now cleaning up...");

    Info(APP_NAME, "Total: %d", counter_total);
    Info(APP_NAME, "GRL: %d", counter_GRL);
    Info(APP_NAME, "Error Flags: %d", counter_errflags);
    Info(APP_NAME, "Vertex: %d", counter_vertex);
    Info(APP_NAME, "Trigger: %d", counter_trig);
    Info(APP_NAME, "Badmu: %d", counter_badmu);
    Info(APP_NAME, "Cosmic: %d", counter_cosmic);
    Info(APP_NAME, "Badjet: %d", counter_badjet);
    Info(APP_NAME, ">=1 baseline: %d", counter_atfill);
    Info(APP_NAME, ">=2 baseline OR: %d", counter_2lep_sig_iso);
    Info(APP_NAME, "2 signal + iso: %d", counter_2OR);
    if ( my_XsecDB ) {
        delete my_XsecDB;
        my_XsecDB = 0;
    }

    if ( objTool_77 ) {
        delete objTool_77;
        objTool_77 = 0;
    }

    if ( objTool_70 ) {
        delete objTool_70;
        objTool_70 = 0;
    }

    if ( objTool_85 ) {
        delete objTool_85;
        objTool_85 = 0;
    }

    if ( objTool_70Flat ) {
        delete objTool_70Flat;
        objTool_70Flat = 0;
    }

    if ( objTool_77Flat ) {
        delete objTool_77Flat;
        objTool_77Flat = 0;
    }

    if ( objTool_85Flat ) {
        delete objTool_85Flat;
        objTool_85Flat = 0;
    }

    if (mem_leaker) {
        delete mem_leaker;
        mem_leaker = 0;
    }

    return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: histFinalize ()
{
    // This method is the mirror image of histInitialize(), meaning it
    // gets called after the last event has been processed on the worker
    // node and allows you to finish up any objects you created in
    // histInitialize() before they are written to disk.  This is
    // actually fairly rare, since this happens separately for each
    // worker node.  Most of the time you want to do your
    // post-processing on the submission node after all your histogram
    // outputs have been merged.  This is different from finalize() in
    // that it gets called on all worker nodes regardless of whether
    // they processed input events.
    return EL::StatusCode::SUCCESS;
}
