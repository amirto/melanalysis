#ifndef MELANALYSIS_LINKDEF_H
#define MELANALYSIS_LINKDEF_H

#include <vector>
#include <string>
#include <map>
#include <utility>

#include <MelAnalysis/MyxAODAnalysis.h>
#include <MelAnalysis/MyxAODTruth.h>
#include <MelAnalysis/OutTree.h>
#include <MelAnalysis/ttbarDecay.h>
#include <MelAnalysis/ClassifyAndCalculateHF.h>
#include <MelAnalysis/HFSystDataMembers.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#endif

#ifdef __CINT__
#pragma link C++ class MyxAODTruth+;
#pragma link C++ class MyxAODAnalysis+;
#pragma link C++ class OutTree+;
#pragma link C++ class ClassifyAndCalculateHF+;
#pragma link C++ class HFSystDataMembers+;
#pragma link C++ class pair<string,TTree >+;
#endif

#endif
