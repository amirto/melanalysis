#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <MelAnalysis/MyxAODTruth.h>
#include "EventLoop/OutputStream.h"
#include "FourMomUtils/xAODP4Helpers.h"

// EDM includes:
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthEvent.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"

#include <AsgTools/MessageCheck.h>

#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"

#ifdef ROOTCORE
#   include "xAODRootAccess/Init.h"
#   include "xAODRootAccess/TEvent.h"
#endif // ROOTCORE


#include "PMGTools/PMGTruthWeightTool.h"
#include "AsgTools/MessageCheck.h"
#include "AsgTools/AnaToolHandle.h"


using namespace xAOD;

static SG::AuxElement::Decorator<bool> dec_passOR("passTruthOR");

// this is needed to distribute the algorithm to the workers
ClassImp(MyxAODTruth)

MyxAODTruth :: MyxAODTruth (): my_XsecDB(0)
{
    // Here you put any code for the base initialization of variables,
    // e.g. initialize all pointers to 0.  Note that you should only put
    // the most basic initialization here, since this method will be
    // called on both the submission and the worker node.  Most of your
    // initialization code will go into histInitialize() and
    // initialize().

}

EL::StatusCode MyxAODTruth :: setupJob (EL::Job &job)
{
    // Here you put code that sets up the job on the submission object
    // so that it is ready to work with your algorithm, e.g. you can
    // request the D3PDReader service or add output files.  Any code you
    // put here could instead also go into the submission script.  The
    // sole advantage of putting it here is that it gets automatically
    // activated/deactivated when you add/remove the algorithm from your
    // job, which may or may not be of value to you.
    job.useXAOD ();

    // let's initialize the algorithm to use the xAODRootAccess package
    xAOD::Init( "MyxAODTruth" ).ignore(); // call before opening first file

    // tell EventLoop about our output:
    EL::OutputStream out ("output");
    job.outputAdd (out);

    return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODTruth :: histInitialize ()
{
    // Here you do everything that needs to be done at the very
    // beginning on each worker node, e.g. create histograms and output
    // trees.  This method gets called before any input files are
    // connected.

    if(MultiWeight) {
      NumberEvents = new TH1D("NumberEvents", "Number Events", 9, 0, 9);
      NumberEvents->GetXaxis()->SetBinLabel(4, "Weight_MUR05_MUF05");
      NumberEvents->GetXaxis()->SetBinLabel(5, "Weight_MUR05_MUF1");
      NumberEvents->GetXaxis()->SetBinLabel(6, "Weight_MUR1_MUF05");
      NumberEvents->GetXaxis()->SetBinLabel(7, "Weight_MUR1_MUF2");
      NumberEvents->GetXaxis()->SetBinLabel(8, "Weight_MUR2_MUF1");
      NumberEvents->GetXaxis()->SetBinLabel(9, "Weight_MUR2_MUF2");
    }
    else NumberEvents = new TH1D("NumberEvents", "Number Events", 3, 0, 3);
    NumberEvents->GetXaxis()->SetBinLabel(1, "Raw");
    NumberEvents->GetXaxis()->SetBinLabel(2, "Weights");
    NumberEvents->GetXaxis()->SetBinLabel(3, "WeightsSquared");


    return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODTruth :: fileExecute ()
{
    // Here you do everything that needs to be done exactly once for every
    // single file, e.g. collect a list of all lumi-blocks processed
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODTruth :: changeInput (bool firstFile)
{

    // Here you do everything you need to do when we change input files,
    // e.g. resetting branch addresses on trees.  If you are using
    // D3PDReader or a similar service this method is not needed.

    const char *APP_NAME = "changeInput()";

    m_event = wk()->xaodEvent();
    Info( APP_NAME, "Number of events in this file = %lli", m_event->getEntries() ); // print long long int
    
    if (skipCBK) {
        NumberEvents->Fill(0., m_event->getEntries());
    }
    else {

        //Read the CutBookkeeper container
        const xAOD::CutBookkeeperContainer* completeCBC = 0;
        if (!m_event->retrieveMetaInput(completeCBC, "CutBookkeepers").isSuccess()) {
            Error( APP_NAME, "Failed to retrieve CutBookkeepers from MetaData! Exiting.");
        }

        const xAOD::CutBookkeeper* allEventsCBK = 0;
        int maxcycle = -1;

        //let's find the right CBK (latest with StreamAOD input before derivations)
        for ( auto cbk : *completeCBC ) {
            if ( cbk->name() == "AllExecutedEvents" && cbk->inputStream() == "StreamAOD" && cbk->cycle() > maxcycle) {
                maxcycle = cbk->cycle();
                allEventsCBK = cbk;
            }
        }

        uint64_t nEventsProcessed  = 0;
        double sumOfWeights        = 0.;
        double sumOfWeightsSquared = 0.;
        if (allEventsCBK) {
            nEventsProcessed  = allEventsCBK->nAcceptedEvents();
            sumOfWeights        = allEventsCBK->sumOfEventWeights();
            sumOfWeightsSquared = allEventsCBK->sumOfEventWeightsSquared();
            Info( APP_NAME, "CutBookkeepers Accepted %lu SumWei %f sumWei2 %f ", nEventsProcessed, sumOfWeights, sumOfWeightsSquared);
        } else {
            Info( APP_NAME, "No relevent CutBookKeepers found" );
            nEventsProcessed = m_event->getEntries();
        }

        NumberEvents->Fill(0., nEventsProcessed);
        NumberEvents->Fill(1., sumOfWeights);
        NumberEvents->Fill(2., sumOfWeightsSquared);
    }

    return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODTruth :: initialize ()
{
    // Here you do everything that you need to do after the first input
    // file has been connected and before the first event is processed,
    // e.g. create additional histograms based on which variables are
    // available in the input files.  You can also create all of your
    // histograms and trees in here, but be aware that this method
    // doesn't get called if no events are processed.  So any objects
    // you create here won't be available in the output if you have no
    // input events.
    const char *APP_NAME = "MyxAODTruth::initialize()";

    m_event = wk()->xaodEvent();

    my_XsecDB = new SUSY::CrossSectionDB(gSystem->ExpandPathName("$ROOTCOREBIN/data/SUSYTools/mc15_13TeV/"));
    if (m_debug) Info(APP_NAME, "xsec DB initialized" );

    TDirectory *out_TDir = (TDirectory*) wk()->getOutputFile ("output");
    NumberEvents->SetDirectory(out_TDir);

    mc_eta = new std::vector<float>();
    mc_pt = new std::vector<float>();
    mc_mass = new std::vector<float>();
    mc_phi = new std::vector<float>();
    mc_pdgid = new std::vector<int>();
    mc_parent_pdgid = new std::vector<int>();

    ptjets_truth = new std::vector<float>();
    etajets_truth = new std::vector<float>();
    phijets_truth = new std::vector<float>();
    massjets_truth = new std::vector<float>();
    labeljets_truth = new std::vector<float>();

    TFile *file_ntup = wk()->getOutputFile ("output");
    truthTree = new TTree("susytree", "a truth Tree");
    truthTree->SetDirectory(file_ntup);
    truthTree->Branch("EventNumber", &m_EventNumber);
    truthTree->Branch("RunNumber", &m_RunNumber);
    truthTree->Branch("xsec", &m_crossSection);
    truthTree->Branch("EventWeight", &m_WeightEvents);
    if(MultiWeight){
       truthTree->Branch("EventWeight_MUR05_MUF05", &m_WeightEvents_MUR05_MUF05);
       truthTree->Branch("EventWeight_MUR05_MUF1", &m_WeightEvents_MUR05_MUF1);
       truthTree->Branch("EventWeight_MUR1_MUF05", &m_WeightEvents_MUR1_MUF05);
       truthTree->Branch("EventWeight_MUR1_MUF2", &m_WeightEvents_MUR1_MUF2);
       truthTree->Branch("EventWeight_MUR2_MUF1", &m_WeightEvents_MUR2_MUF1);
       truthTree->Branch("EventWeight_MUR2_MUF2", &m_WeightEvents_MUR2_MUF2);
    }
    truthTree->Branch("ptjets_truth", ptjets_truth);
    truthTree->Branch("etajets_truth", etajets_truth);
    truthTree->Branch("phijets_truth", phijets_truth);
    truthTree->Branch("massjets_truth", massjets_truth);
    truthTree->Branch("labeljets_truth", labeljets_truth);

    truthTree->Branch("mc_eta", mc_eta);
    truthTree->Branch("mc_pt", mc_pt);
    truthTree->Branch("mc_mass", mc_mass);
    truthTree->Branch("mc_phi", mc_phi);
    truthTree->Branch("mc_pdgid", mc_pdgid);
    truthTree->Branch("mc_parent_pdgid", mc_parent_pdgid);

    truthTree->Branch("EtMiss_truthPhi", &EtMiss_truthPhi);
    truthTree->Branch("EtMiss_truth", &EtMiss_truth);
    /*
        truthTree->Branch("EventNumber"                , &EventNumber                );
        truthTree->Branch("WeightLumi"                 , &m_weight_luminosity        );

    //  truthTree->Branch("nVx"                        , &NumPrimaryVertices         );
        truthTree->Branch("mu"                         , &AverageInteractionsPerCrossing);

        truthTree->Branch("xsec"                       , &xsec                       );
        truthTree->Branch("WeightEventsPU"             , &WeightEventsPU             );
        truthTree->Branch("WeightEventselSF"           , &WeightEventselSF           );
        truthTree->Branch("WeightEventsmuSF"           , &WeightEventsmuSF           );
        truthTree->Branch("WeightEvents"               , &WeightEvents               );
        truthTree->Branch("WeightEventsJVT"            , &WeightEventsJVT            );
        truthTree->Branch("WeightEventsbTag"           , &WeightEventsbTag           );
        truthTree->Branch("WeightEvents_trigger"       , &WeightEvents_trigger_dilep );

        truthTree->Branch("jet1pT"                     , &jet1pT                     );
        truthTree->Branch("njet30"                     , &m_njet30                   );
        truthTree->Branch("njet40"                     , &m_njet40                   );
        truthTree->Branch("njet50"                     , &m_njet50                   );

        truthTree->Branch("lep1pT"                     , &lep1pT                     );
        truthTree->Branch("lep2pT"                     , &lep2pT                     );
        truthTree->Branch("lep3pT"                     , &lep3pT                     );
        truthTree->Branch("lep1eta"                    , &lep1eta                    );
        truthTree->Branch("lep2eta"                    , &lep2eta                    );
        truthTree->Branch("lep3eta"                    , &lep3eta                    );
        truthTree->Branch("lep1class"                  , &lep1class                  );
        truthTree->Branch("lep2class"                  , &lep2class                  );
        truthTree->Branch("lep3class"                  , &lep3class                  );

        truthTree->Branch("MET"                        , &MET                        );
        truthTree->Branch("MET_soft"                   , &MET_soft                   );
        truthTree->Branch("ptll"                       , &closest_ptll               );
        truthTree->Branch("dphill"                     , &closest_dphill             );
        truthTree->Branch("nbjet"                      , &m_nbjet                    );
        truthTree->Branch("closest_mll"                , &closest_mll                );
        truthTree->Branch("mlll"                       , &mlll                       );

        truthTree->Branch("EventType"                  , &bin                        );
    */


    return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODTruth :: execute ()
{
    const char *APP_NAME = "execute()";

    mc_eta->clear();
    mc_pt->clear();
    mc_mass->clear();
    mc_phi->clear();
    mc_pdgid->clear();
    mc_parent_pdgid->clear();

    ptjets_truth->clear();
    etajets_truth->clear();
    phijets_truth->clear();
    massjets_truth->clear();
    labeljets_truth->clear();

    if (m_debug) Info(APP_NAME, "New event" );

    //----------------------------
    // Event information
    //---------------------------
    if (m_debug) Info(APP_NAME, "Retrieving event info collection." );

    const xAOD::EventInfo *eventInfo = 0;
    if ( ! m_event->retrieve( eventInfo, "EventInfo").isSuccess() ) {
        Error(APP_NAME, "Failed to retrieve event info collection. Exiting." );
        return EL::StatusCode::FAILURE;
    }

    m_EventNumber = eventInfo->eventNumber();
    m_RunNumber = eventInfo->runNumber();
    m_WeightEvents = eventInfo->mcEventWeights().at(0);
    if(MultiWeight){
       m_WeightEvents_MUR05_MUF05 = eventInfo->mcEventWeights().at(4);
       m_WeightEvents_MUR05_MUF1 = eventInfo->mcEventWeights().at(5);
       m_WeightEvents_MUR1_MUF05 = eventInfo->mcEventWeights().at(6);
       m_WeightEvents_MUR1_MUF2 = eventInfo->mcEventWeights().at(8);
       m_WeightEvents_MUR2_MUF1 = eventInfo->mcEventWeights().at(9);
       m_WeightEvents_MUR2_MUF2 = eventInfo->mcEventWeights().at(10);

       // 'MUR0.5_MUF0.5_PDF261000': 4
       // 'MUR0.5_MUF1_PDF261000': 5
       // 'MUR1_MUF0.5_PDF261000': 6
       // 'MUR1_MUF2_PDF261000': 8
       // 'MUR2_MUF1_PDF261000': 9
       // 'MUR2_MUF2_PDF261000': 10

        /*std::cout << "peso default: " <<  m_WeightEvents << std::endl;
        std::cout << "peso 1: " <<  m_WeightEvents_MUR05_MUF05 << std::endl;
        std::cout << "peso 2: " <<  m_WeightEvents_MUR05_MUF1 << std::endl;
        std::cout << "peso 3: " <<  m_WeightEvents_MUR1_MUF05 << std::endl;
        std::cout << "peso 4: " <<  m_WeightEvents_MUR1_MUF2 << std::endl;
        std::cout << "peso 5: " <<  m_WeightEvents_MUR2_MUF1 << std::endl;
        std::cout << "peso 6: " <<  m_WeightEvents_MUR2_MUF2 << std::endl;*/
    }


    //asg::AnaToolHandle< PMGTools::IPMGTruthWeightTool > weightTool("PMGTools::PMGTruthWeightTool/PMGTruthWeightTool");
    //ANA_CHECK(weightTool.retrieve());


    ANA_MSG_INFO("Creating PMGTruthWeightTool...");
    asg::AnaToolHandle< PMGTools::IPMGTruthWeightTool > weightTool;
    ASG_SET_ANA_TOOL_TYPE(weightTool, PMGTools::PMGTruthWeightTool);
    weightTool.setName("PMGTruthWeightTool");
    ANA_CHECK(weightTool.initialize());

    auto weightNames = weightTool->getWeightNames();
    ANA_MSG_INFO("found " << weightNames.size() << " weights for this event");


    // Print out the first weight for every event
    if (weightNames.size() > 1) {
      static std::string weight_name_to_test = weightNames.at(1);
      ANA_MSG_INFO(" weight called '" << weight_name_to_test << "' = " << weightTool->getWeight(weight_name_to_test));
    }

/*
    if (skipCBK) {
        NumberEvents->Fill(1, m_WeightEvents);
        NumberEvents->Fill(2, m_WeightEvents * m_WeightEvents);
        if(MultiWeight){
          NumberEvents->Fill(3, m_WeightEvents_MUR05_MUF05);
          NumberEvents->Fill(4, m_WeightEvents_MUR05_MUF1);
          NumberEvents->Fill(5, m_WeightEvents_MUR1_MUF05);
          NumberEvents->Fill(6, m_WeightEvents_MUR1_MUF2);
          NumberEvents->Fill(7, m_WeightEvents_MUR2_MUF1);
          NumberEvents->Fill(8, m_WeightEvents_MUR2_MUF2);
        }
    }
    if (processID == -1) m_crossSection = my_XsecDB->xsectTimesEff(eventInfo->mcChannelNumber());
    else m_crossSection = my_XsecDB->xsectTimesEff(eventInfo->mcChannelNumber(), processID);

    //Retrieve the truth particles
    const xAOD::TruthParticleContainer* truthP(0);
    ANA_CHECK( m_event->retrieve( truthP, "TruthParticles" ) );

    //Retrieve the truth JETS
    const xAOD::JetContainer* truth_jets(0);
    ANA_CHECK( m_event->retrieve(truth_jets, "AntiKt4TruthJets") );

    if (m_debug) Info(APP_NAME, "Loop on Jet" );
    for (const auto& truthJ_itr : *truth_jets) {
        if (truthJ_itr->pt() > 25000. && fabs(truthJ_itr->eta()) < 2.8) {

            dec_passOR(*truthJ_itr) = true;

            for ( const auto& truthP_itr : *truthP) {

                if (isSherpa) {
                    if (((truthP_itr->pt() > 5000. && fabs(truthP_itr->pdgId()) == 13) || (truthP_itr->pt() > 7000. && fabs(truthP_itr->pdgId()) == 11)) && fabs(truthP_itr->eta()) < 2.5 && truthP_itr->status() == 3 && truthP_itr->auxdata<unsigned
                            int>("classifierParticleType") == 21) {

                        float dR = xAOD::P4Helpers::deltaR2( truthJ_itr, truthP_itr, true);
                        if (m_debug) Info(APP_NAME, "jet %f lep %f DR %f", truthJ_itr->pt(), truthP_itr->pt(), sqrt(dR) );

                        if (dR < (0.2 * 0.2)) {
                            dec_passOR(*truthJ_itr) = false;
                            if (m_debug) Info(APP_NAME, "Rejecting jet!" );
                            break;
                        }
                    }
                }
                else {
                    if (((truthP_itr->pt() > 5000. && fabs(truthP_itr->pdgId()) == 13) || (truthP_itr->pt() > 7000. && fabs(truthP_itr->pdgId()) == 11)) && fabs(truthP_itr->eta()) < 2.5 && truthP_itr->status() == 1) {

                        float dR = xAOD::P4Helpers::deltaR2( truthJ_itr, truthP_itr, true);
                        if (m_debug) Info(APP_NAME, "jet %f lep %f DR %f", truthJ_itr->pt(), truthP_itr->pt(), sqrt(dR) );

                        if (dR < (0.2 * 0.2)) {
                            dec_passOR(*truthJ_itr) = false;
                            if (m_debug) Info(APP_NAME, "Rejecting jet!" );
                            break;
                        }
                    }
                }

            }
        }
        else {
            dec_passOR(*truthJ_itr) = false;
        }
    }

    if (m_debug) Info(APP_NAME, "Loop on lep" );
    for ( const auto& truthP_itr : *truthP) {

        if (isSherpa) {
            if (truthP_itr->pt() > 5000. && fabs(truthP_itr->eta()) < 2.5 && truthP_itr->status() == 3 && truthP_itr->auxdata<unsigned
                    int>("classifierParticleType") == 21) {

                dec_passOR(*truthP_itr) = true;

                if (fabs(truthP_itr->pdgId()) == 13 || (truthP_itr->pt() > 7000. && fabs(truthP_itr->pdgId()) == 11)) {

                    for (const auto& truthJ_itr : *truth_jets) {

                        if (truthJ_itr->pt() > 25000. && fabs(truthJ_itr->eta()) < 2.8 && truthJ_itr->auxdata< bool >("passTruthOR") ) {

                            float dR = xAOD::P4Helpers::deltaR2( truthP_itr, truthJ_itr, true);
                            if (m_debug) Info(APP_NAME, "jet %f lep %f DR %f", truthJ_itr->pt(), truthP_itr->pt(), sqrt(dR) );

                            if (dR < (0.4 * 0.4)) {
                                dec_passOR(*truthP_itr) = false;
                                if (m_debug) Info(APP_NAME, "Rejecting lep!" );
                                break;
                            }
                        }
                    }
                }
            } else {
                dec_passOR(*truthP_itr) = false;
            }
        }
        else {
            if (truthP_itr->pt() > 5000. && fabs(truthP_itr->eta()) < 2.5 && truthP_itr->status() == 1) {

                dec_passOR(*truthP_itr) = true;

                if (fabs(truthP_itr->pdgId()) == 13 || (truthP_itr->pt() > 7000. && fabs(truthP_itr->pdgId()) == 11)) {

                    for (const auto& truthJ_itr : *truth_jets) {

                        if (truthJ_itr->pt() > 25000. && fabs(truthJ_itr->eta()) < 2.8 && truthJ_itr->auxdata< bool >("passTruthOR") ) {

                            float dR = xAOD::P4Helpers::deltaR2( truthP_itr, truthJ_itr, true);
                            if (m_debug) Info(APP_NAME, "jet %f lep %f DR %f", truthJ_itr->pt(), truthP_itr->pt(), sqrt(dR) );

                            if (dR < (0.4 * 0.4)) {
                                dec_passOR(*truthP_itr) = false;
                                if (m_debug) Info(APP_NAME, "Rejecting lep!" );
                                break;
                            }
                        }
                    }
                }
            } else {
                dec_passOR(*truthP_itr) = false;
            }
        }
    }

    //Retrieve the truth MET
    const xAOD::MissingETContainer* truth_metcontainer(0);
    ANA_CHECK( m_event->retrieve( truth_metcontainer, "MET_Truth" ));
    const xAOD::MissingET* met_nonint = (*truth_metcontainer)["NonInt"];

    float EtmissTruth_Etx = met_nonint->mpx();
    float EtmissTruth_Ety = met_nonint->mpy();
    float EtmissTruth_Et = sqrt(EtmissTruth_Etx * EtmissTruth_Etx + EtmissTruth_Ety * EtmissTruth_Ety);

    TLorentzVector MET_Truth(EtmissTruth_Etx, EtmissTruth_Ety, 0., EtmissTruth_Et);

    //Look at MET truth
    EtMiss_truth = MET_Truth.Perp();
    EtMiss_truthPhi = MET_Truth.Phi();

    int parent = -1;
    int nele10 = 0;
    int nmu10 = 0;
    int nele7 = 0;
    int nmu5 = 0;

    for ( const auto& truthP_itr : *truthP) {
        bool save = false;

        if (isSherpa) {
            //Electrons
            if (truthP_itr->pt() > 7000. && truthP_itr->status() == 3 && truthP_itr->auxdata<unsigned
                    int>("classifierParticleType") == 21 && fabs(truthP_itr->pdgId()) == 11 && truthP_itr->auxdata< bool >("passTruthOR")) {
                save = true;
                nele7++;
                if (truthP_itr->pt() > 10000.) nele10++;
            }
            //Muons
            if (truthP_itr->pt() > 5000. && truthP_itr->status() == 3 && truthP_itr->auxdata<unsigned
                    int>("classifierParticleType") == 21 && fabs(truthP_itr->pdgId()) == 13 && truthP_itr->auxdata< bool >("passTruthOR")) {
                save = true;
                nmu5++;
                if (truthP_itr->pt() > 10000.) nmu10++;
            }
        }
        else {
            //Electrons
            if (truthP_itr->pt() > 7000. && truthP_itr->status() == 1 && fabs(truthP_itr->pdgId()) == 11 && truthP_itr->auxdata< bool >("passTruthOR")) {
                if (truthP_itr->nParents() == 1)parent = truthP_itr->parent(0)->pdgId();
                else parent = -1;
                for (unsigned int nparents = 0; nparents < truthP_itr->nParents(); ++nparents) {
                    if (truthP_itr->parent(nparents)) {
                        if ((fabs(truthP_itr->parent(nparents)->pdgId()) == 15) || (fabs(truthP_itr->parent(nparents)->pdgId()) >= 23 &&
                                fabs(truthP_itr->parent(nparents)->pdgId()) <= 25)) {
                            save = true;
                            nele7++;
                            if (truthP_itr->pt() > 10000.) nele10++;
                        }
                    }
                }
            }
            //Muons
            if (truthP_itr->pt() > 5000. && truthP_itr->status() == 1 && fabs(truthP_itr->pdgId()) == 13 && truthP_itr->auxdata< bool >("passTruthOR")) {
                if (truthP_itr->nParents() == 1)parent = truthP_itr->parent(0)->pdgId();
                else parent = -1;
                for (unsigned int nparents = 0; nparents < truthP_itr->nParents(); ++nparents) {
                    if (truthP_itr->parent(nparents)) {
                        if ((fabs(truthP_itr->parent(nparents)->pdgId()) == 15) ||
                                (fabs(truthP_itr->parent(nparents)->pdgId()) >= 23 && fabs(truthP_itr->parent(nparents)->pdgId()) <= 25)) {
                            save = true;
                            nmu5++;
                            if (truthP_itr->pt() > 10000.) nmu10++;
                        }
                    }
                }
            }
        }

        //Electron neutrino
        if (truthP_itr->pt() > 10000. && truthP_itr->status() == 1 && fabs(truthP_itr->pdgId()) == 12) {
            if (truthP_itr->nParents() == 1)parent = truthP_itr->parent(0)->pdgId();
            else parent = -1;
            for (unsigned int nparents = 0; nparents < truthP_itr->nParents(); ++nparents) {
                if (truthP_itr->parent(nparents)) {
                    if ((fabs(truthP_itr->parent(nparents)->pdgId()) == 15) || (fabs(truthP_itr->parent(nparents)->pdgId()) >= 23 &&
                            fabs(truthP_itr->parent(nparents)->pdgId()) <= 25)) {
                        save = true;
                    }
                }
            }
        }
        //Muon neutrino
        if (truthP_itr->pt() > 10000. && truthP_itr->status() == 1 && fabs(truthP_itr->pdgId()) == 14) {
            if (truthP_itr->nParents() == 1)parent = truthP_itr->parent(0)->pdgId();
            else parent = -1;
            for (unsigned int nparents = 0; nparents < truthP_itr->nParents(); ++nparents) {
                if (truthP_itr->parent(nparents)) {
                    if ((fabs(truthP_itr->parent(nparents)->pdgId()) == 15) ||
                            (fabs(truthP_itr->parent(nparents)->pdgId()) >= 23 && fabs(truthP_itr->parent(nparents)->pdgId()) <= 25)) {
                        save = true;
                    }
                }
            }
        }


        // B - take only bot from W Z H + SUSY PARTICLES and TOP
        if (truthP_itr->pt() > 10000. && fabs(truthP_itr->pdgId()) == 5) {
            if (truthP_itr->nParents() == 1)parent = truthP_itr->parent(0)->pdgId();
            else parent = -1;
            for (unsigned int nparents = 0; nparents < truthP_itr->nParents(); ++nparents) {
                if (truthP_itr->parent(nparents)) {
                    if ((fabs(truthP_itr->parent(nparents)->pdgId()) >= 1000001 && fabs(truthP_itr->parent(nparents)->pdgId()) <= 2000016) ||
                            (fabs(truthP_itr->parent(nparents)->pdgId()) >= 23 && fabs(truthP_itr->parent(nparents)->pdgId()) <= 25) ||
                            (fabs(truthP_itr->parent(nparents)->pdgId()) == 6)) {
                        save = true;
                    }
                }
            }
        }

        // TOP - take all
        if (truthP_itr->pt() > 10000. && fabs(truthP_itr->pdgId()) == 6) {
            if (truthP_itr->nParents() == 1)parent = truthP_itr->parent(0)->pdgId();
            else parent = -1;
            for (unsigned int nparents = 0; nparents < truthP_itr->nParents(); ++nparents) {
                if (truthP_itr->parent(nparents)) {
                    if (truthP_itr->parent(nparents)->pdgId() != truthP_itr->pdgId()) {
                        save = true;
                    }
                }
            }
        }

        // Vector Boson (W,Z,H)
        if (truthP_itr->pt() > 10000. && fabs(truthP_itr->pdgId()) >= 23 && fabs(truthP_itr->pdgId()) <= 25) {
            if (truthP_itr->nParents() == 1)parent = truthP_itr->parent(0)->pdgId();
            else parent = -1;
            for (unsigned int nparents = 0; nparents < truthP_itr->nParents(); ++nparents) {
                if (truthP_itr->parent(nparents)) {
                    if ((truthP_itr->parent(nparents)->pdgId() != truthP_itr->pdgId()) && truthP_itr->p4().M() > 40000.) {
                        save = true;
                    }
                }
            }
        }

        // SUSY!
        if (truthP_itr->pt() > 10000. && fabs(truthP_itr->pdgId()) >= 1000001 && fabs(truthP_itr->pdgId()) <= 2000016) save = true;

        if (save == true) {
            mc_eta->push_back(truthP_itr->eta());
            mc_pt->push_back(truthP_itr->pt());
            mc_mass->push_back(truthP_itr->p4().M());
            mc_phi->push_back(truthP_itr->phi());
            mc_pdgid->push_back(truthP_itr->pdgId());
            mc_parent_pdgid->push_back(parent);
        }
    }

    //Look at the truth JETS
    for (const auto& truthJ_itr : *truth_jets) {
        if (truthJ_itr->pt() > 25000. && fabs(truthJ_itr->eta()) < 2.8 && truthJ_itr->auxdata< bool >("passTruthOR")) {
            ptjets_truth->push_back(truthJ_itr->pt());
            etajets_truth->push_back(truthJ_itr->eta());
            phijets_truth->push_back(truthJ_itr->phi());
            massjets_truth->push_back(truthJ_itr->m());
            labeljets_truth->push_back(truthJ_itr->auxdata<int>("PartonTruthLabelID"));
        }
    }

    if (m_debug) Info(APP_NAME, "nele %d nmu %d", nele10, nmu10 );
    if (m_debug) Info(APP_NAME, "Filling Tree" );

    if (((nele10 + nmu10) > 1) || (((nele7 + nmu5) > 1) && EtMiss_truth > 100000.)) truthTree->Fill();

*/
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODTruth :: postExecute ()
{
    // Here you do everything that needs to be done after the main event
    // processing.  This is typically very rare, particularly in user
    // code.  It is mainly used in implementing the NTupleSvc.
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODTruth :: finalize ()
{
    // This method is the mirror image of initialize(), meaning it gets
    // called after the last event has been processed on the worker node
    // and allows you to finish up any objects you created in
    // initialize() before they are written to disk.  This is actually
    // fairly rare, since this happens separately for each worker node.
    // Most of the time you want to do your post-processing on the
    // submission node after all your histogram outputs have been
    // merged.  This is different from histFinalize() in that it only
    // gets called on worker nodes that processed input events.

    const char *APP_NAME = "finalize()";
    Info(APP_NAME, "All done. Now cleaning up...");

    if ( my_XsecDB ) {
        delete my_XsecDB;
        my_XsecDB = 0;
    }

    return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODTruth :: histFinalize ()
{
    // This method is the mirror image of histInitialize(), meaning it
    // gets called after the last event has been processed on the worker
    // node and allows you to finish up any objects you created in
    // histInitialize() before they are written to disk.  This is
    // actually fairly rare, since this happens separately for each
    // worker node.  Most of the time you want to do your
    // post-processing on the submission node after all your histogram
    // outputs have been merged.  This is different from finalize() in
    // that it gets called on all worker nodes regardless of whether
    // they processed input events.
    return EL::StatusCode::SUCCESS;
}
