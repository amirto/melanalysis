// analysis code
#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/Sample.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoopGrid/PrunDriver.h"
#include <TSystem.h>

#include "MelAnalysis/MyxAODAnalysis.h"

// standard library stuff
#include <iostream>
#include <stdlib.h>     /* getenv */

using namespace std;

//comment for merge
// usage: stopNtupGrid dataset format syst truth btag photons processID debug

// processID  process
// 2          gluino pairs
// 61         stop pairs
// 51         sbottom pairs

// formats: stop2L, stop4b, stopZ, stopH

int main( int argc, char* argv[] ) {

  // Take the submit directory from the input if provided:
  std::string submitDir = "submitDir";
  gSystem->Exec(("rm -rf " + submitDir).c_str());

  std::string sample;
  if ( argc < 2 ) {
    cout << "You need to provide a dataset name!" << endl;
    return 1;
  }

  sample = argv[ 1 ];

  std::string format;
  if ( argc < 3 ) {
    cout << "You need to provide the ntuple format!" << endl;
    return 1;
  }

  format = argv[ 2 ];

  bool doSyst = false;
  if ( argc > 3 ) {
    std::string checktruth = argv[ 3 ];
    if ( checktruth == "true" )
      doSyst = true;
    else
      doSyst = false;
  }

  bool doTruth = false;
  if ( argc > 4 ) {
    std::string checktruth = argv[ 4 ];
    if ( checktruth == "true" )
      doTruth = true;
    else
      doTruth = false;
  }

  bool doBtag = false;
  if ( argc > 5 ) {
    std::string checktruth = argv[ 5 ];
    if ( checktruth == "true" )
      doBtag = true;
    else
      doBtag = false;
  }

  bool doPhotons = false;
  if ( argc > 6 ) {
    std::string checktruth = argv[ 6 ];
    if ( checktruth == "true" )
      doPhotons = true;
    else
      doPhotons = false;
  }

  int processID = -1;
  if ( argc > 7 ) {
    processID = atoi(argv[ 7 ]);
  }

  bool doDebug = false;
  if ( argc > 8 ) {
    std::string checktruth = argv[ 8 ];
    if ( checktruth == "true" )
      doDebug = true;
    else
      doDebug = false;
  }

  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // Construct the samples to run on:
  SH::SampleHandler sh;

  // use SampleHandler to scan the grid for a dataset
  //SH::scanRucio (sh, sample, true);
  SH::addGrid (sh, sample); //bypass rucio

  int isData = 0;
  int isA=0, isD=0, isE=0;

  for (SH::SampleHandler::iterator s = sh.begin(), end = sh.end(); s != end; ++ s) {

    std::size_t found_data = (*s)->name().find("data");
    std::size_t found_mca = (*s)->name().find("_r9364");
    std::size_t found_mcd = (*s)->name().find("_r10201");
    std::size_t found_mce = (*s)->name().find("_r10724");

    if (found_data != std::string::npos) isData = 1;
    if (found_mca != std::string::npos) isA = 1;
    if (found_mcd != std::string::npos) isD = 1;
    if (found_mce != std::string::npos) isE = 1;

    (*s)->meta()->setInteger("isdata", isData);
    (*s)->meta()->setInteger("isA", isA);
    (*s)->meta()->setInteger("isD", isD);
    (*s)->meta()->setInteger("isE", isE);

std::cout << "isA: "<< isA << std::endl;
std::cout << "isD: "<< isD << std::endl;
std::cout << "isE: "<< isE << std::endl;
std::cout << "isData: "<< isData << std::endl;



    (*s)->meta()->setString("samplename", (*s)->name());

    int isAtlFast = 0;
    std::size_t found_atlfast = (*s)->name().find("_a");
    if (found_atlfast != std::string::npos) {
      isAtlFast = 1;
    }
    (*s)->meta()->setInteger("isatlfast", isAtlFast);
    
  }

  // define the output sample name
  std::size_t pos_start = sample.find(".");
	std::cout << sample << std::endl;
	if(sample.substr(0,4) == "data") pos_start = sample.find(":");
  std::size_t middle = sample.find(".", pos_start + 1);
  std::size_t pos_end = sample.find(".", middle + 1);
	
//std::cout << "/////////////" << std::endl;

  std::string short_name = sample.substr(pos_start + 1, pos_end - pos_start - 1);
  std::string outname;
  std::string release;
  std::string version;
	version = "3.2";
	release = "21.2.51";
	outname = " ";
//std::cout << version << std::endl;
//std::cout << release << std::endl;
//std::cout << release << std::endl;
  if (isA){ outname = "user." + (std::string)getenv("RUCIO_ACCOUNT") + "." + short_name + "."+release+".mc16a_v"+ version + "";}
  else if (isD) {outname = "user." + (std::string)getenv("RUCIO_ACCOUNT") + "." + short_name + "."+release+".mc16d_v"+ version + "";}
  else if (isE) {outname = "user." + (std::string)getenv("RUCIO_ACCOUNT") + "." + short_name + "."+release+".mc16e_v"+ version + "";}
  else if (isData) {outname = "user." + (std::string)getenv("RUCIO_ACCOUNT") + "." + short_name + "."+release+".data_v"+ version + "";}

  std::cout << "Output dataset: " << outname << std::endl;

  // Set the name of the input TTree. It's always "CollectionTree"
  // for xAOD files.
  sh.setMetaString( "nc_tree", "CollectionTree" );

  // Print what we found:
  sh.print();

  // Create an EventLoop job:
  EL::Job job;
  job.sampleHandler( sh );
  job.options()->setString( EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_class );
  job.options()->setDouble( EL::Job::optMaxEvents, -1 );

  // Add our analysis to the job:
  MyxAODAnalysis* alg = new MyxAODAnalysis();
  alg->processID = processID;
  alg->doSyst = doSyst;
  alg->format = format;
  alg->doTruth = doTruth;
  alg->m_debug = doDebug;
  alg->doBtagWP = doBtag;
  alg->savePhotons = doPhotons;
  job.algsAdd( alg );

  // Run the job using the prun/grid driver:
  EL::PrunDriver driver;
  driver.options()->setString( "nc_outputSampleName", outname );
  driver.options()->setDouble( "nc_disableAutoRetry", 0 );
  if (isData) driver.options()->setDouble( "nc_nFilesPerJob", 5);
  else driver.options()->setDouble( "nc_nFilesPerJob", 1);
  driver.options()->setString( "nc_optGridDestSE", "INFN-LECCE_LOCALGROUPDISK");
  driver.options()->setString( "nc_excludedSite", "ANALY_INFN-NAPOLI");
  driver.options()->setString( "nc_excludedSite", "ANALY_INFN-NAPOLI-RECAS");

  driver.submitOnly( job, submitDir );

  return 0;
}
