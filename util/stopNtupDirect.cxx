// analysis code
#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/Sample.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/MetaObject.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>

#include "MelAnalysis/MyxAODAnalysis.h"

// standard library stuff
#include <iostream>
#include <stdlib.h>     /* getenv */

using namespace std;

// usage: stopNtupDirect format syst truth btag photons processID debug

// processID  process
// 2          gluino pairs
// 61         stop pairs
// 51         sbottom pairs

// formats: stop2L, stop4b, stopZ, stopH

void print_usage() {
  printf("Usage: stopNtupDirect required inputs : -d datasetname -f format [stop2L, stop4b, stopZ, stopH] options: -s doSys [default = false] -t doTruth [default = false] -b doBtag [default = false] -p doPhotons [default = false] -i processID [default = -1] // [2 : gluino pairs, 61 : stop pairs, 51 :sbottom pairs] -D debug\n");
}

int main( int argc, char* argv[] ) {
  int option = 0;
  // Take the submit directory from the input if provided:
  std::string submitDir = "submitDir";
  gSystem->Exec(("rm -rf " + submitDir).c_str());

  bool doSyst = false;
  bool doTruth = false;
  bool doBtag = false;
  bool doPhotons = false;
  bool doDebug = false;
  int processID = -1;
  std::string sample;
  std::string format;
  while ((option = getopt(argc, argv, "d:f:stbpi:D")) != -1) {
    switch (option) {
    case 'd' : sample = optarg;
      break;
    case 'f' : format = optarg;
      break;
    case 's' : doSyst = true;
      break;
    case 't' : doTruth = true;
      break;
    case 'b' : doBtag = true;
      break;
    case 'p' : doPhotons = true;
      break;
    case 'D' : doDebug = true;
      break;
    case 'i' : processID = atoi(optarg);
      break;
    default: print_usage();
      exit(1);
    }
  }
  if (sample.empty() || format.empty()) {
    if (sample.empty()) {
      std::cout << "Need dataset name option -d" << std::endl;
      print_usage();
      exit(1);
    }
    else {
      std::cout << "Need format option -f [stop2L, stop4b, stopZ]" << std::endl;
      print_usage();
      exit(1);
    }
  }
  if (!((format == "stop2L") || (format == "stop4b") || (format == "stopZ") || (format == "stopH"))) {
    std::cout << "format option unrecognized, please choose one of the following [stop2L, stop4b, stopZ, stopH]" << std::endl;
    print_usage();
    exit(1);
  }
  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // Construct the samples to run on:
  SH::SampleHandler sh;

  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
  //const char* inputFilePath = gSystem->ExpandPathName ("/afs/cern.ch/work/l/llongo/MelAnalysis_v49/Sample/mc15_13TeV.387928.MGPy8EG_A14N23LO_TT_bffN_260_180_MadSpin_m1002L3.merge.DAOD_SUSY5.e5469_a766_a821_r7676_p2874/");
  const char* inputFilePath = gSystem->ExpandPathName ("/terabig/amiucci/StopZH_rel21/MyCMakeProject/source/sample/");
  SH::ScanDir().filePattern("DAOD_SUSY7.11493420._000182.pool.root.1").scan(sh, inputFilePath);

  for (SH::SampleHandler::iterator sample = sh.begin(), end = sh.end(); sample != end; ++ sample) {

    int isData = 0;
    std::size_t found_data = (*sample)->name().find("data");
    if (found_data != std::string::npos) {
      isData = 1;
    }
    (*sample)->meta()->setInteger("isdata", isData);

    std::string prod = "mc15c";
    (*sample)->meta()->setString("MC_production", prod);

    int isAtlFast = 0;
    std::size_t found_atlfast = (*sample)->name().find("_a");
    if (found_atlfast != std::string::npos) {
      isAtlFast = 1;
    }
    (*sample)->meta()->setInteger("isatlfast", isAtlFast);

  }

// Set the name of the input TTree. It's always "CollectionTree"
// for xAOD files.
  sh.setMetaString( "nc_tree", "CollectionTree" );

// Print what we found:
  sh.print();

// Create an EventLoop job:
  EL::Job job;
  job.sampleHandler( sh );
  job.options()->setString( EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_class );
  job.options()->setDouble( EL::Job::optMaxEvents, -1 );

// Add our analysis to the job:
  MyxAODAnalysis* alg = new MyxAODAnalysis();
  alg->processID = processID;
  alg->doSyst = doSyst;
  alg->format = format;
  alg->doTruth = doTruth;
  alg->m_debug = doDebug;
  alg->doBtagWP = doBtag;
  alg->savePhotons = doPhotons;
  job.algsAdd( alg );

// Run the job using the direct driver:
  EL::DirectDriver driver;
  driver.submit( job, submitDir );

  return 0;
}
