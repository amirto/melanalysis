// analysis code
#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/Sample.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/MetaObject.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>

#include "MelAnalysis/MyxAODTruth.h"

// standard library stuff
#include <iostream>
#include <stdlib.h>     /* getenv */

using namespace std;

// usage: stopNtupDirect syst truth btag photons processID stopZ debug

// processID  process
// 2          gluino pairs
// 61         stop pairs
// 51         sbottom pairs

int main( int argc, char* argv[] ) {

  // Take the submit directory from the input if provided:
  std::string submitDir = "submitDir";
  gSystem->Exec(("rm -rf " + submitDir).c_str());

  int processID = -1;
  if ( argc > 1 ) {
    processID = atoi(argv[ 1 ]);
  }

  bool isSherpa = false;
  if ( argc > 2 ) {
    std::string checktruth = argv[ 2 ];
    if ( checktruth == "true" )
      isSherpa = true;
    else
      isSherpa = false;
  }

  bool skipCBK = false;
  if ( argc > 3 ) {
    std::string checktruth = argv[ 3 ];
    if ( checktruth == "true" )
      skipCBK = true;
    else
      skipCBK = false;
  }

  bool doDebug = false;
  if ( argc > 4 ) {
    std::string checktruth = argv[ 4 ];
    if ( checktruth == "true" )
      doDebug = true;
    else
      doDebug = false;
  }

  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // Construct the samples to run on:
  SH::SampleHandler sh;

  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
  const char* inputFilePath = gSystem->ExpandPathName ("/terabig/fmeloni/workarea/test_ZH");
  SH::ScanDir().scan(sh, inputFilePath);

// Set the name of the input TTree. It's always "CollectionTree"
// for xAOD files.
  sh.setMetaString( "nc_tree", "CollectionTree" );

// Print what we found:
  sh.print();

// Create an EventLoop job:
  EL::Job job;
  job.sampleHandler( sh );
  job.options()->setString( EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_class );
  job.options()->setDouble( EL::Job::optMaxEvents, -1 );

// Add our analysis to the job:
  MyxAODTruth* alg = new MyxAODTruth();
  alg->processID = processID;
  alg->skipCBK = skipCBK;
  alg->isSherpa = isSherpa;
  alg->m_debug = doDebug;
  job.algsAdd( alg );

// Run the job using the direct driver:
  EL::DirectDriver driver;
  driver.submit( job, submitDir );

  return 0;
}
