// analysis code
#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/Sample.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoopGrid/PrunDriver.h"
#include <TSystem.h>

#include "MelAnalysis/MyxAODAnalysis.h"

// standard library stuff
#include <iostream>
#include <stdlib.h>     /* getenv */

using namespace std;

// usage: stopNtupGrid dataset format syst truth btag photons processID debug

// processID  process
// 2          gluino pairs
// 61         stop pairs
// 51         sbottom pairs

// formats: stop2L, stop4b, stopZ, stopH

void print_usage() {
  printf("Usage: stopNtupGrid required inputs : -d datasetname -f format [stop2L, stop4b, stopZ, stopH] options: -s doSys [default = false] -t doTruth [default = false] -b doBtag [default = false] -p doPhotons [default = false] -i processID [default = -1] // [2 : gluino pairs, 61 : stop pairs, 51 :sbottom pairs] -D debug\n");
}

int main( int argc, char* argv[] ) {
  int option = 0;
  // Take the submit directory from the input if provided:
  std::string submitDir = "submitDir";
  gSystem->Exec(("rm -rf " + submitDir).c_str());

  bool doSyst = false;
  bool doTruth = false;
  bool doBtag = false;
  bool doPhotons = false;
  bool doDebug = false;
  int processID = -1;
  std::string sample;
  std::string format;
  std::string version;
  std::string versioninter;
  while ((option = getopt(argc, argv, "d:f:stbpi:Dv:")) != -1) {
    switch (option) {
    case 'd' : sample = optarg;
      break;
    case 'f' : format = optarg;
      break;
    case 's' : doSyst = true;
      break;
    case 't' : doTruth = true;
      break;
    case 'b' : doBtag = true;
      break;
    case 'p' : doPhotons = true;
      break;
    case 'D' : doDebug = true;
      break;
    case 'i' : processID = atoi(optarg);
      break;
    case 'v' : versioninter = optarg;
      break;
    default: print_usage();
      exit(1);
    }
  }
  if (sample.empty() || format.empty()) {
    if (sample.empty()) {
      std::cout << "Need dataset name option -d" << std::endl;
      print_usage();
      exit(1);
    }
    else {
      std::cout << "Need format option -f [stop2L, stop4b, stopZ]" << std::endl;
      print_usage();
      exit(1);
    }
  }
  if (!((format == "stop2L") || (format == "stop4b") || (format == "stopZ") || (format == "stopH"))) {
    std::cout << "format option unrecognized, please choose one of the following [stop2L, stop4b, stopZ, stopH]" << std::endl;
    print_usage();
    exit(1);
  }

  if (versioninter.empty()) {
    std::cout << "Defaulting to default version v44" << std::endl;
    version = "_v44";
    // version.append(versioninter);
  }
  else {
    std::cout << "using version " << version << std::endl;
    version = "_v";
    version.append(versioninter);
  }

  std::cout << "sample : " << sample << std::endl;
  std::cout << "format : " << format << std::endl;



  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // Construct the samples to run on:
  SH::SampleHandler sh;

  // use SampleHandler to scan the grid for a dataset
  //SH::scanRucio (sh, sample, true);
  SH::addGrid (sh, sample); //bypass rucio

  for (SH::SampleHandler::iterator s = sh.begin(), end = sh.end(); s != end; ++ s) {

    int isData = 0;
    std::size_t found_data = (*s)->name().find("data");
    if (found_data != std::string::npos) {
      isData = 1;
    }
    (*s)->meta()->setInteger("isdata", isData);

    std::string prod = "mc15c";
    (*s)->meta()->setString("MC_production", prod);

    int isAtlFast = 0;
    std::size_t found_atlfast = (*s)->name().find("_a");
    if (found_atlfast != std::string::npos) {
      isAtlFast = 1;
    }
    (*s)->meta()->setInteger("isatlfast", isAtlFast);

  }

  // define the output sample name
  std::size_t pos_start = sample.find(".");
  std::size_t middle = sample.find(".", pos_start + 1);
  std::size_t pos_end = sample.find(".", middle + 1);

  std::string short_name = sample.substr(pos_start + 1, pos_end - pos_start - 1);

  std::string outname = "user." + (std::string)getenv("RUCIO_ACCOUNT") + "." + short_name + "." + format + version;

  cout << "Output dataset: " << outname << endl;

// exit(0);

  // Set the name of the input TTree. It's always "CollectionTree"
  // for xAOD files.
  sh.setMetaString( "nc_tree", "CollectionTree" );

  // Print what we found:
  sh.print();

  // Create an EventLoop job:
  EL::Job job;
  job.sampleHandler( sh );
  job.options()->setString( EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_class );
  job.options()->setDouble( EL::Job::optMaxEvents, -1 );

  // Add our analysis to the job:
  MyxAODAnalysis* alg = new MyxAODAnalysis();
  alg->processID = processID;
  alg->doSyst = doSyst;
  alg->format = format;
  alg->doTruth = doTruth;
  alg->m_debug = doDebug;
  alg->doBtagWP = doBtag;
  alg->savePhotons = doPhotons;
  job.algsAdd( alg );

  // Run the job using the prun/grid driver:
  EL::PrunDriver driver;
  driver.options()->setString( "nc_outputSampleName", outname );
  driver.options()->setDouble( "nc_disableAutoRetry", 0 );
  //driver.options().setString("nc_optGridDestSE", "UNIBE-LHEP_LOCALGROUPDISK");

  driver.submitOnly( job, submitDir );

  cout << "Done with " << outname << endl;
  
  return 0;
}
