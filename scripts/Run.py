import ROOT
import logging
import shutil
import os
import re

#Example of usage
# python Run.py -L -s -b -t --inputDS 387918 --driver grid

logging.basicConfig(level=logging.INFO)
from optparse import OptionParser
parser = OptionParser()
parser.add_option("--submitDir", help="dir to store the output", default="submit_dir")
parser.add_option("--nevents", type=int, help="number of events to process for all the datasets")
parser.add_option("--skip-events", type=int, help="skip the first n events")
parser.add_option("-w", "--overwrite", action='store_false', default=True, help="overwrite previous submitDir")
parser.add_option("-s", "--syst", action='store_true', default=False, help="compute sytematic variations")
parser.add_option("-t", "--truth", action='store_true', default=False, help="add truth information")
parser.add_option("-d", "--debug", action='store_true', default=False, help="activate DEBUG mode")
parser.add_option("-p", "--photons", action='store_true', default=False, help="adds photons")
parser.add_option("-b", "--btagWP", action='store_true', default=False, help="adds b-tagging WP")
parser.add_option("--processID", type=int, help="process ID flag (int)", default=-1)
parser.add_option("--isdata", action='store_true', default=False, help="set as data")
parser.add_option("--isatlfast", action='store_true', default=False, help="set as atlfast")
parser.add_option("--format", type='string', help="ntuple format", default="stop2L")

#	processID		process
#	2				gluino pairs
#	61				stop pairs
#	51 				sbottom pairs	

(options, args) = parser.parse_args()

import atexit
@atexit.register
def quite_exit():
	ROOT.gSystem.Exit(0)

logging.info("loading packages")
ROOT.gROOT.Macro("$ROOTCOREDIR/scripts/load_packages.C")

if options.overwrite:
	shutil.rmtree(options.submitDir, True)

#Set up the job for xAOD access:
ROOT.xAOD.Init().ignore();

# create a new sample handler to describe the data files we use
logging.info("creating new sample handler")
sh_all = ROOT.SH.SampleHandler()

search_directories = []

if options.isdata:
	search_directories = ("/afs/cern.ch/work/a/amirto/C1C1WW",) 
else:
	search_directories = ("/afs/cern.ch/work/a/amirto/C1C1WW",) 
 

# scan for datasets in the given directories
for directory in search_directories:
	ROOT.SH.scanDir(sh_all, directory)

logging.info("%d different datasets found scanning all directories", len(sh_all))

#create the Sample Handler that will contain the actual datasets to be run over
sh_selected = ROOT.SH.SampleHandler()

for sample in sh_all:

	sample.setMetaString("samplename", sample.name())
	sample.setMetaDouble("isatlfast", options.isatlfast)	
	if "data" in sample.name(): isData=True
	else: isData=False
	if "_r9364" in sample.name(): isA=True
	else: isA=False
	if "_r10201" in sample.name(): isD=True
	else: isD=False
	if "_r10724" in sample.name(): isE=True
	else: isE=False

	sample.setMetaDouble("isdata", isData)
	sample.setMetaDouble("isA", isA)	
	sample.setMetaDouble("isD", isD)
	sample.setMetaDouble("isE", isE)	
	sh_selected.add(sample)

# print out the samples we found
logging.info("%d selected datasets", len(sh_selected))

# set the name of the tree in our files
sh_all.setMetaString("nc_tree", "CollectionTree")
  
# this is the basic description of our job
logging.info("creating new job")
job = ROOT.EL.Job()
job.sampleHandler(sh_selected)
job.options().setString(ROOT.EL.Job.optXaodAccessMode,ROOT.EL.Job.optXaodAccessMode_class);  
#job.options().setString(ROOT.EL.Job.optXaodAccessMode,ROOT.EL.Job.optXaodAccessMode_athena)

if options.nevents:
	logging.info("processing only %d events", options.nevents)
	job.options().setDouble(ROOT.EL.Job.optMaxEvents, options.nevents)

if options.skip_events:
	logging.info("skipping first %d events", options.skip_events)
	job.options().setDouble(ROOT.EL.Job.optSkipEvents, options.skip_events)

# add our algorithm to the job
logging.info("creating algorithms")
alg = ROOT.MyxAODAnalysis()
alg.processID = options.processID;
alg.doSyst = options.syst;
alg.format = options.format;
alg.doTruth = options.truth;
alg.m_debug = options.debug;
alg.doBtagWP = options.btagWP;
alg.savePhotons = options.photons;

logging.info("adding algorithms")
job.algsAdd(alg)

# make the driver we want to use
# this one works by running the algorithm directly
logging.info("creating driver")
driver = ROOT.EL.DirectDriver()
logging.info("submit job")
driver.submit(job, options.submitDir)

logging.info("Done!")