# $1 is the dataset list
# $2 is the format
# $3 turns on the systs
# $4 saves the truth

for dataset in `cat $1`
  do 
    stopNtupGrid $dataset $2 $3 $4

done
