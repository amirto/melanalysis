#!/usr/bin/python

import ROOT
import logging
import os, sys
import shutil

logging.basicConfig(level=logging.INFO)
from optparse import OptionParser

parser = OptionParser()
parser.add_option("--inDir", help="dir to store the output", default="ntup_stop2L_v23")
parser.add_option("--outDir", help="dir to store the output", default="bonsai_v23")

(options, args) = parser.parse_args()

# Open a file

dirs = os.listdir( options.inDir )

logging.info("Navigating %s", options.inDir)

# This would print all the files and directories
for file in dirs:
	
	print file

	current = -1
	first = -1
	last = -1

	for z in range(1,5):
		current = file.find(".",current+1)
		if z==2:
			first = current+1
		if z==4:
			last = current
 
	short_name = file[first:last]
	logging.info(" -> Merging %s", short_name)

	subdirs = os.listdir( options.inDir +"/"+ file )

	full_files = []

	for wut in subdirs:
		full_files.append(options.inDir+"/"+file+"/"+wut)

	shutil.rmtree(options.outDir+"/mc15_13TeV."+short_name, True)

	os.mkdir(options.outDir+"/mc15_13TeV."+short_name)

	command = "hadd "+options.outDir+"/mc15_13TeV."+short_name+"/"+short_name+".root "+' '.join(full_files)

	print command

	os.system(command)

logging.info("Done!")






